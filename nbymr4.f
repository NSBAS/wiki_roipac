c nbynhgt.f -- average a height file n looks in range, n in azimuth

	integer*4 N_RA
	parameter (N_RA = 80000)
	integer*4 width, wido
	integer*2 isum(N_RA), flag
        real*4 a2(N_RA*64), b(N_RA)
	integer*4 i,j,k
	integer*4 iind(64,64),jind(64,64)
	character*512 name, fin,fout
	
	i = iargc()
	if(i .lt. 4) then
	   write(6,*)
	1	'usage: nbymdem infile outfile width acavg [dnmavg flag]'
	   stop
	end if

	call getarg(1, fin)
	call getarg(2,fout)
	call getarg(3,name)
	read(name,*) width
	write(*,*) 'input width: ', width
	call getarg(4,name)
	read(name,*) navg
	write(*,*) 'averaging box width: ', navg
	mavg = navg
	if(i .ge. 5) then
	   call getarg(5,name)
	   read(name,*) mavg
	end if
	write(*,*) 'averaging box length: ', mavg
	flag = 0
	if(i .eq. 6) then
	   call getarg(6,name)
	   read(name,*) flag
	end if
	write(*,*) 'flag value: ', flag
	
	do i = 1 , navg
	   do j = 1 , mavg
	      iind(i,j) = i
	      jind(i,j) = j
	   end do
	end do
	nbin  = 4*width*mavg
	wido  = width/navg
	nbout = 4*wido
	write(*,*) 'number of samples out ', wido
        open(11,file=fin,access='direct',form='unformatted',recl=nbin)
        open(12,file=fout,access='direct',form='unformatted',recl=nbout)
	
c       loop over line number
	
	do i=1,1000000
	   if(mod(i,64).eq.0) print *,(i-1)*mavg-1
	   read(11,rec=i,err=99) (a2(k),k=1,width*mavg)
	   
	   do j = 1, wido
	      b(j)    = 0
	      isum(j) = 0
	   end do
	   
           do j=1,wido
	      
c       flag the indicator for bad points
	      
	      do k = 1 , navg
		 do l = 1, mavg
		    ioff = (j-1)*navg+iind(k,l)+(jind(k,l)-1)*width
		    if ( a2(ioff) .ne. flag) then
		       isum(j) = isum(j) + 1
		       b(j)    = b(j) + a2(ioff)
		    end if
		 end do
	      end do
	      
	   end do
	   
c       isum is number of good points
	   
	   do j = 1 , wido
              if(isum(j).ge.1)then
		 b(j)=b(j)/isum(j)	 
              else
		 b(j)=0.0
              end if
           end do
	   
	   write(12,rec=i) (b(k),k=1,wido)
	   
	end do
 99     continue
        close(12)
        close(11)
	end
