/*  *** JPL/Caltech Repeat Orbit Interferometry (ROI) Package ***   */

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

/*Right Looking SAR*/

int main(int argc, char *argv[]){
  /***************************/
  /***  Input Variables    ***/
  /***************************/
  double RangeStart, RangeStep;
  double EarthRadius, Height;
  int Width;
  char OutFile[1000];
  /***************************/
  /***  Local Variables    ***/
  /***************************/
  FILE *OutFP;
  int i,j;
  /* 
  float *Out; 
  int UpData;
  double Pi=4*atan2(1,1);
  double dr,dh;
  */
  double r;
  double sini;
  float *Incidence;
  int Length;
  /***************************/
  /***    Get  Input       ***/
  /***************************/
  if(argc < 7){
    printf("   usage:%s RangeStart RangeStep EarthRadius Height Width Length OutFile \n",argv[0]);
    exit(0);
    }
  sscanf(argv[1],"%lf",&RangeStart);
  sscanf(argv[2],"%lf",&RangeStep);
  sscanf(argv[3],"%lf",&EarthRadius);
  sscanf(argv[4],"%lf",&Height);
  sscanf(argv[5],"%d",&Width);
  sscanf(argv[6],"%d",&Length);
  sprintf(OutFile,"%s",argv[7]);
  /***************************/
  /***      Open Files     ***/
  /***************************/
  if((OutFP=fopen(OutFile,"w"))==NULL){
    fprintf(stderr,"OutFile %s not open\n",OutFile);
    exit(1);
    }
Incidence=(float *)malloc(sizeof(double)*Width);
  /***************************/
  /***    Compute phase    ***/
  /***************************/
  for(i=0;i<Width;i++){
    r=RangeStart+i*RangeStep;
    sini=Height/r-r/(2*EarthRadius)*(1.-(Height/r)*(Height/r));
    Incidence[i]=atan2(sini,sqrt(1.-sini*sini));
/*    printf("%lf\n", Incidence[i]);*/
    }
  for(j=0;j<Length;j++){
    fwrite(Incidence,sizeof(float),Width,OutFP);
    }

  /***************************/
  /***         End         ***/
  /***************************/
  return(0);
}

//POD=pod
//POD
//POD=head1 USAGE
//POD
//POD Usage:disp2phase RangeStart(dbl) RangeStep(dbl) EarthRadius(dbl)
//POD                  Height(dbl) Heading(dbl) WaveLength(dbl) Width(int)
//POD    OutFile(str) EastFile(str) NorthFile(str) [UpFile(str)]
//POD 
//POD=head1 FUNCTION
//POD
//POD FUNCTIONAL DESCRIPTION: "disp2phase" computes phase change   
//POD from a displacement field [NorthFile,EastFile], with optional 
//POD uplift [UpFile], and an angle of incidence computed from
//POD orbit data. All input (2 or 3) and output files have the same 
//POD record length: "Width".
//POD
//POD r=RangeStart+i*RangeStep;  
//POD sini=Height/r-r/(2*EarthRadius)*(1.-(Height/r)*(Height/r));
//POD Incidence[i]=atan2(sini,sqrt(1.-sini*sini));
//POD
//POD dh=East[i]*cos(Heading)-North[i]*sin(Heading);
//POD dr=dh*cos(Incidence[i]);
//POD if(UpData)dr+=-1.*Up[i]*sin(Incidence[i]);
//POD Out[i]=dr*4.*Pi/Wavelength; 
//POD
//POD=head1 ROUTINES CALLED
//POD
//POD none
//POD
//POD=head1 CALLED BY
//POD
//POD
//POD=head1 FILES USED
//POD
//POD Two files containg shifts in "Northing & Easting" directions as flat r*4/float binary files.
//POD
//POD=head1 FILES CREATED
//POD
//POD "OutFile" contains the simulated phased (r*4/float) due to the displacements field [EastFile,NorthFile,[UpFile]]
//POD
//POD=head1 DIAGNOSTIC FILES
//POD
//POD
//POD=head1 HISTORY
//POD
//POD Routines written by Francois Rogez
//POD
//POD=head1 LAST UPDATE
//POD  Date Changed        Reason Changed 
//POD  ------------       ----------------
//POD
//POD POD comments trm Jan 29th '04
//POD=cut
