      program rect_lookup
c     
c*****************************************************************************
c**   
c**   FILE NAME: rect_lookup.f
c** 
c**   modified to work with complex input and fixed some minor bugs EJF 96/9/10
c**
c**   modified from bi_polar.f
c**  
c**   DATE WRITTEN: 27-jan-93
c**   
c**   PROGRAMMER: P.A.Rosen
c**   
c**   FUNCTIONAL DESCRIPTION:  This program corrects
c**   monostatic image to align with the bistatic image
c**   
c**   UPDATE LOG:
c**   
c*****************************************************************************
      
      
      implicit none
      
      REAL*4, DIMENSION(:,:), ALLOCATABLE :: in_img
      REAL*4, DIMENSION(:), ALLOCATABLE :: lonlat
      REAL*4, DIMENSION(:), ALLOCATABLE :: arr
      COMPLEX*8, DIMENSION(:), ALLOCATABLE :: carr

      real*4  pt1(3),pt2(3),pt3(3),pt4(3)
      real*8  colval, rowval, ocolval, orowval
      real*8  ifrac, jfrac
      real    interp,alon_first,alat_first,alon_step,alat_step,alon,alat

      integer oi, oj, i, j, k, ift, iis, ilooksac,ilooksdn, icnt, jcnt, nacout
      integer iargc, ndac, nddn, nrac, nrdn, nraco, nrdno, sraco, srdno, ncarrmax

      character*512 fname, lonlatfile, resfile, outfile, intstyle, filetype

      integer rdflen
      character*255 rdfval,rdftmp
      character*255 rdfcullsp,rdfdata

      save in_img

      if(iargc() .eq. 0) then
         write(*,*) 'usage: rect_lookup param_file'
         stop
      end if

      call getarg(1,fname)
      call rdf_init('ERRFILE=SCREEN')
      write(6,'(a)') 'Reading command file data...'
      call rdf_read(fname)

      rdftmp= rdfval('Radar Lookup File Name','-')
      read(unit=rdftmp,fmt='(a)') lonlatfile
      rdftmp= rdfval('Lookup File Dimensions','-')
      read(unit=rdftmp,fmt=*) ndac, nddn
      rdftmp= rdfval('Input Image File Name','-')
      read(unit=rdftmp,fmt='(a)') resfile
      rdftmp= rdfval('Input File Dimensions','-')
      read(unit=rdftmp,fmt=*) nrac, nrdn
      rdftmp= rdfval('Longitude Latitude First Pixel','-')
      read(unit=rdftmp,fmt=*) alon_first,alat_first
      rdftmp= rdfval('Longitude Latitude Step','-')
      read(unit=rdftmp,fmt=*) alon_step,alat_step
      rdftmp= rdfval('Output Image File Name','-')
      read(unit=rdftmp,fmt='(a)') outfile
      rdftmp= rdfval('Lookup File Start Sample for Output ','-')
      read(unit=rdftmp,fmt=*) sraco
      rdftmp= rdfval('Number of Samples for Output ','-')
      read(unit=rdftmp,fmt=*) nraco
      rdftmp= rdfval('Lookup File Start Line for Output ','-')
      read(unit=rdftmp,fmt=*) srdno
      rdftmp= rdfval('Number of Lines for Output ','-')
      read(unit=rdftmp,fmt=*) nrdno
      rdftmp= rdfval('Skip Every N Lookup points Across-Down ','-')
      read(unit=rdftmp,fmt=*) ilooksac, ilooksdn
      rdftmp= rdfval('File Type','-')
      read(unit=rdftmp,fmt='(a)') filetype
      rdftmp= rdfval('Interpolation Method','-')
      read(unit=rdftmp,fmt='(a)') intstyle

      ift = 2
      if(index(filetype,'RMG') .ne. 0)then
         ift = 1
         write (*,*)  'Assuming RMG file type '
      elseif(index(filetype,'CPX') .ne. 0)then
         write (*,*)  'Assuming complex file type '
         ift = 0
      else
         write (*,*)  'Assuming R4 file type '
      endif
      iis = 0
      if(index(intstyle,'Bilinear') .ne. 0)then
         iis = 1
         write (*,*)  'Assuming Bilinear Interpolation '
      elseif(index(intstyle,'Sinc') .ne. 0)then
         iis = 2
         write (*,*)  'Assuming Sinc Interpolation '
      else
         write (*,*)  'Assuming Nearest Neighbor '

      end if

      nacout = nraco/ilooksac
      write(*,*) 'nrac ', nrac
      write(*,*) 'nrdn ', nrdn
      write(*,*) 'ndac ', ndac
      write(*,*) 'nddn ', nddn
      write(*,*) 'intstyle', intstyle
      write(*,*) 'num samples out 1', nraco,ilooksac,sraco,srdno,nrdno,ilooksdn
      write(*,*) 'num samples out', nacout

      ncarrmax=max(nrac,nacout)
      ALLOCATE( in_img (0:2*nrac-1,0:nrdn-1) )
      ALLOCATE( lonlat (2*ndac) )
      ALLOCATE( carr   (0:ncarrmax-1) )

      write(*,*) 'Allocating a resource map of dimension ',nrac,nrdn
      write(*,*) 'Allocating linear arrays of  dimension ',ndac

      ALLOCATE( arr   (2*nacout) )
      write(*,*) 'Allocating linear array of  dimension ',nacout

      open(11,file=lonlatfile,form='unformatted',
     .     access='direct',recl=8*ndac,status='old') 
      if(ift.eq.2) then
      open(13,file=outfile,form='unformatted',
     .     access='direct',recl=4*nacout,status='unknown') 
      open(12,file=resfile,form='unformatted',
     .     access='direct',recl=4*nrac,status='unknown') 
      else
      open(13,file=outfile,form='unformatted',
     .     access='direct',recl=8*nacout,status='unknown')
      open(12,file=resfile,form='unformatted',
     .     access='direct',recl=8*nrac,status='unknown') 
      endif

c read in the data

      write(*,*) 'reading input image ', resfile

      if(ift .eq.0) then
         do j = 0 , nrdn-1
            if(mod(j,256) .eq. 0) write(*,*) j
            read(12,rec=j+1,err=999) (carr(k),k=0,nrac-1)
            do k = 0 , nrac-1
               in_img(k,j) = real(carr(k))
               in_img(k+nrac,j) = aimag(carr(k))
            end do
         end do
      elseif(ift .eq.1) then
         do j = 0 , nrdn-1
            if(mod(j,256) .eq. 0) write(*,*) j
            read(12,rec=j+1,err=999) (in_img(k,j),k=0,2*nrac-1)
         end do
      else
         do j = 0 , nrdn-1
            if(mod(j,256) .eq. 0) write(*,*) j
            read(12,rec=j+1,err=999) (in_img(k,j),k=0,nrac-1)
         end do
      end if

 999  write(*,*) 'finished input file read ',j-1, ' lines'
      close(unit=12)

c do the interpolation
      jcnt = 0
      do j = srdno , srdno+nrdno-1, ilooksdn

         jcnt = jcnt + 1
         read(11,rec=j,err=998) (lonlat(k),k=1,2*ndac)

         if(mod(jcnt,100) .eq. 0) write(*,*) ' output line', jcnt,' dem line',j 

         if(iis .eq. 0) then ! nearest neighbour interpolation
            icnt = 0
            do i = sraco , sraco+nraco-1, ilooksac
               icnt = icnt + 1
               alon=(lonlat(i)-alon_first)/alon_step
               ocolval = alon/ilooksac
               alat=(lonlat(i+ndac)-alat_first)/alat_step
               orowval = alat/ilooksdn
               oi = nint(ocolval)
               oj = nint(orowval)
               if(.not.((oi .le. 0) .or. (oj .le. 0) .or. (oi .ge. nrac) .or. (oj .ge. nrdn))) then
                  arr(icnt) = in_img(oi,oj)
                  arr(icnt+nacout) = in_img(oi+nrac,oj)
               else             ! outside area to process
                  arr(icnt) = 0
                  arr(icnt+nacout) = 0
               endif
            end do

         elseif(iis .eq. 1) then !          bilinear interpolation
            icnt = 0
            do i = sraco , sraco+nraco-1, ilooksac
               icnt = icnt + 1
               alon=(lonlat(i)-alon_first)/alon_step
               ocolval = alon/ilooksac
               alat=(lonlat(i+ndac)-alat_first)/alat_step
               orowval = alat/ilooksdn
               oi = nint(ocolval)
               oj = nint(orowval)
               ifrac = (ocolval - oi)
               jfrac = (orowval - oj)
               if(ifrac .lt. 0.d0) then
                  oi = oi - 1
                  ifrac = (ocolval - oi)
               end if
               if(jfrac .lt. 0.d0) then
                  oj = oj - 1
                  jfrac = (orowval - oj)
               end if
               if(.not.((oi .le. 0) .or. (oi .ge. nrac-1) .or. (oj .le. 0) .or. (oj .ge. nrdn-1))) then
                  pt1(1) = 0.
                  pt1(2) = 0.
                  pt1(3) = in_img(oi,oj)
                  pt2(1) = 1.
                  pt2(2) = 0.
                  pt2(3) = in_img(oi+1,oj)
                  pt3(1) = 0.
                  pt3(2) = 1.
                  pt3(3) = in_img(oi,oj+1)
                  pt4(1) = 1.
                  pt4(2) = 1.
                  pt4(3) = in_img(oi+1,oj+1)
                  if(.not.((pt1(3) .eq. 0.) .or. (pt2(3) .eq. 0.) .or.
     $                 (pt3(3) .eq. 0.) .or. (pt4(3) .eq. 0.))) then
                     call bilinear(pt1,pt2,pt3,pt4,sngl(ifrac)
     $                    ,sngl(jfrac),arr(icnt))
                     pt1(1) = 0.
                     pt1(2) = 0.
                     pt1(3) = in_img(oi+nrac,oj)
                     pt2(1) = 1.
                     pt2(2) = 0.
                     pt2(3) = in_img(oi+1+nrac,oj)
                     pt3(1) = 0.
                     pt3(2) = 1.
                     pt3(3) = in_img(oi+nrac,oj+1)
                     pt4(1) = 1.
                     pt4(2) = 1.
                     pt4(3) = in_img(oi+1+nrac,oj+1)
                     call bilinear(pt1,pt2,pt3,pt4,sngl(ifrac)
     $                    ,sngl(jfrac),arr(icnt+nacout))
                  else
                     arr(icnt) = 0.
                     arr(icnt+nacout) = 0.
                  end if
               else
                  arr(icnt) = 0.
                  arr(icnt+nacout) = 0.
               end if
            end do
         elseif(iis .eq. 2) then !          sinc interpolation
            icnt = 0
            do i = sraco , sraco+nraco-1, ilooksac
               icnt = icnt + 1
               alon=(lonlat(i)-alon_first)/alon_step
               ocolval = alon/ilooksac
               alat=(lonlat(i+ndac)-alat_first)/alat_step
               orowval = alat/ilooksdn
               oi = nint(ocolval)
               oj = nint(orowval)
               ifrac = (ocolval - oi)
               jfrac = (orowval - oj)
               if(ifrac .lt. 0.d0) then
                  oi = oi - 1
                  ifrac = (ocolval - oi)
               end if
               if(jfrac .lt. 0.d0) then
                  oj = oj - 1
                  jfrac = (orowval - oj)
               end if
               
               if(.not.(oi .lt. 4 .or. oi .ge. nrac-3 .or. oj .lt. 4 .or
     $              . oj .ge. nrdn-3)) then
                  arr(icnt)      = interp(oi, oj, ifrac, jfrac, in_img, nrac, 0)
                  arr(icnt+nacout) = interp(oi, oj, ifrac, jfrac, in_img, nrac, nrac)
               else
                  arr(icnt) = 0.
                  arr(icnt+nacout) = 0.
               end if
            end do

         end if

         if(ift .eq. 0) then
            do k = 1, nacout 
               carr(k-1) = cmplx(arr(k),arr(k+nacout))
            end do
            write(13,rec=jcnt) (carr(k),k=0,nacout-1)
         elseif(ift .eq.1) then
            write(13,rec=jcnt) (arr(k),k=1,2*nacout)
         else
            write(13,rec=jcnt) (arr(k),k=1,nacout)
         end if
      end do
         
 998  write(*,*) 'finished output',jcnt, ' lines, dem line', j
      close(unit=11)
      close(unit=13)
      end

CPOD      
CPOD=pod
CPOD
CPOD=head1 USAGE
CPOD
CPOD usage: rect cmd_file 
CPOD
CPOD where cmd_file is the name of the command file (= RDF ascii formatted file).
CPOD
CPOD=head1 FUNCTION
CPOD
CPOD FUNCTIONAL DESCRIPTION:  This program resamples an image based
CPOD    on a lookup table of range and azimuth coordinates. In the program
CPOD    it is called a latlong file, but in actuality, the values
CPOD    of the lookup map are range/azimuth coordinate pairs arranged
CPOD    on a lat/long grid.
CPOD
CPOD=head1 ROUTINES CALLED
CPOD
CPOD bilinear
CPOD interp
CPOD
CPOD=head1 CALLED BY
CPOD
CPOD
CPOD=head1 FILES USED
CPOD
CPOD Program reads in an RDF command file; template listing:
CPOD
CPOD Longitude-Latitude Lookup File Name    (-) = rdmap.dat       ! dimension of simulation output
CPOD Lookup File Dimensions                 (-) = 6000 6800       ! across, down
CPOD Input Image File Name                  (-) = north.sim       ! dimension of simulation output
CPOD Input File Dimensions                  (-) = 6560 7200       ! across, down
CPOD Output Image File Name                 (-) = north.sim.rect  ! dimension of interferogram
CPOD Lookup File Start Sample for Output    (-) = 1
CPOD Number of Samples for Output           (-) = 2000
CPOD Lookup File Start Line for Output      (-) = 1000
CPOD Number of Lines for Output             (-) = 2000
CPOD Skip Every N Lookup points Across-Down (-) = 1
CPOD File Type                              (-) = RMG             ! [RMG, COMPLEX]                  
CPOD Interpolation Method                   (-) = NN              ! [NN, Bilinear, Sinc]
CPOD
CPOD Program reads a flat, binary, image files, in either
CPOD RMG format or 8bytes complex
CPOD 
CPOD=head1 FILES CREATED
CPOD
CPOD see "Output Image File Name" RDF entry for the Affine Transformed SLC
CPOD
CPOD=head1 DIAGNOSTIC FILES
CPOD
CPOD=head1 HISTORY
CPOD
CPOD Original Routines: P.A.Rosen
CPOD
CPOD=head1 LAST UPDATE
CPOD Date Changed        Reason Changed 
CPOD ------------       ----------------
CPOD
CPOF CPOD: par Dec 22 '03
CPOD=cut
