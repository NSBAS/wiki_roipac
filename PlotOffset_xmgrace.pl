#!/usr/bin/perl -w

$OffsetFile = shift;

$SetAppearance = "-pexec \"s0 line type 0\" -pexec \"s0 symbol 1\" -pexec \"s0 symbol size 0.25\" ";

$Cmd  = 'xmgrace -pexec "ARRANGE(2,2,0.1,0.2,0.2)"';
$Cmd .= " -graph 0 -block $OffsetFile -bxy 1:2 $SetAppearance";
$Cmd .= ' -pexec \'xaxis label "Range Pixel" \' -pexec \'yaxis label "Range Offset" \' ';
$Cmd .= " -graph 1 -block $OffsetFile -bxy 1:4 $SetAppearance";
$Cmd .= ' -pexec \'xaxis label "Range Pixel" \' -pexec \'yaxis label "Azimuth Offset" \' ';
$Cmd .= " -graph 2 -block $OffsetFile -bxy 3:2 $SetAppearance";
$Cmd .= ' -pexec \'xaxis label "Azimuth Pixel" \' -pexec \'yaxis label "Range Offset" \' ';
$Cmd .= " -graph 3 -block $OffsetFile -bxy 3:4 $SetAppearance";
$Cmd .= ' -pexec \'xaxis label "Azimuth Pixel" \' -pexec \'yaxis label "Azimuth Offset" \' ';

system $Cmd;

