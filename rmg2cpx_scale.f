c  
      parameter(nxm=12000,nym=50000)
      real amp(nxm),phs(nxm)
      complex cpx(nxm),a
      character*512 nom_input,nom_output,buff
      integer seed 

      iseed= 86456
c
      call getarg(1,nom_input)
      call getarg(2,nom_output)
      call getarg(3,buff)
      read(buff,*)iwidth
      call getarg(4,buff)
      read(buff,*)ascale
c
      open(1,file=nom_input,status='old',form='unformatted',
     $   access='direct',recl=4*iwidth)
      open(2,file=nom_output,status='unknown',form='unformatted',
     $   access='direct',recl=8*iwidth)
      do j=1,nym
        irec=2*j-1
        read(1,rec=irec,err=10)(amp(i),i=1,iwidth)
        irec=2*j
        read(1,rec=irec)(phs(i),i=1,iwidth)
        do i=1,iwidth
c           if(abs(amp(i)).lt.1.e-6)then
c             amp(i)=0.01
c             phs(i)=ascale*4*(random(iseed)-0.5)
c           endif
           a=(0.,1.)*(phs(i)/ascale)
           cpx(i)=amp(i)*cexp(a)
        enddo
        write(2,rec=j)(cpx(i),i=1,iwidth)
       enddo
10     close(1)
       close(2)
c
       stop
       end

      FUNCTION RANDOM (N0)
 
C- On initialise N0 a une valeur quelconque ENTIERE 
C-RANDOM renvoie un REEL compris entre 0.0 et 1.0
 
      REAL      RANDOM
      INTEGER   N0, K, CONST1, CONST2, CONST3, CONST4
      PARAMETER (CONST1 = 127773)
      PARAMETER (CONST2 = 16807)
      PARAMETER (CONST3 = 2836)
      PARAMETER (CONST4 = 2147483647)
 
      K = N0 / CONST1
      N0 = CONST2 * (N0 - K * CONST1) - K * CONST3
      IF (N0 .LT. 0) N0 = N0 + CONST4
      RANDOM = REAL(N0) / REAL(CONST4)
 
      END
