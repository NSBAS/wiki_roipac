/*  *** JPL/Caltech Repeat Orbit Interferometry (ROI) Package ***   */

#include <stdio.h>
#include <math.h>
#include <string.h>

main(int argc, char *argv[]){
  /***************************/
  /***  Input Variables    ***/
  /***************************/
  char InputFile[1000];
  FILE *InputFP;

  char  GeoFile[1000],   LookupFile[1000],  OutFile[1000];
  FILE  *GeoFP,         *LookupFP,         *OutFP;
  float *GeoData,       *LookupData,       *OutData;
  float GeoLon0,         LookupLon0;
  float GeoLat0,         LookupLat0;
  float GeoLonStep,      LookupLonStep;
  float GeoLatStep,      LookupLatStep;
  int   GeoWidth,        LookupWidth,       OutWidth;
  int   GeoLength,       LookupLength,      OutLength;
  int   OutRangeIndex,   OutAzimuthIndex;
  int   LookupLonIndex,  LookupLatIndex, LookupIndex;
  float GeoLon,          GeoLat;
  /***************************/
  /***  Local Variables    ***/
  /***************************/
  int i,j;

  /***************************/
  /***      Input          ***/
  /***************************/
  if(argc < 2){
    printf("   usage:%s InputFile\n",argv[0]);
    exit(1);
    }
  
  strcpy(InputFile,argv[1]);
  if((InputFP=fopen(InputFile,"r"))==NULL){
    fprintf(stderr,"file %s not open\n",InputFile);
    exit(1);
    }
  fscanf(InputFP,"%s",GeoFile);
  fscanf(InputFP,"%f %f %d",&GeoLon0,&GeoLonStep,&GeoWidth);
  fscanf(InputFP,"%f %f %d",&GeoLat0,&GeoLatStep,&GeoLength);
  fscanf(InputFP,"%s",LookupFile);
  fscanf(InputFP,"%f %f %d",&LookupLon0,&LookupLonStep,&LookupWidth);
  fscanf(InputFP,"%f %f %d",&LookupLat0,&LookupLatStep,&LookupLength);
  fscanf(InputFP,"%s",OutFile);
  fscanf(InputFP,"%d %d",&OutWidth,&OutLength);


  /***************************/
  /***    Read Data        ***/
  /***************************/
  if((GeoFP=fopen(GeoFile,"r"))==NULL){
    fprintf(stderr,"file %s not open\n",GeoFile);
    exit(1);
    }
   if((LookupFP=fopen(LookupFile,"r"))==NULL){
     fprintf(stderr,"file %s not open\n",LookupFile);
     exit(1);
     }
  if((OutFP=fopen(OutFile,"w"))==NULL){
    fprintf(stderr,"file %s not open\n",OutFile);
    exit(1);
    }

   fseek(GeoFP,0L,SEEK_END);
   GeoLength=ftell(GeoFP)/(sizeof(float)*GeoWidth);
   rewind(GeoFP);
   fseek(LookupFP,0L,SEEK_END);
   LookupLength=ftell(LookupFP)/(sizeof(float)*LookupWidth*2);
   rewind(LookupFP);

//  GeoData=   (float *)malloc(sizeof(float)*GeoWidth*GeoLength);
  GeoData=   (float *)malloc(sizeof(float)*GeoWidth);
  
//  LookupData=(float *)malloc(2*sizeof(float)*LookupWidth*LookupLength);
  LookupData=(float *)malloc(2*sizeof(float)*LookupWidth);

  OutData=   (float *)malloc(sizeof(float)*OutWidth*OutLength);
  
  if((GeoData==NULL) ||(LookupData==NULL) ||(OutData==NULL)){
    fprintf(stderr,"Could not allocate memory\n");
    exit(1);
    }

// read whole file (memory consuming!)
//  fread(GeoData,sizeof(float),GeoWidth*GeoLength,GeoFP);
//  fread(LookupData,sizeof(float),2*LookupWidth*LookupLength,LookupFP);

  for(j=0;j<OutLength;j++){
    for(i=0;i<OutWidth;i++){
      OutData[i+j*OutWidth]=0;
      }
    }
 
  for(j=0;j<GeoLength;j++){
    // read only one line
    fread(GeoData,sizeof(float),GeoWidth,GeoFP);
    fread(LookupData,sizeof(float),2*LookupWidth,LookupFP);
    GeoLat=GeoLat0+j*GeoLatStep;
    LookupLatIndex=(int)((GeoLat-LookupLat0)/LookupLatStep);
    if ((LookupLatIndex<0)||(LookupLatIndex>=LookupLength)) continue;
    for(i=0;i<GeoWidth;i++){
      GeoLon=GeoLon0+i*GeoLonStep;
      LookupLonIndex=(int)((GeoLon-LookupLon0)/LookupLonStep);
      if ((LookupLonIndex<0)||(LookupLonIndex>=LookupWidth)) continue;
//      LookupIndex=(LookupLonIndex+LookupWidth*LookupLatIndex*2);
      LookupIndex=i;

      OutRangeIndex=  (int)LookupData[LookupIndex];
      OutAzimuthIndex=(int)LookupData[LookupIndex+LookupWidth];
     
  if((OutAzimuthIndex<0)||(OutAzimuthIndex>(OutLength-1))
   ||(OutRangeIndex<0)||(OutRangeIndex>(OutWidth-1))   
   ||((OutAzimuthIndex*OutWidth+OutRangeIndex)>(OutWidth*OutLength-1))){
      continue;
  }

      if ( i%500==0 && j%500==0 ) {
	printf("%d %d %d %d %d %f\n",i,j,LookupIndex,OutRangeIndex,OutAzimuthIndex,GeoData[i]);
      }

//      OutData[OutAzimuthIndex*OutWidth+OutRangeIndex]=GeoData[i+j*GeoWidth];
      OutData[OutAzimuthIndex*OutWidth+OutRangeIndex]=GeoData[i];

      }
    }
  OutData[0]=0;

  fwrite(OutData,sizeof(float),OutWidth*OutLength,OutFP);
}

//POD=pod
//POD
//POD=head1 USAGE
//POD
//POD Usage: geo2radar  InputFile
//POD        where InputFile (geo2radar.in) is itself produced by perl script ../INT_SCR/geo2radar.pl
//POD  
//POD=head1 FUNCTION
//POD
//POD FUNCTIONAL DESCRIPTION:  "geo2radr" Maps an input data into an output image according to a look up
//POD table in "range" (along records=i,ii,longitude) and "azimuth" (record to record=j,jj,latitude).  
//POD The lookup table gives the output location of each input data: i(ii,jj) and j(ii,jj)
//POD 
//POD  OutData[i(ii,jj),j(ii,jj)] = InData(ii,jj) 
//POD 
//POD The array indices in the input and LU tables are computed assuming regular Longitude(i), Latitude(j) samplings.
//POD  
//POD=head1 ROUTINES CALLED
//POD
//POD none
//POD
//POD=head1 CALLED BY
//POD
//POD perl script geo2radar.pl
//POD
//POD=head1 FILES USED
//POD
//POD listing for "geo2radar.in"
//POD =========================
//POD InData
//POD GeoLon0 GeoLonStep GeoWidth
//POD GeoLat0 GeoLatStep GeoLength
//POD InTable
//POD LookupLon0 LookupLonStep vWidth
//POD LookupLat0 LookupLatStep LookupLength
//POD OutData
//POD OutWidth OutLength
//POD 
//POD Infile r*4/float.record Length = GeoWidth, number of records = GeoLength
//POD Intable, RMG: Range & Azimuth 2 X r*4/float, record Length = LookupWidth, number of records = LookupLength
//POD
//POD=head1 FILES CREATED
//POD
//POD "Outfile" r*4/Float, record Length = OutWidth, number of records = OutLength
//POD 
//POD=head1 DIAGNOSTIC FILES
//POD
//POD
//POD=head1 HISTORY
//POD
//POD Routines written by Francois Rogez
//POD
//POD=head1 LAST UPDATE
//POD  Date Changed        Reason Changed 
//POD  ------------       ----------------
//POD
//POD trm Feb 2nd '04
//POD=cut
