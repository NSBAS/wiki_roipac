      byte in(16384)
      complex a(16384),b(16384),prod(16384)
      real acc(16384)
      character*512 file

      print '(a,$)',' Input file: '
      read '(a)',file
      print *,'Line length in bytes, first, number of lines: '
      read *,len,i0,n
      print '(a,$)',' mean level, PRF ? '
      read *,xmn, prf

      open(21,file=file,access='direct',recl=len)
      do k=1,8192
         prod(k)=cmplx(0.,0.)
      end do

      do i=i0,i0+n-1
         read(21,rec=i,err=99)(in(k),k=1,len)
         do k=413,len, 2
            a((k-412)/2+1)=cmplx((in(k).and.255)-xmn,(in(k+1).and.255)
     $           -xmn)
         end do
c     get second line
         read(21,rec=i+1,err=99)(in(k),k=1,len)
         do k=413,len,2
            b((k-412)/2+1)=cmplx((in(k).and.255)-xmn,(in(k+1).and.255)
     $           -xmn)
         end do
         do k=1, (len-412)/2
            prod(k)=prod(k)+conjg(a(k))*b(k)
         end do
      end do
c     convert to frequencies in cycles
 99   open(22,file='dop.out')
      do k=1, (len-412)/2
         acc(k)=atan2(aimag(prod(k)),real(prod(k)))
         acc(k)=acc(k)/2/3.14159265
         write(22,*)k,acc(k),acc(k)*prf
      end do

      end

