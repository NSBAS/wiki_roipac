      program rectify
c     
c*****************************************************************************
c**   
c**   FILE NAME: rect.f
c**   
c**   DATE WRITTEN: 27-Nov-98
c**   
c**   PROGRAMMER: P.A.Rosen
c**   
c**   FUNCTIONAL DESCRIPTION:  This program adjusts an image
c**   by affine transformation and interpolation
c**   
c**   UPDATE LOG:
c**   
c*****************************************************************************
      
      
      implicit none

c      integer CMAX, RMAX
c      parameter (CMAX = 7000, RMAX = 7200)
c      real*4  rvs(0:2*CMAX-1,0:RMAX-1)
c      complex*8  carr(0:20000)
c      real*4  arr(0:40000)
      REAL*4, DIMENSION(:,:), ALLOCATABLE :: rvs
c     For byte format     *****GP 01-05******
      INTEGER*1, DIMENSION(:), ALLOCATABLE :: barr
      REAL*4, DIMENSION(:), ALLOCATABLE :: arr
      COMPLEX*8, DIMENSION(:), ALLOCATABLE :: carr
      real*4  pt1(3),pt2(3),pt3(3),pt4(3)
      real*8  colval, rowval, ocolval, orowval
      real*8  ifrac, jfrac
      real*8  a,b,c,d,e,f,xx1,xx2,yy1,yy2,xy1,xy2
      real*4  interp,prod

      integer oi, oj, i, j, k, ift, iis
      integer iargc, ndac, nddn, nrac, nrdn

      character*512 fname, infile, outfile, intstyle, filetype

      integer rdflen
      character*255 rdfval,rdftmp
      character*255 rdfcullsp,rdfdata

      save rvs

      if(iargc() .eq. 0) then
         write(*,*) 'usage: rect rect.rdf'
         stop
      end if

      call getarg(1,fname)

      call rdf_init('ERRFILE=SCREEN')
      write(6,'(a)') 'Reading command file data...'
      call rdf_read(fname)

      rdftmp=rdfval('Input Image File Name','-')
      read(unit=rdftmp,fmt='(a)') infile
      rdftmp=rdfval('Output Image File Name','-')
      read(unit=rdftmp,fmt='(a)') outfile
      rdftmp=rdfval('Input Dimensions','-')
      read(unit=rdftmp,fmt=*) ndac, nddn
      rdftmp=rdfval('Output Dimensions','-')
      read(unit=rdftmp,fmt=*) nrac, nrdn
      rdftmp=rdfval('Affine Matrix Row 1','-')
      read(unit=rdftmp,fmt=*) a, b
      rdftmp=rdfval('Affine Matrix Row 2','-')
      read(unit=rdftmp,fmt=*) c, d
      rdftmp=rdfval('Affine Offset Vector','-')
      read(unit=rdftmp,fmt=*) e, f
      rdftmp=rdfval('Quadratic terms Row 1','-')
      read(unit=rdftmp,fmt=*)xx1,yy1,xy1
      rdftmp=rdfval('Quadratic terms Row 2','-')
      read(unit=rdftmp,fmt=*)xx2,yy2,xy2
      rdftmp=rdfval('File Type','-')
      read(unit=rdftmp,fmt='(a)') filetype
      rdftmp=rdfval('Interpolation Method','-')
      read(unit=rdftmp,fmt='(a)') intstyle

c      if(ndac .gt. CMAX) stop 'Increase column array dimension in rect' 
c      if(nddn .gt. RMAX) stop 'Increase row array dimension in rect'

      ALLOCATE( rvs(0:2*ndac-1,0:nddn-1) )
      write(*,*) 'Allocated a map of dimension ',ndac,nddn
      ALLOCATE( carr(0:2*ndac-1) )
      write(*,*) 'Allocated an array of dimension ',ndac
      ALLOCATE( arr(0:2*nrac-1) )
      write(*,*) 'Allocated array of dimension ',nrac
c     For byte format     *****GP 01-05******
      ALLOCATE( barr(0:2*ndac-1) )
      write(*,*) 'Allocated an array of dimension ',ndac


      ift = 0
      if(index(filetype,'RMG') .ne. 0)then
         ift = 1
         write (*,*)  'Assuming RMG file type '
c      For byte format  *****GP 01-05******
      elseif(index(filetype,'BYTE') .ne. 0)then
         ift = 2
         write (*,*)  'Assuming byte file type '
      else
         write (*,*)  'Assuming complex file type '
      endif

      iis = 0
      if(index(intstyle,'Bilinear') .ne. 0)then
         iis = 1
         write (*,*)  'Assuming Bilinear Interpolation '
      elseif(index(intstyle,'Sinc') .ne. 0)then
         iis = 2
         write (*,*)  'Assuming Sinc Interpolation '
      else
         write (*,*)  'Assuming Nearest Neighbor '
      end if

      write (*,*)  'opening files ...'
c      For non byte format  *****GP 01-05******
      if(ift .ne. 2) then
         open(11,file=infile,form='unformatted',
     .     access='direct',recl=8*ndac,status='old') 
         open(12,file=outfile,form='unformatted',
     .     access='direct',recl=8*nrac,status='unknown') 
      else
c      For byte format  *****GP 01-05******
         open(11,file=infile,form='unformatted',
     .     access='direct',recl=ndac,status='old') 
         open(12,file=outfile,form='unformatted',
     .     access='direct',recl=nrac,status='unknown') 
      end if

c    forcing NN interpolation for byte format
      if(ift .eq. 2) then    
          iis = 0
      end if

c read in the data

      write (*,*)  'reading data ...'

      if(ift .eq. 0) then
         do j = 0 , nddn-1
            if(mod(j,256) .eq. 0) write (*,*)  j
            read(11,rec=j+1,err=999) (carr(k),k=0,ndac-1)
            do k = 0 , ndac -1
               rvs(k,j) = real(carr(k))
               rvs(k+ndac,j) = aimag(carr(k))
            end do
         end do
      elseif(ift .eq. 1) then
         do j = 0 , nddn-1
            if(mod(j,256) .eq. 0) write (*,*)  j
            read(11,rec=j+1,err=999) (rvs(k,j),k=0,2*ndac-1)
         end do
      else
         do j = 0 , nddn-1
            if(mod(j,256) .eq. 0) write (*,*)  j
            read(11,rec=j+1,err=999) (barr(k),k=0,ndac-1)
            do k = 0 , ndac -1
               rvs(k,j) = barr(k)
            end do
         end do
      end if

 999  write (*,*)  'finished read ',j,' now interpolating ...'

c do the interpolation

      do j = 0 , nrdn-1
         if(mod(j,100) .eq. 0) write (*,*)  j
         rowval = dble(j)

         if(iis .eq. 0) then    !  nearest neighbor

            do i = 0 , nrac-1
               colval = dble(i)
               ocolval =  a * colval + b * rowval + e + xx1 * colval**2 + yy1 * rowval**2 + xy1 * colval*rowval
               orowval =  c * colval + d * rowval + f + xx2 * colval**2 + yy2 * rowval**2 + xy2 * colval*rowval
               oi = nint(ocolval)
               oj = nint(orowval)
               if(.not.(oi .lt. 0 .or. oi .ge. ndac .or. oj .lt. 0 .or
     $              . oj .ge. nddn)) then
                  arr(i) = rvs(oi,oj)
                  arr(i+nrac) = rvs(oi+ndac,oj)
               else
                  arr(i) = 0.
                  arr(i+nrac) = 0.
               end if
            end do
            
         elseif(iis. eq. 1) then !          bilinear interpolation

            do i = 0 , nrac-1
               colval = dble(i)
               ocolval =  a * colval + b * rowval + e + xx1 * colval**2 + yy1 * rowval**2 + xy1 * colval*rowval
               orowval =  c * colval + d * rowval + f + xx2 * colval**2 + yy2 * rowval**2 + xy2 * colval*rowval
               oi = nint(ocolval)
               oj = nint(orowval)
               ifrac = (ocolval - oi)
               jfrac = (orowval - oj)
               if(ifrac .lt. 0.d0) then
                  oi = oi - 1
                  ifrac = (ocolval - oi)
               end if
               if(jfrac .lt. 0.d0) then
                  oj = oj - 1
                  jfrac = (orowval - oj)
               end if
               if(.not.(oi .lt. 0 .or. oi .ge. ndac-1 .or. oj .lt. 0 .or
     $              . oj .ge. nddn-1)) then
               prod=abs(rvs(oi,oj)*rvs(oi+1,oj)*rvs(oi,oj+1)*rvs(oi+1,oj+1))
               if(prod.gt.1.e-12)then
                  pt1(1) = 0.
                  pt1(2) = 0.
                  pt1(3) = rvs(oi,oj)
                  pt2(1) = 1.
                  pt2(2) = 0.
                  pt2(3) = rvs(oi+1,oj)
                  pt3(1) = 0.
                  pt3(2) = 1.
                  pt3(3) = rvs(oi,oj+1)
                  pt4(1) = 1.
                  pt4(2) = 1.
                  pt4(3) = rvs(oi+1,oj+1)
                  call bilinear(pt1,pt2,pt3,pt4,sngl(ifrac),sngl(jfrac),arr(i))
                  pt1(1) = 0.
                  pt1(2) = 0.
                  pt1(3) = rvs(oi+ndac,oj)
                  pt2(1) = 1.
                  pt2(2) = 0.
                  pt2(3) = rvs(oi+1+ndac,oj)
                  pt3(1) = 0.
                  pt3(2) = 1.
                  pt3(3) = rvs(oi+ndac,oj+1)
                  pt4(1) = 1.
                  pt4(2) = 1.
                  pt4(3) = rvs(oi+1+ndac,oj+1)
                  call bilinear(pt1,pt2,pt3,pt4,sngl(ifrac),sngl(jfrac),arr(i+nrac))
               else
                  arr(i) = 0.
                  arr(i+nrac) = 0.
               end if
               else
                  arr(i) = 0.
                  arr(i+nrac) = 0.
               end if
            end do
            

         elseif(iis. eq. 2) then !          sinc interpolation

            do i = 0 , nrac-1
               colval = dble(i)
               ocolval =  a * colval + b * rowval + e + xx1 * colval**2 + yy1 * rowval**2 + xy1 * colval*rowval
               orowval =  c * colval + d * rowval + f + xx2 * colval**2 + yy2 * rowval**2 + xy2 * colval*rowval
               oi = nint(ocolval)
               oj = nint(orowval)
               ifrac = (ocolval - oi)
               jfrac = (orowval - oj)
               if(ifrac .lt. 0.d0) then
                  oi = oi - 1
                  ifrac = (ocolval - oi)
               end if
               if(jfrac .lt. 0.d0) then
                  oj = oj - 1
                  jfrac = (orowval - oj)
               end if
               
               if(.not.(oi .lt. 4 .or. oi .ge. ndac-3 .or. oj .lt. 4 .or
     $              . oj .ge. nddn-3)) then
                  arr(i)      = interp(oi, oj, ifrac, jfrac, rvs, ndac, 0)
                  arr(i+nrac) = interp(oi, oj, ifrac, jfrac, rvs, ndac, ndac)
               else
                  arr(i) = 0.
                  arr(i+nrac) = 0.
               end if
            end do

         end if

         if(ift .eq. 0) then
            do k = 0 , nrac -1
               carr(k) = cmplx(arr(k),arr(k+nrac))
            end do
            write(12,rec=j+1) (carr(k),k=0,nrac-1)
         elseif(ift .eq. 1) then
            write(12,rec=j+1) (arr(k),k=0,2*nrac-1)
         else
            do k = 0 , nrac -1
               barr(k) = arr(k)
            end do
            write(12,rec=j+1) (barr(k),k=0,nrac-1)
         end if
         
      end do
      
      close(unit=12)
      end
