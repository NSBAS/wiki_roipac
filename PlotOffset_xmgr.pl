#!/usr/bin/perl -w

$OffsetFile = shift;
$CullFile = shift;

$SetAppearance1 = "-pexec \"s0 linestyle 0\" -pexec \"s0 symbol 2\" -pexec \"s0 symbol size 0.25\" ";
$SetAppearance2 = "-pexec \"s1 linestyle 0\" -pexec \"s1 symbol 2\" -pexec \"s1 symbol size 0.25\" -pexec \"s1 symbol color 2\"";

$Cmd  = 'xmgr -arrange 2 2 ';

$Cmd .= " -graph 0 -block $OffsetFile -bxy 1:2 $SetAppearance1";
$Cmd .= ' -pexec \'subtitle "Ra Off - Ra Pix" \' ';
$Cmd .= " -graph 1 -block $OffsetFile -bxy 1:4 $SetAppearance1";
$Cmd .= ' -pexec \'subtitle "Az off - Ra Pix" \' ';
$Cmd .= " -graph 2 -block $OffsetFile -bxy 3:2 $SetAppearance1";
$Cmd .= ' -pexec \'subtitle "Ra off - Az Pix" \' ';
$Cmd .= " -graph 3 -block $OffsetFile -bxy 3:4 $SetAppearance1";
$Cmd .= ' -pexec \'subtitle "Az off - Az Pix" \' ';

$Cmd .= " -graph 0 -block $CullFile -bxy 1:2 $SetAppearance2";
$Cmd .= ' -pexec \'subtitle "Ra Off - Ra Pix" \' ';
$Cmd .= " -graph 1 -block $CullFile -bxy 1:4 $SetAppearance2";
$Cmd .= ' -pexec \'subtitle "Az off - Ra Pix" \' ';
$Cmd .= " -graph 2 -block $CullFile -bxy 3:2 $SetAppearance2";
$Cmd .= ' -pexec \'subtitle "Ra off - Az Pix" \' ';
$Cmd .= " -graph 3 -block $CullFile -bxy 3:4 $SetAppearance2";
$Cmd .= ' -pexec \'subtitle "Az off - Az Pix" \' ';


system $Cmd;

