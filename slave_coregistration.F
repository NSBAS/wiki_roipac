c****************************************************************

      Program resamp_roi 

c****************************************************************
c     INPUT FICHIER d OFFSETS SYNTHETHIQUE CONSTRUIT A PARTIR D UN .UNW
c**********************************************************************
c**     
c**   FILE NAME: resamp_roi.F
c**     
c**   DATE WRITTEN: Long, long ago. (March 16, 1992)
c**     
c**   PROGRAMMER: Charles Werner, Paul Rosen and Scott Hensley
c**     
c**   FUNCTIONAL DESCRIPTION: Interferes two SLC images 
c**   range, azimuth interpolation with a quadratic or sinc interpolator 
c**   no circular buffer is used, rather a batch algorithm is implemented
c**   The calculation of the range and azimuth offsets is done for
c**   each of the data sets in the offset data file. As soon as the
c**   current line number exceeds the range line number for one of the
c**   data sets in the offset data file, the new lsq coefficients are
c**   to calculate the offsets for any particular range pixel. 
c**     
c**   ROUTINES CALLED:
c**     
c**   NOTES: 
c**     
c**   UPDATE LOG:
c**
c**   Date Changed        Reason Changed 
c**   ------------       ----------------
c**     20-apr-92    added removal/reinsertion of range phase slope to 
c**                  improve correlation
c**     11-may-92    added code so that the last input block of data is processed
c**                  even if partially full
c**     9-jun-92     modified maximum number of range pixels
c**     17-nov-92    added calculation of the range phase shift/pixel
c**     29-mar-93    write out multi-look images (intensity) of the two files 
c**     93-99        Stable with small enhancements changes
c**     Dec 99       Modified range interpolation to interpret (correctly)
c**                  the array indices to be those of image 2 coordinates.  
c**                  Previous code assumed image 1, and therefore used 
c**                  slightly wrong offsets for range resampling depending
c**                  on the gross offset between images.  Mods involve computing
c**                  the inverse mapping
c**     Aug 16, 04   This version uses MPI (Message Passing Interface)
c**                  to parallelize the resamp_roi sequential computations.
c**                  File Name is changed to resamp_roi.F in order to use
c**                  the Fortran compiler pre-processor to do conditional
c**                  compiling (#ifdef etc).  This code can be compiled for
c**                  either sequential or parallel uses. Compiler flag 
c**                  -DMPI_PARA is needed in order to pick up the MPI code.
c**
c*****************************************************************

      implicit none

c     INCLUDE FILES:

c     PARAMETER STATEMENTS:

      integer    NPP,MP
      parameter (NPP=10)

      real*8   PI
      integer  NP, NAZMAX, N_OVER, NBMAX, NLINESMAX
      parameter (PI=3.1415926535d0)
      parameter (NP=30000)	!maximum number of range pixels
      parameter (NLINESMAX=200000) ! maximum number of SLC lines
      parameter (NAZMAX=8)	        !number of azimuth looks
      parameter (N_OVER=2000)  !overlap between blocks
      parameter (NBMAX=200*NAZMAX+2*N_OVER) !number of lines in az interpol

      integer FL_LGT
      parameter (FL_LGT=8192*8)

      integer MAXDECFACTOR      ! maximum lags in interpolation kernels
      parameter(MAXDECFACTOR=8192)                        
      
      integer MAXINTKERLGH      ! maximum interpolation kernel length
      parameter (MAXINTKERLGH=8)
      
      integer MAXINTLGH         ! maximum interpolation kernel array size
      parameter (MAXINTLGH=MAXINTKERLGH*MAXDECFACTOR)

c     INPUT VARIABLES:
	
c     OUTPUT VARIABLES:

c     LOCAL VARIABLES:

cmp
      real*4 r_lard, r_ao0, r_ao1, r_aomin, r_aomax, dem(np,500), dem2(np,500)
      real*4 tecto(np,500), tecto2(np,500)
      real*4 r_rph
      integer ilect_dem, nlard, i00, irecl, icoazoff
      real*8 bt,bmt,bmmt

      logical ex
      character*512 f(5),a_cmdfile,a_offfile,a_temp
      character*512 a_coazoff, a_offtecto
      
      integer ierr, l1, l2, lc, line, iargc, iflatten
      integer ist, istoff, npl, npl2, nplo, nr, naz, nl
      integer ibs, ibe, irec, i_a1, i_r1, jrec, jrecp
      integer i, j, k, ii, ix, nb, jj, iadd,ii1,ii2
      integer int_az_off,nazt,nrat
      integer int_rd(0:NP-1)
      integer int_az(0:NP-1)
      integer ibfcnt    

      real*4  fintp(0:FL_LGT-1),f_delay
      real am(0:NP-1,0:NAZMAX-1),amm(0:NP-1)
      real bm(0:NP-1,0:NAZMAX-1),bmm(0:NP-1)
      complex abmm(0:NP-1)
      
      real*8 fr_rd(0:NP-1),fr_az(0:NP-1)

      real*8 wvl, cpp, rphs, aa1, rphs1, r_ro, r_ao
      real*8 slr, azoff1, rd, azs, wvl2, slr2,xx

      complex cm(0:NP-1)
      complex dm(0:NP-1)
      complex em(0:NP-1)
      real*8 fd(0:NP-1)
      
      complex a(0:NP-1),b(0:NP-1,0:NBMAX-1),tmp(0:NP-1)
      complex cc(0:NP-1),c(0:NP-1,0:NAZMAX-1),dddbuff(0:NP-1)
      complex rph(0:NP-1,0:NAZMAX-1)               !range phase correction
      complex sinc_eval

      real*8 ph1, phc, r_q
      real*8 f0,f1,f2,f3           !doppler centroid function of range poly file 1
      real*8 r_rancoef(NPP),r_azcoef(NPP)
      real*8 r_rancoef2(NPP),r_azcoef2(NPP)
      real*8 r_rancoefrr

      real*8 r_beta,r_relfiltlen,r_filter(0:MAXINTLGH),r_pedestal
      real*4 r_delay
      integer i_decfactor,i_weight,i_intplength,i_filtercoef

      real*4 t0, t1, t2, t3, t4, t5, t6


c     EQUIVALENCE STATEMENTS:

c     DATA STATEMENTS:

c     FUNCTION STATEMENTS:

      integer rdflen
      character*255 rdfval,rdftmp

c     SAVE STATEMENTS:

      save b,c,am,bm,rph


c     PROCESSING STEPS:

cc      write(6,*) ' XXX start timer'

      write(6,*) ' '       
      write(6,*)  ' << RTI Interpolation and Cross-correlation (quadratic) v1.0 >>'
      write(6,*) ' ' 

        if(iargc() .lt. 1)then
          write(6,'(a)') 'Usage: slave_coregistration cmd_file'
          write(6,*) ' '
          stop
        endif

        call getarg(1,a_cmdfile)
      
      call rdf_init('ERRFILE=SCREEN')
      call rdf_init('ERROR_SCREEN=ON')
      call rdf_init('ERROR_OUTPUT=rdf_errors.log')
      call rdf_init('COMMENT= ! ! !')
      write(6,'(a)') 'Reading command file data...'
      call rdf_read(a_cmdfile)

c     read parameters from command file

      rdftmp=rdfval('SLC Image File 1','-')
      read(unit=rdftmp,fmt='(a)') f(1)
      rdftmp=rdfval('Number of Range Samples Image 1','-')
      read(unit=rdftmp,fmt=*) npl
      rdftmp=rdfval('SLC Image File 2','-')
      read(unit=rdftmp,fmt='(a)') f(2)
      rdftmp=rdfval('Number of Range Samples Image 2','-')
      read(unit=rdftmp,fmt=*) npl2
      if((npl .gt. NP) .or. (npl2 .gt. NP)) then
         write(6,*) 'ERROR:number of pixels greater than array in resamp_roi'
         stop
      end if
      rdftmp=rdfval('Coregistered slc File','-')
      read(unit=rdftmp,fmt='(a)') f(3)
      rdftmp=rdfval('Starting Line, Number of Lines, and First Line Offset','-')
      read(unit=rdftmp,fmt=*) ist, nl, istoff
      rdftmp=rdfval('Doppler Cubic Fit Coefficients - PRF Units','-')
      read(unit=rdftmp,fmt=*) f0,f1,f2,f3
      rdftmp=rdfval('Radar Wavelength','m')
      read(unit=rdftmp,fmt=*) WVL
      rdftmp=rdfval('Slant Range Pixel Spacing','m')
      read(unit=rdftmp,fmt=*) SLR
      rdftmp=rdfval('Number of Range and Azimuth Looks','-')
      read(unit=rdftmp,fmt=*) NR,NAZ
      rdftmp=rdfval('Flatten with offset fit?','-')
      read(unit=rdftmp,fmt='(a)') a_temp
cmp
c parameter concerning the offset function (residues + model)
      rdftmp=rdfval('Elevation or unw File Name','-')
      read(unit=rdftmp,fmt='(a)') a_offfile
      rdftmp=rdfval('Affine Matrix Row 1','-')
      read(unit=rdftmp,fmt=*) r_rancoef(2), r_rancoef(3)
      rdftmp=rdfval('Affine Matrix Row 2','-')
      read(unit=rdftmp,fmt=*) r_azcoef(2), r_azcoef(3)
      rdftmp=rdfval('Affine Offset Vector','-')
      read(unit=rdftmp,fmt=*) r_rancoef(1), r_azcoef(1)
      rdftmp=rdfval('Quadratic terms Row 1','-')
      read(unit=rdftmp,fmt=*) r_rancoef(5), r_rancoef(6), r_rancoef(4)
      rdftmp=rdfval('Quadratic terms Row 2','-')
      read(unit=rdftmp,fmt=*)  r_azcoef(5),  r_azcoef(6),  r_azcoef(4)
      rdftmp=rdfval('Elevation coefficient in range','-')
      read(unit=rdftmp,fmt=*) r_rancoef(7)
      rdftmp=rdfval('Number of azimuth look of elevation file','-')
      read(unit=rdftmp,fmt=*) NAZT
      rdftmp=rdfval('Number of range look of elevation file','-')
      read(unit=rdftmp,fmt=*) NRAT
      rdftmp=rdfval('Radar Wavelength slave','m')
      read(unit=rdftmp,fmt=*) WVL2
      rdftmp=rdfval('Range Pixel size slave','m')
      read(unit=rdftmp,fmt=*) SLR2
      rdftmp=rdfval('Coregistration with Azimuth offset file?','-')
      read(unit=rdftmp,fmt='(a)') a_coazoff
c
      icoazoff = 0
      if(index(a_coazoff,'Yes') .ne. 0)then
         icoazoff = 1
         rdftmp=rdfval('Azimuth offset file Name','-')
      read(unit=rdftmp,fmt='(a)') a_offtecto
      endif
c
      r_rancoef2(7)=r_rancoef(7)*SLR/SLR2
c
cmp
c 1 is removed from the affine function
      r_rancoef(2)=r_rancoef(2)-1.
      r_azcoef(3)= r_azcoef(3)-1.
c on ote l effet du a la difference de freq.
      r_rancoefrr=r_rancoef(2)+(1.-SLR/SLR2)
      print*,'r_rancoefrr ',r_rancoefrr
c
c coefficients transforamtion to optain az. and range offsets parameterization
c as a function of azimuth of file 2 and range of file 1
c
      bt=r_azcoef(3)+1
      bmt=1-r_azcoef(3)
      bmmt=1.-2.*r_azcoef(3)
      r_azcoef2(1)=r_azcoef(1)+ r_azcoef(6)*r_azcoef(1)**2*bmmt
      r_azcoef2(2)=r_azcoef(2)+r_azcoef(1)*(2.*r_azcoef(6)*r_azcoef2(2)-r_azcoef(4)*bmt)
      r_azcoef2(3)=r_azcoef(3)-2*r_azcoef(1)*r_azcoef(6)*bmmt
      r_azcoef2(4)=r_azcoef(4)*bmt-2.*r_azcoef(6)*r_azcoef(2)
      r_azcoef2(5)=r_azcoef(5)-r_azcoef2(4)*r_azcoef2(2)
      r_azcoef2(6)=r_azcoef(6)*bmmt
      do k=1,6
        r_azcoef2(k)=r_azcoef2(k)/bt
      enddo
      r_rancoef2(1)=r_rancoef(1)-r_rancoef(3)*r_azcoef2(1)+r_rancoef(6)*r_azcoef2(1)**2
      r_rancoef2(2)=r_rancoef(2)-r_rancoef(3)*r_azcoef2(2)-r_rancoef(4)* r_azcoef2(1)
      r_rancoef2(3)=r_rancoef(3)*(1-r_azcoef2(3))-2*r_rancoef(6)*r_azcoef2(1)
      r_rancoef2(4)=r_rancoef(4)-2.*r_rancoef(3)*r_azcoef2(4)-2.*r_rancoef(6)*r_azcoef2(2)
      r_rancoef2(5)=r_rancoef(5)-r_rancoef(3)*r_azcoef2(5)-r_rancoef(4)*r_rancoef2(2)
      r_rancoef2(6)=r_rancoef(6)-r_rancoef(3)*r_azcoef2(6)-2.*r_rancoef(6)*r_azcoef2(3)
c
c offset en azimuth moyen
         r_ao = r_azcoef(1) + (nl/2)*(r_azcoef(3) + (nl/2)*r_azcoef(6)) +
     +        (npl/2)*(r_azcoef(2) + (npl/2)*r_azcoef(5)+(nl/2)*r_azcoef(4))
      int_az_off = nint(r_ao)
c
      iflatten = 2
      if(index(a_temp,'Yes') .ne. 0)then
         iflatten = 1
      elseif(index(a_temp,'No') .ne. 0)then
         iflatten = 0
      endif

c     open offset file

      write(6,*) ' '
      write(6,'(a,x,i5,x,i5)') 'Resample slave image formed from lines: ',ist,ist+nl

      write(6,*) ' '
      write(6,'(a)') 'Opening file '//a_offfile(1:rdflen(a_offfile))

c ICI radar_ODR a la meme taille que l image maitre
c      nplo = min(npl,npl2)
      nplo = npl
      open(unit=20,file=a_offfile,form='unformatted',access='direct',
     $   status='old',recl=4*(nplo/nrat),iostat=ierr)
      
      if (ierr .ne. 0) then
         write(6,*) 'ERROR...elevation/unw file does not exist !'
         stop
      end if

      if (icoazoff .eq. 1) then
      open(unit=19,file=a_offtecto,form='unformatted',access='direct',
     $   status='old',recl=4*(nplo/nrat),iostat=ierr)
      endif

      if (ierr .ne. 0) then 
         write(6,*) 'ERROR...tecto az offset file does not exist !'
         stop
      end if
      
      write(6,*) ' '
      write(6,*) 'Finished reading elevation/unw file...'

      write(6,*) ' ' 
      write(6,*) 'Range offset fit parameters'
      write(6,*) ' '
      write(6,*) 'Constant term =            ',r_rancoef(1) 
      write(6,*) 'Range Slope term =         ',r_rancoef(2) 
      write(6,*) 'Azimuth Slope =            ',r_rancoef(3) 
      write(6,*) 'Range/Azimuth cross term = ',r_rancoef(4) 
      write(6,*) 'Range quadratic term =     ',r_rancoef(5) 
      write(6,*) 'Azimuth quadratic term =   ',r_rancoef(6) 
       
      write(6,*) ' ' 
      write(6,*) 'Azimuth offset fit parameters'
      write(6,*) ' '
      write(6,*) 'Constant term =            ',r_azcoef(1) 
      write(6,*) 'Range Slope term =         ',r_azcoef(2) 
      write(6,*) 'Azimuth Slope =            ',r_azcoef(3) 
      write(6,*) 'Range/Azimuth cross term = ',r_azcoef(4) 
      write(6,*) 'Range quadratic term =     ',r_azcoef(5) 
      write(6,*) 'Azimuth quadratic term =   ',r_azcoef(6) 

      write(6,*) ' '
      write(6,*) 'Inverted Range offset fit parameters'
      write(6,*) ' '
      write(6,*) 'Constant term =            ',r_rancoef2(1)
      write(6,*) 'Range Slope term =         ',r_rancoef2(2)
      write(6,*) 'Azimuth Slope =            ',r_rancoef2(3)
      write(6,*) 'Range/Azimuth cross term = ',r_rancoef2(4)
      write(6,*) 'Range quadratic term =     ',r_rancoef2(5)
      write(6,*) 'Azimuth quadratic term =   ',r_rancoef2(6)

      write(6,*) ' '
      write(6,*) 'Inverted Azimuth offset fit parameters'
      write(6,*) ' '
      write(6,*) 'Constant term =            ',r_azcoef2(1)
      write(6,*) 'Range Slope term =         ',r_azcoef2(2)
      write(6,*) 'Azimuth Slope =            ',r_azcoef2(3)
      write(6,*) 'Range/Azimuth cross term = ',r_azcoef2(4)
      write(6,*) 'Range quadratic term =     ',r_azcoef2(5)
      write(6,*) 'Azimuth quadratic term =   ',r_azcoef2(6)

c     read in data files

      inquire(file=f(1),exist=ex)
      if (.not.ex) then 
         write(6,*) 'ERROR...file does not exist !'
         stop
      end if

      inquire(file=f(2),exist=ex)
      if (.not.ex) then 
         write(6,*) 'ERROR...file does not exist !'
         stop
      end if		

      write(6,*) 'XXX unit=21, file=(1): ', f(1)
      write(6,*) 'XXX unit=22, file=(2): ', f(2)

      open(unit=21,file=f(1),form='unformatted',status='old',access='direct',recl=8*npl)
      open(unit=22,file=f(2),form='unformatted',status='old',access='direct',recl=8*npl2)
      
      write(6,*) ' '
      write(6,'(a,x,i5)') 'Number samples in interferogram: ',nplo/NR

      write(6,*) 'XXX unit=23, file=(3): ', f(3)
        open(unit=23, file=f(3), form='unformatted', status='unknown', access='direct',recl=(nplo/NR)*8)

      CPP=SLR/WVL

      if(f0 .eq. -99999.)then
         write(6,*) ' '
         write(6,*) 'Estimating Doppler from imagery...' 
         l1 = 1
         l2 = nb
         do j=l1-1,l2-1
            if(mod(j,100) .eq. 0)then
               write(6,*) 'Reading file at line = ',j
            endif
            read(21,rec=j+1) (b(i,j),i=0,npl-1)
         enddo 
         call doppler(npl,l1,l2,b,fd,dddbuff)
         do j=0,npl-1
            write(66,*) j,fd(j)
         enddo
      endif

c     compute resample coefficients 
      
      r_beta = 1.d0
      r_relfiltlen = 8.d0
      i_decfactor = 8192
      r_pedestal = 0.d0
      i_weight = 1
      
      write(6,*) ' '
      write(6,'(a)') 'Computing sinc coefficients...'
      write(6,*) ' '
      
      call sinc_coef(r_beta,r_relfiltlen,i_decfactor,r_pedestal,i_weight,
     +     i_intplength,i_filtercoef,r_filter)
      
      r_delay = i_intplength/2.d0
      f_delay = r_delay
      
      do i = 0 , i_intplength - 1
         do j = 0 , i_decfactor - 1
            fintp(i+j*i_intplength) = r_filter(j+i*i_decfactor)
         enddo
      enddo

      nb = NBMAX
      ibfcnt = (NBMAX-2*N_OVER)/NAZ
      ibfcnt = ibfcnt * NAZ
      nb = ibfcnt + 2*N_OVER

      if(nb .ne. NBMAX) then
         write(6,*) 'Modified buffer max to provide sync-ed overlap'
         write(6,*) 'Max buffer size = ',NBMAX
         write(6,*) 'Set buffer size = ',nb
      end if

c     begin interferogram formation

      write(6,'(a)') 'Beginning interferogram formation...'
      write(6,*) ' '
      
      ibfcnt = nb-2*N_OVER

cc XXX Start of line loop
      do line=0,nl/NAZ-1

         lc = line*NAZ
         ibfcnt = ibfcnt + NAZ
         
         if(ibfcnt .ge. nb-2*N_OVER) then

            ibfcnt = 0
            ibs = ist+int_az_off-N_OVER+lc/(nb-2*N_OVER)*(nb-2*N_OVER)
            ibe = ibs+nb-1

            write(6,'(a,x,i5,x,i5,x,i5,x,i5,x,i5)') 
     +           'int line, slc line, buffer #, line start, line end: ',
     +           line,lc,lc/(nb-2*N_OVER)+1,ibs,ibe
            write(6,'(a,i5,a)') 'Reading ',nb,' lines of data'

            do i=0, nb-1        !load up  buffer

               if(mod(i+1,128) .eq. 0)then
                  write(6,'(a,x,i10)') 'At line: ',i+1
               endif

               irec = i + ibs
               jrec = irec + istoff - 1  ! irec,jrec = image 2 coordinates
               jrecp = jrec  !CHANGEMP  subtract big constant for fit ????

               if(irec .gt. 0)then       !in the data?

                  if(irec .gt. nl+ist+int_az_off)then
                     go to 900
                  endif
                  read(UNIT=22,REC=irec,err=900) (tmp(ii),ii=0,npl2-1) 
		if(npl2.lt.npl)then
 		  do ii=npl2,npl
		   tmp(ii)=cmplx(0.,0.)
                  enddo
                endif

c lecture du fichier elevation : ds geometrie de image1 : a decaler de offset en azimuth=f(az de im2)
c soit jrec l'abscisse en azimuth de l image 2 et j l index en range de l image 1
c a jrecIm2 connu : jrecIm1= jrecIm2 - off_az2(jrecIm2,j)
c on va prendre la topo en jrecIm1,j
c il faut lire une bande de largeur nlard
c ATTENTION : ici radar_ODR.unw commence a la meme ligne que la slc maitre
c 
         r_ao0 = r_azcoef2(1) + jrecp*(r_azcoef2(3) + jrecp*r_azcoef2(6)) 
         r_ao1 = r_azcoef2(1) + jrecp*(r_azcoef2(3) + jrecp*r_azcoef2(6)) +
     +        nplo*(r_azcoef2(2) + nplo*r_azcoef2(5)+jrecp*r_azcoef2(4))
               if(mod(i+1,128) .eq. 0)then
                  write(6,*) 'offset azimuth NR FR',r_ao0,r_ao1
               endif
c
c buffer extension taking into account offsets (assumed to less than 4 pixels) in the a_offtecto file
c
         r_aomin=-max(r_ao0,r_ao1)-4
         r_aomax=-min(r_ao0,r_ao1)+4 
         r_ao0 = r_aomin
         nlard = int(r_aomax-r_aomin)+1
c info DEM non existante en amont jrec+int(r_ao0) < 1
         if(mod(i+1,128) .eq. 0)then
           write(6,*) 'record image 2',jrec
           write(6,*) 'lecture DEM entre',r_ao0,nlard,
     $          jrec+int(r_ao0),jrec+int(r_ao0)+nlard-1
         endif
c 
         if((jrec+int(r_ao0)).gt.0)then
           do ilect_dem=1,nlard
              irecl=((jrec+int(r_ao0)+ilect_dem-1)/nazt+1)*2
              read(20,rec=irecl,err=50)(dem2(ii,ilect_dem),ii=1,npl/nrat)
            if(nrat.gt.1)then
            do ii=1,nplo
              ii1= int((ii-nrat/2.-0.5)/nrat+1.)
              ii1=max(1,ii1)
              ii2= ii1+1
              ii2=min(nplo/nrat,ii2)
              xx=(ii-nrat/2.-0.5)/nrat+1.-ii1
              dem(ii,ilect_dem)=xx*dem2(ii2,ilect_dem)+(1.-xx)*dem2(ii1,ilect_dem)
            enddo
            else
             do ii=1,nplo
              dem(ii,ilect_dem)=dem2(ii,ilect_dem)
             enddo
            endif
           enddo
         else
           do ilect_dem=1,nlard
             do ii=1,nplo
              dem(ii,ilect_dem)=0.
             enddo
           enddo
         endif
         goto 10
50       continue
c info DEM non existante en aval
         do while(ilect_dem.le.nlard)
          do ii=1,nplo
            dem(ii,ilect_dem)=dem(ii,ilect_dem-1)
          enddo
          ilect_dem=ilect_dem+1 
         enddo
10       continue
                  
c
c read buffer from a_offtecto file
c
      if(icoazoff .eq. 1) then
         if((jrec+int(r_ao0)).gt.0)then
           do ilect_dem=1,nlard
              irecl=((jrec+int(r_ao0)+ilect_dem-2)/nazt+1)*2
              read(19,rec=irecl,err=51)(tecto2(ii,ilect_dem),ii=1,nplo/nrat)
c if number of looks in range >1, interpolate a_offtectofile
            do ii=1,nplo
              ii1= int((ii-nrat/2.-0.5)/nrat+1.)
              ii1=max(1,ii1)
              ii2= ii1+1
              ii2=min(nplo/nrat,ii2)
              xx=(ii-nrat/2.-0.5)/nrat+1.-ii1
              tecto(ii,ilect_dem)=xx*tecto2(ii2,ilect_dem)+(1.-xx)*tecto2(ii1,ilect_dem)
            enddo
           enddo
         else
           do ilect_dem=1,nlard
             do ii=1,nplo
              tecto(ii,ilect_dem)=0.
             enddo
           enddo
         endif
         goto 11
51       continue
c info DEM non existante en aval
         do while(ilect_dem.le.nlard)
          do ii=1,nplo
            tecto(ii,ilect_dem)=tecto(ii,ilect_dem-1)
          enddo
          ilect_dem=ilect_dem+1
         enddo
11       continue
      endif
c*    calculate range interpolation factors, which depend on range looping on
c      Image 1 coordinate and azimuth
c*    looping over IMAGE 2 COORDINATE.

                  do j=0,nplo-1 
                     r_ao = r_azcoef2(1) + jrecp*(r_azcoef2(3) + jrecp*r_azcoef2(6)) +
     +                    j*(r_azcoef2(2) + j*r_azcoef2(5)+jrecp*r_azcoef2(4))
                     jj=j+1
                     ilect_dem = 1 + int(-r_ao - r_aomin)
                     ilect_dem=max(ilect_dem,1)
                     ilect_dem=min(ilect_dem,nlard)
                     if(icoazoff .eq. 1) then
c     refine r_ao 1st estimate from  polynomial fit using tecto offsets values
                        r_ao = r_ao + tecto(jj,ilect_dem)
                        ilect_dem = 1 + int(-r_ao - r_aomin)
                        ilect_dem=max(ilect_dem,1)
                        ilect_dem=min(ilect_dem,nlard)
                     endif
                     jj=max(jj,2)
                     jj=min(jj,nplo)
                     r_ro = r_rancoef2(1) + jrecp*(r_rancoef2(3) + jrecp*r_rancoef2(6) ) +   
     +                    j*(r_rancoef2(2) + j*r_rancoef2(5) +jrecp*r_rancoef2(4))
     +                    +r_rancoef2(7)*(0.23*dem(jj-1,ilect_dem)+0.54*dem(jj,ilect_dem)+0.23*dem(jj+1,ilect_dem))
                     rd = r_ro + j 
                     int_rd(j)=int(rd+f_delay)
                     fr_rd(j)=rd+f_delay-int_rd(j)
                  end do
                  do j=0,nplo-1  !range interpolate
                     b(j,i)= sinc_eval(tmp,npl2,fintp,8192,8,int_rd(j),fr_rd(j))
                  end do

               else

                  do j=0,nplo-1  !fill with 0, no data yet
                     b(j,i)=(0.,0.)
                  end do

               end if  !have data in image 2 corresponding to image 1

            end do     !i loop

            goto 901            !jump around this code to fill

 900        write(6,'(a,x,i5)') 'Filling last block, line: ',i

            do ii=i,nb-1
               do j=0,nplo-1
                  b(j,ii)=(0.,0.)
               end do
            end do

 901        continue

         end if

c range phase (in cycles) per pixel 

c         i_a1 = ist + istoff - 1 + line*NAZ + NAZ/2
c         i_r1 = nplo/2.
c         rphs  = 2. * CPP * (r_rancoef(2) + i_a1*(r_rancoef(4) + 
c     +        r_rancoef(7)*i_a1) + i_r1*(2.*r_rancoef(5) +
c     $        3.*r_rancoef(9)*i_r1 + 2.*r_rancoef(8)*i_a1))

c         rphs  = 2. * CPP * (r_rancoef(2) + i_a1*(r_rancoef(4)/2. + 
c     +        (r_rancoef(7)/3.)*i_a1) + nplo*(r_rancoef(5) +
c     $        r_rancoef(9)*nplo) + r_rancoef(8)*(i_a1/2.)*nplo )

         do k=0,NAZ-1
            irec = ist + line*NAZ + k
            jrec = irec + istoff - 1
c
clecture du dem, seulment utile si filtrage en range avant interpolation
c            irecl=((irec-1-ist)/nazt+1)*2
c            read(20,rec=irecl)(dem2(ii,1),ii=1,nplo/nrat)
c            if(nrat.gt.1)then
c            do ii=1,nplo
c             ii1= int((ii-nrat/2.-0.5)/nrat+1.)
c             ii1=max(1,ii1)
c             ii2= ii1+1
c             ii2=min(nplo/nrat,ii2)
c             xx=(ii-nrat/2.-0.5)/nrat+1.-ii1
c             dem(ii,1)=xx*dem2(ii2,1)+(1.-xx)*dem2(ii2,1)
c           enddo
c            else
c              do ii=1,nplo
c              dem(ii,1)=dem2(ii,1)
c              enddo
c            endif

clecture de a_offtecto et interpolation si multilook
          if(icoazoff .eq. 1) then 
            irecl=((irec-1)/nazt+1)*2
            read(19,rec=irecl)(tecto2(ii,1),ii=1,nplo/nrat)
            if(nrat.gt.1)then
            do ii=1,nplo
              ii1= int((ii-nrat/2.-0.5)/nrat+1.)
              ii1=max(1,ii1)
              ii2= ii1+1
              ii2=min(nplo/nrat,ii2)
              xx=(ii-nrat/2.-0.5)/nrat+1.-ii1
              tecto(ii,1)=xx*tecto2(ii2,1)+(1.-xx)*tecto2(ii1,1)
            enddo
            else
              do ii=1,nplo
              tecto(ii,1)=tecto2(ii,1)
              enddo
            endif
          endif

c note: this is only half the phase! Some for each channel

            do j=0,nplo-1
               jj=j+1
c seulment utile si filtrage en range avant interpolation
c              jj=min(jj,nplo-2)
c              jj=max(jj,3)
c              r_ro = r_rancoef(1) + jrec*(r_rancoef(3) + jrec*r_rancoef(6)) +   
c    +              j*(r_rancoefrr + j*r_rancoef(5) +jrec*r_rancoef(4)) 
c    +              + r_rancoef(7)*
c    $             (0.1*dem(jj-2,1)+0.2*dem(jj-1,1)+0.4*dem(jj,1)+0.2*dem(jj+1,1)+0.1*dem(jj+2,1))
c              r_rph=r_ro*CPP+(1./wvl2-1/wvl)*slr2*(r_ro+SLR/SLR2*j)
               r_ao = r_azcoef(1) + jrec*(r_azcoef(3) + jrec*r_azcoef(6)) +
     +              j*(r_azcoef(2) + j*r_azcoef(5) + jrec*r_azcoef(4)) 
          if(icoazoff .eq. 1) then
               r_ao=r_ao+tecto(jj,1)
          endif
c*    !calculate azimuth offsets
               azs = irec + r_ao 
               if(azs .ge. 0.d0) then
                  int_az(j) = int(azs)
               else
                  int_az(j) = int(azs) - 1
               end if
               fr_az(j) = azs - int_az(j)
c A commenter pour oter filtre en range avant interpolation
c               rph(j,k)=cmplx(cos(sngl(2.*pi*r_ro*CPP)),-sin(sngl(2.*pi*r_ro*CPP)))
c               rph(j,k)=cmplx(cos(sngl(2.*pi*r_rph)),-sin(sngl(2.*pi*r_rph)))

            end do

            read(unit=21,rec=irec,err=1000) (tmp(ii),ii=0,npl-1)

            do j=0,npl-1
c A commenter pour oter filtre en range avant interpolation
               a(j) = tmp(j)
c               a(j) = tmp(j)*rph(j,k)
            end do
            
            do j=0,nplo-1        !azimuth interpolation
               ix = int_az(j)-ibs
               r_q = (((f3 * j + f2) * j) + f1) * j + f0
               ph1 = (r_q)*2.0*PI
               phc = fr_az(j) * ph1
               do ii = -3, 4
                  tmp(ii+3) = b(j,ix+ii) * cmplx(cos(ii*ph1),-sin(ii*ph1))
               end do
               cm(j) = sinc_eval(tmp,8,fintp,8192,8,7,fr_az(j))
               cm(j) = cm(j) * cmplx(cos(phc),+sin(phc))
c A commenter pour oter filtre en range avant interpolation
c               cm(j) = cm(j) * conjg(rph(j,k)) * cmplx(cos(phc),sin(phc))
            end do
            dm(nplo-1) = a(nplo-1)
            dm(0) = a(0)
            em(nplo-1) = cm(nplo-1)
            em(0) = cm(0)
c            dm(nplo-2) = a(nplo-2)
c            dm(1) = a(1)
c            em(nplo-2) = cm(nplo-2)
c            em(1) = cm(1)
            do j = 1, nplo-2
c A commenter si on veut resolution maximale 
c moyenne avant de reinserer la rampe
c              dm(j) = .23*a(j-1)+a(j)*.54+a(j+1)*.23
c              em(j) = .23*cm(j-1)+cm(j)*.54+cm(j+1)*.23
c              dm(j) = 0.1*a(j-2)+0.2*a(j-1)+a(j)*.4+a(j+1)*.2+a(j+2)*.1
c              em(j) = 0.1*cm(j-2)+.2*cm(j-1)+cm(j)*.4+cm(j+1)*.2+cm(j+2)*.1
              dm(j) = a(j)
              em(j) = cm(j)
            end do
            
            do j = 0, nplo -1
               c(j,k)  =       dm(j)*   conjg(em(j))    !1-look correlation
               am(j,k) = real(dm(j))**2+aimag(dm(j))**2 !intensity of a
               bm(j,k) = real(em(j))**2+aimag(em(j))**2 !intensity of b
            end do

         end do

c     take looks

         if(iflatten .eq. 1) then
            
            do j=0, nplo/NR-1   !sum over NR*NAZ looks
               cc(j)=(0.,0.)    !intialize sums
               amm(j)=0.
               bmm(j)=0.
               do k=0,NAZ-1
                  do i=0,NR-1
                     cc(j)=cc(j)+c(j*NR+i,k)
                     amm(j)=amm(j)+am(j*NR+i,k)
                     bmm(j)=bmm(j)+bm(j*NR+i,k)
                  end do
               end do
            end do
         else
            do j=0, nplo/NR-1   !sum over NR*NAZ looks
               cc(j)=(0.,0.)    !intialize sums
               amm(j)=0.
               bmm(j)=0.
               do k=0,NAZ-1
                  do i=0,NR-1
                     cc(j)=cc(j)+c(j*NR+i,k)
                     amm(j)=amm(j)+am(j*NR+i,k)
                     bmm(j)=bmm(j)+bm(j*NR+i,k)
                  end do
               end do
c  A commenter pour oter filtre en range avant interpolation
c               cc(j)=cc(j)*conjg(rph(NR*j,NAZ/2)*rph(NR*j,NAZ/2)) !reinsert range phase
               abmm(j)=cmplx(sqrt(amm(j)),sqrt(bmm(j)))
            end do
         end if

         if(line.eq.0.and.ist.gt.1)then
           do ii=0,nplo/NR-1
             em(ii)=cmplx(0.,0.)
           enddo
           do iadd=1,ist-1
             write(UNIT=23,rec=iadd)(em(ii),ii=0,nplo/NR-1)
           enddo
         endif
         write(UNIT=23,rec=line+ist)(em(ii),ii=0,nplo/NR-1) !write out

      end do
cc XXX End of line loop


 1000 close(UNIT=21)
      close(UNIT=22)
      close(UNIT=23)
      close(UNIT=20)

      end
      

      subroutine intp_coefg(psfilename,dec_fac,intp_lgt,f_delay,fintp)
      
      implicit none
      
      integer fl_lgt
      parameter (fl_lgt=8*8192)

      real*8        yintp(0:FL_LGT-1)
      real*4        fintp(0:FL_LGT-1),f_delay
      integer*4     i,j
      integer*4     dec_fac, intp_lgt, k
      real*8        av,dc_max,dc_min
      character*(*) psfilename

      if(dec_fac*intp_lgt .gt. FL_LGT) then
         write(6,*)
     $        'intp_coefg: insufficient space allocated for filter'
         stop
      end if

      open(88,file=psFilename,recl=8*dec_fac*intp_lgt,access='direct')
      read(88,rec=1) (yintp(k),k = 1,dec_fac*intp_lgt)
      close(88)

      do i=0,intp_lgt-1
         do j=0,dec_fac-1
            fintp(i + j*intp_lgt) = yintp(j + i*dec_fac)
         end do
      end do

c f_delay is chosen below "incorrectly" because of the bias introduced
c in the code by choosing the integer index smaller than the actual index
c i.e. ifrac = int(frac*dec_fac)  biases the delay downward
c
c      f_delay =  (float(dec_fac*intp_lgt)/2.-0.5)/float(dec_fac)
c
      f_delay =  (float(dec_fac*intp_lgt)/2.)/float(dec_fac)

c      write(6,*) 'f_delay    = ',f_delay
      
      dc_max = 0.0
      dc_min = 9.9
      do j = 0,dec_fac-1
         av = 0.0
         do i = 0,intp_lgt-1
            av = av + fintp(i + j*intp_lgt)
         enddo
         dc_min = min(dc_min,av)
         dc_max = max(dc_max,av)
      enddo
      av = 0.5 * (dc_max+dc_min)
      do j=0,dec_fac*intp_lgt-1
         fintp(j) = fintp(j)/av
      end do
      
c      write(6,*) 'dc_min   = ',dc_min/av
c      write(6,*) 'dc_max   = ',dc_max/av
      
      return
      end
      
      complex*8 function sinc_eval(arrin,nsamp,intarr,idec,ilen,intp,frp)
      
      integer ilen,idec,intp, nsamp
      real*8 frp
      complex arrin(0:nsamp-1)
      real*4 intarr(0:idec*ilen-1)

      sinc_eval = cmplx(0.,0.)
      if(intp .ge. ilen-1 .and. intp .lt. nsamp ) then
         ifrac= min(max(0,int(frp*idec)),idec-1)
         do k=0,ilen-1
            sinc_eval = sinc_eval + arrin(intp-k)*
     +           intarr(k + ifrac*ilen)
         enddo
      end if

      end


c****************************************************************

      subroutine sinc_coef(r_beta,r_relfiltlen,i_decfactor,r_pedestal,
     +     i_weight,i_intplength,i_filtercoef,r_filter)

c****************************************************************
c**     
c**   FILE NAME: sinc_coef.f
c**     
c**   DATE WRITTEN: 10/15/97
c**     
c**   PROGRAMMER: Scott Hensley
c**     
c**   FUNCTIONAL DESCRIPTION: The number of data values in the array 
c**   will always be the interpolation length * the decimation factor, 
c**   so this is not returned separately by the function.
c**     
c**   ROUTINES CALLED:
c**     
c**   NOTES: 
c**     
c**   UPDATE LOG:
c**
c**   Date Changed        Reason Changed                  CR # and Version #
c**   ------------       ----------------                 -----------------
c**     
c*****************************************************************

      implicit none

c     INPUT VARIABLES:

      real*8 r_beta             !the "beta" for the filter
      real*8 r_relfiltlen       !relative filter length
      integer i_decfactor       !the decimation factor
      real*8 r_pedestal         !pedestal height
      integer i_weight          !0 = no weight , 1=weight
	
c     OUTPUT VARIABLES:
      
      integer i_intplength      !the interpolation length
      integer i_filtercoef      !number of coefficients
      real*8 r_filter(*)        !an array of data values 

c     LOCAL VARIABLES:

      real*8 r_alpha,pi,r_wgt,r_s,r_fct,r_wgthgt,r_soff,r_wa
      integer i_psfl,i,j,ii

c     COMMON BLOCKS:

c     EQUIVALENCE STATEMENTS:

c     DATA STATEMENTS:

C     FUNCTION STATEMENTS:

c     PROCESSING STEPS:

      pi = 4.d0*atan(1.d0)

c     number of coefficients

      i_intplength = nint(r_relfiltlen/r_beta)
      i_filtercoef = i_intplength*i_decfactor
      r_wgthgt = (1.d0 - r_pedestal)/2.d0
      r_soff = (i_filtercoef - 1.d0)/2.d0
      
      do i=0,i_filtercoef-1
         r_wa = i - r_soff
         r_wgt = (1.d0 - r_wgthgt) + r_wgthgt*cos((pi*r_wa)/r_soff)
         j = i - (i_filtercoef - 1.d0)/2.d0
         r_s = dble(j)*r_beta/dble(i_decfactor)
         if(r_s .ne. 0.0)then
            r_fct = sin(pi*r_s)/(pi*r_s)
         else
            r_fct = 1.0
         endif
         if(i_weight .eq. 1)then
            r_filter(i+1) = r_fct*r_wgt
         else
            r_filter(i+1) = r_fct
         endif
      enddo

      end

cc-------------------------------------------

      subroutine funcs(x,y,afunc,ma)

      real*8 afunc(ma),x,y
      real*8 cf(10)
      integer i_fitparam(10),i_coef(10)

      common /fred/ i_fitparam,i_coef

      data cf /10*0./

      do i=1,ma
         cf(i_coef(i))=1.
         afunc(i) = cf(1) + x*(cf(2) + x*(cf(5) + x*cf(9))) +
     +        y*(cf(3) + y*(cf(6) + y*cf(10))) +
     +        x*y*(cf(4) + y*cf(7) + x*cf(8))
c         afunc(i)= cf(1) + cf(2)*x + cf(3)*y + cf(4)*x*y + cf(5)*x*x +
c     +        cf(6)*y*y + cf(7)*y*y*x + cf(8)*x*x*y + cf(9)*x*x*x + cf(10)*y*y*y
         cf(i_coef(i))=0.
      end do

      return
      end
c-------------------------------

CPOD      
CPOD=pod
CPOD
CPOD=head1 USAGE
CPOD
CPOD Usage: resamp_rti cmd_file 
CPOD
CPOD where cmd_file is the name of the command file (= RDF ascii formatted file).
CPOD
CPOD
CPOD=head1 FUNCTION
CPOD
CPOD FUNCTIONAL DESCRIPTION: Interferes two SLC images 
CPOD range, azimuth interpolation with a quadratic or sinc interpolator 
CPOD no circular buffer is used, rather a batch algorithm is implemented
CPOD The calculation of the range and azimuth offsets is done for
CPOD each of the data sets in the offset data file. As soon as the
CPOD current line number exceeds the range line number for one of the
CPOD data sets in the offset data file, the new lsq coefficients are
CPOD to calculate the offsets for any particular range pixel. 
CPOD Looks are taken in azimuth and range as requested.
CPOD
CPOD=head1 ROUTINES CALLED
CPOD
CPOD intp_coefg, sinc_coef
CPOD
CPOD=head1 CALLED BY
CPOD
CPOD
CPOD=head1 FILES USED
CPOD
CPOD Program reads in an RDF command file
CPOD
CPOD Command file Listing:
CPOD =====================
CPOD Image Offset File Name                                (-) =    cull.out
CPOD Display Fit Statistics to Screen                      (-) =    Show Fit Stats     ![Show Fit Stats , No Fit Stats]
CPOD Number of Fit Coefficients                            (-) =        3
CPOD SLC Image File 1                                      (-) =  /u/argus0/er/IFSAR_LASER/SIRC_DATA/MAHANT_1/mah_1.shh 
CPOD Number of Range Samples Image 1                       (-) =   1160
CPOD SLC Image File 2                                      (-) =   /u/argus0/er/IFSAR_LASER/SIRC_DATA/MAHANT_2/mah_2.shh   
CPOD Number of Range Samples Image 2                       (-) =   1160
CPOD Output Interferogram File                             (-) = mah_Lhh.int              
CPOD Multi-look Amplitude File                             (-) = mah_Lhh.amp
CPOD Starting Line, Number of Lines, and First Line Offset (-) =  1    2000    3107
CPOD Doppler Quadractic Fit Coefficients - PRF Units       (-) = -0.06   0     0  
CPOD Radar Wavelength                                      (m) =  0.2400894        
CPOD Slant Range Pixel Spacing                             (m) =  6.66212409  
CPOD Number of Range and Azimuth Looks                     (-) =  1   4
CPOD
CPOD
CPOD Program reads in two flat, binary, image files;
CPOD each pixel in the image files are 8 byte complex numbers:
CPOD "SLC Image File 1" and "SLC Image File 2".
CPOD Record length: "Number of Range Samples Image 1/2"
CPOD 
CPOD Program reads in an ascii-formatted correlation offset file
CPOD "Image Offset File Name" (cull.out in RDF above)
CPOD  with entries: 
CPOD  Range    R offset     Azimuth    Az offset     SNR
CPOD
CPOD=head1 FILES CREATED
CPOD
CPOD "Output Interferogram File"  (cull.out in RDF above) 
CPOD "Multi-look Amplitude File"  (cull.out in RDF above)
CPOD
CPOD=head1 DIAGNOSTIC FILES
CPOD
CPOD Code has option to dump image chip
CPOD purposes in addition to the normal
CPOD
CPOD=head1 HISTORY
CPOD
CPOD Original Routines: Charles Werner, Paul Rosen and Scott Hensley, Soren(?)
CPOD
CPOD=head1 LAST UPDATE
CPOD  Date Changed        Reason Changed 
CPOD  ------------       ----------------
CPOD    20-apr-92    added removal/reinsertion of range phase slope to 
CPOD                 improve correlation
CPOD    11-may-92    added code so that the last input block of data is processed
CPOD                 even if partially full
CPOD    9-jun-92     modified maximum number of range pixels
CPOD    17-nov-92    added calculation of the range phase shift/pixel
CPOD    29-mar-93    write out multi-look images (intensity) of the two files 
CPOD    93-99        Stable with small enhancements changes
CPOD    Dec 99       Modified range interpolation to interpret (correctly)
CPOD                 the array indices to be those of image 2 coordinates.  
CPOD                 Previous code assumed image 1, and therefore used 
CPOD                 slightly wrong offsets for range resampling depending
CPOD                 on the gross offset between images.  Mods involve computing
CPOD                 the inverse mapping
CPOD
CPOF CPOD: trm Nov 24th '03
CPOD=cut

