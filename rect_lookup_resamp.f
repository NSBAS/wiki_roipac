      program rect_lookup
c     
c*****************************************************************************
c**   
c**   FILE NAME: rect_lookup.f
c** 
c**   modified to work with complex input and fixed some minor bugs EJF 96/9/10
c**
c**   modified from bi_polar.f
c**  
c**   DATE WRITTEN: 27-jan-93
c**   
c**   PROGRAMMER: P.A.Rosen
c**   
c**   FUNCTIONAL DESCRIPTION:  This program corrects
c**   monostatic image to align with the bistatic image
c**   
c**   UPDATE LOG:
c**   
c*****************************************************************************
      
      
      implicit none
      
      REAL*4, DIMENSION(:,:), ALLOCATABLE :: in_img
      REAL*4, DIMENSION(:), ALLOCATABLE :: lonlat
      REAL*4, DIMENSION(:), ALLOCATABLE :: arr
      COMPLEX*8, DIMENSION(:), ALLOCATABLE :: carr

      real*4  pt1(3),pt2(3),pt3(3),pt4(3)
      real*8  colval, rowval, ocolval, orowval
      real*8  ifrac, jfrac
      real    interp

      integer oi, oj, i, j, k, ift, iis, ilooksac,ilooksdn, icnt, jcnt, nacout
      integer iargc, ndac, nddn, nrac, nrdn, nraco, nrdno, sraco, srdno, ncarrmax

      character*512 fname, lonlatfile, resfile, outfile, intstyle, filetype

      integer rdflen
      character*255 rdfval,toto
      character*255 rdfcullsp,rdfdata

      save in_img

      if(iargc() .eq. 0) then
         write(*,*) 'usage: rect_lookup param_file'
         stop
      end if

      call getarg(1,fname)
      call rdf_init('ERRFILE=SCREEN')
      write(6,'(a)') 'Reading command file data...'
      call rdf_read(fname)

      toto=rdfval('Longitude-Latitude Lookup File Name','-')
      read(toto,fmt='(a)') lonlatfile
      toto=rdfval('Lookup File Dimensions','-')
      read(toto,fmt=*) ndac, nddn
      toto=rdfval('Input Image File Name','-')
      read(toto,fmt='(a)') resfile
      toto=rdfval('Input File Dimensions','-')
      read(toto,fmt=*) nrac, nrdn
      toto=rdfval('Output Image File Name','-')
      read(toto,fmt='(a)') outfile
      toto=rdfval('Lookup File Start Sample for Output ','-')
      read(toto,fmt=*) sraco
      toto=rdfval('Number of Samples for Output ','-')
      read(toto,fmt=*) nraco
      toto=rdfval('Lookup File Start Line for Output ','-')
      read(toto,fmt=*) srdno
      toto=rdfval('Number of Lines for Output ','-')
      read(toto,fmt=*) nrdno
      toto=rdfval('Skip Every N Lookup points Across-Down ','-')
      read(toto,fmt=*) ilooksac, ilooksdn
      toto=rdfval('File Type','-')
      read(toto,fmt='(a)') filetype
      toto=rdfval('Interpolation Method','-')
      read(toto,fmt='(a)') intstyle
c      read(unit=rdfval('Longitude-Latitude Lookup File Name','-'),fmt='(a)') lonlatfile
c      read(unit=rdfval('Lookup File Dimensions','-'),fmt=*) ndac, nddn
c      read(unit=rdfval('Input Image File Name','-'),fmt='(a)') resfile
c      read(unit=rdfval('Input File Dimensions','-'),fmt=*) nrac, nrdn
c      read(unit=rdfval('Output Image File Name','-'),fmt='(a)') outfile
c      read(unit=rdfval('Lookup File Start Sample for Output ','-'),fmt=*) sraco
c      read(unit=rdfval('Number of Samples for Output ','-'),fmt=*) nraco
c      read(unit=rdfval('Lookup File Start Line for Output ','-'),fmt=*) srdno
c      read(unit=rdfval('Number of Lines for Output ','-'),fmt=*) nrdno
c      read(unit=rdfval('Skip Every N Lookup points Across-Down ','-'),fmt=*) ilooksac, ilooksdn
c      read(unit=rdfval('File Type','-'),fmt='(a)') filetype
c      read(unit=rdfval('Interpolation Method','-'),fmt='(a)') intstyle

      ift = 0
      if(index(filetype,'RMG') .ne. 0)then
         ift = 1
         write (*,*)  'Assuming RMG file type '
      else
         write (*,*)  'Assuming complex file type '
      endif
      iis = 0
      if(index(intstyle,'Bilinear') .ne. 0)then
         iis = 1
         write (*,*)  'Assuming Bilinear Interpolation '
      elseif(index(intstyle,'Sinc') .ne. 0)then
         iis = 2
         write (*,*)  'Assuming Sinc Interpolation '
      else
         write (*,*)  'Assuming Nearest Neighbor '

      end if

      nacout = int((nraco-1.)/ilooksac)+1
c      nacout = nraco/ilooksac
      write(*,*) 'nrac ', nrac
      write(*,*) 'nrdn ', nrdn
      write(*,*) 'ndac ', ndac
      write(*,*) 'nddn ', nddn
      write(*,*) 'intstyle', intstyle
      write(*,*) 'num samples out 1', nraco,ilooksac,sraco,srdno,nrdno,ilooksdn
      write(*,*) 'num samples out', nacout

      ncarrmax=max(nrac,nacout)
      ALLOCATE( in_img (0:2*nrac-1,0:nrdn-1) )
      ALLOCATE( lonlat (2*ndac) )
      ALLOCATE( carr   (0:ncarrmax-1) )

      write(*,*) 'Allocating a resource map of dimension ',nrac,nrdn
      write(*,*) 'Allocating linear arrays of  dimension ',ndac

      ALLOCATE( arr   (2*nacout) )
      write(*,*) 'Allocating linear array of  dimension ',nacout

      open(11,file=lonlatfile,form='unformatted',
     .     access='direct',recl=8*ndac,status='old') 
      open(12,file=resfile,form='unformatted',
     .     access='direct',recl=8*nrac,status='unknown') 
      open(13,file=outfile,form='unformatted',
     .     access='direct',recl=8*nacout,status='unknown') 

c read in the data

      write(*,*) 'reading input image ', resfile

      if(ift .eq.0) then
         do j = 0 , nrdn-1
            if(mod(j,256) .eq. 0) write(*,*) j
            read(12,rec=j+1,err=999) (carr(k),k=0,nrac-1)
            do k = 0 , nrac-1
               in_img(k,j) = real(carr(k))
               in_img(k+nrac,j) = aimag(carr(k))
            end do
         end do
      else
         do j = 0 , nrdn-1
            if(mod(j,256) .eq. 0) write(*,*) j
            read(12,rec=j+1,err=999) (in_img(k,j),k=0,2*nrac-1)
         end do
      end if

 999  write(*,*) 'finished input file read ',j-1, ' lines'
      close(unit=12)

c do the interpolation
      jcnt = 0
      do j = srdno , srdno+nrdno-1, ilooksdn

         jcnt = jcnt + 1
         read(11,rec=j,err=998) (lonlat(k),k=1,2*ndac)

         if(mod(jcnt,100) .eq. 0) write(*,*) ' output line', jcnt,' dem line',j 

         if(iis .eq. 0) then
            icnt = 0
            do i = sraco , sraco+nraco-1, ilooksac
               icnt = icnt + 1
               ocolval = lonlat(i)
               orowval = lonlat(i+ndac)
c               ocolval = lonlat(i)/ilooksac
c               orowval = lonlat(i+ndac)/ilooksdn
               oi = nint(ocolval)
               oj = nint(orowval)
               if(.not.((oi .lt. 0) .or. (oj .lt. 0) .and. (oi .ge. nrac) .and. (oj .ge. nrdn))) then
                  arr(icnt) = in_img(oi,oj)
                  arr(icnt+nacout) = in_img(oi+nrac,oj)
               else             ! outside area to process
                  arr(icnt) = 0
                  arr(icnt+nacout) = 0
               endif
            end do

         elseif(iis .eq. 1) then !          bilinear interpolation
            icnt = 0
            do i = sraco , sraco+nraco-1, ilooksac
               icnt = icnt + 1
               ocolval = lonlat(i)
               orowval = lonlat(i+ndac)
c               ocolval = lonlat(i)/ilooksac
c               orowval = lonlat(i+ndac)/ilooksdn
               oi = nint(ocolval)
               oj = nint(orowval)
               ifrac = (ocolval - oi)
               jfrac = (orowval - oj)
               if(ifrac .lt. 0.d0) then
                  oi = oi - 1
                  ifrac = (ocolval - oi)
               end if
               if(jfrac .lt. 0.d0) then
                  oj = oj - 1
                  jfrac = (orowval - oj)
               end if
               if(.not.((oi .lt. 0) .or. (oi .ge. nrac-1) .or. (oj .lt. 0) .or. (oj .ge. nrdn-1))) then
                  pt1(1) = 0.
                  pt1(2) = 0.
                  pt1(3) = in_img(oi,oj)
                  pt2(1) = 1.
                  pt2(2) = 0.
                  pt2(3) = in_img(oi+1,oj)
                  pt3(1) = 0.
                  pt3(2) = 1.
                  pt3(3) = in_img(oi,oj+1)
                  pt4(1) = 1.
                  pt4(2) = 1.
                  pt4(3) = in_img(oi+1,oj+1)
                  if(.not.((pt1(3) .eq. 0.) .or. (pt2(3) .eq. 0.) .or.
     $                 (pt3(3) .eq. 0.) .or. (pt4(3) .eq. 0.))) then
                     call bilinear(pt1,pt2,pt3,pt4,sngl(ifrac)
     $                    ,sngl(jfrac),arr(icnt))
                     pt1(1) = 0.
                     pt1(2) = 0.
                     pt1(3) = in_img(oi+nrac,oj)
                     pt2(1) = 1.
                     pt2(2) = 0.
                     pt2(3) = in_img(oi+1+nrac,oj)
                     pt3(1) = 0.
                     pt3(2) = 1.
                     pt3(3) = in_img(oi+nrac,oj+1)
                     pt4(1) = 1.
                     pt4(2) = 1.
                     pt4(3) = in_img(oi+1+nrac,oj+1)
                     call bilinear(pt1,pt2,pt3,pt4,sngl(ifrac)
     $                    ,sngl(jfrac),arr(icnt+nacout))
                  else
                     arr(icnt) = 0.
                     arr(icnt+nacout) = 0.
                  end if
               else
                  arr(icnt) = 0.
                  arr(icnt+nacout) = 0.
               end if
            end do
         elseif(iis .eq. 2) then !          sinc interpolation
            icnt = 0
            do i = sraco , sraco+nraco-1, ilooksac
               icnt = icnt + 1
c               ocolval = lonlat(i)/ilooksac
c               orowval = lonlat(i+ndac)/ilooksdn
               ocolval = lonlat(i)
               orowval = lonlat(i+ndac)
               oi = nint(ocolval)
               oj = nint(orowval)
               ifrac = (ocolval - oi)
               jfrac = (orowval - oj)
               if(ifrac .lt. 0.d0) then
                  oi = oi - 1
                  ifrac = (ocolval - oi)
               end if
               if(jfrac .lt. 0.d0) then
                  oj = oj - 1
                  jfrac = (orowval - oj)
               end if
               
               if(.not.(oi .lt. 4 .or. oi .ge. nrac-3 .or. oj .lt. 4 .or
     $              . oj .ge. nrdn-3)) then
                  arr(icnt)      = interp(oi, oj, ifrac, jfrac, in_img, nrac, 0)
                  arr(icnt+nacout) = interp(oi, oj, ifrac, jfrac, in_img, nrac, nrac)
               else
                  arr(icnt) = 0.
                  arr(icnt+nacout) = 0.
               end if
            end do

         end if

         if(ift .eq. 0) then
            do k = 1, nacout 
               carr(k-1) = cmplx(arr(k),arr(k+nacout))
            end do
            write(13,rec=jcnt) (carr(k),k=0,nacout-1)
         else
            write(13,rec=jcnt) (arr(k),k=1,2*nacout)
         end if
      end do
         
 998  write(*,*) 'finished output',jcnt, ' lines, dem line', j
      close(unit=11)
      close(unit=13)
      end
