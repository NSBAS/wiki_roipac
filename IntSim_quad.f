c     23456789012345678901234567890123456789012345678901234567890123456789012
c     
c     FILE NAME: IntSim.f
c     
c     DATE WRITTEN: April 14, 1994
c     
c     PROGRAMMER: Paul Rosen
c     
c     FUNCTIONAL DESCRIPTION: Maps latitude, longitude and height to
c     range, azimuth and height.
c     
c     ROUTINES CALLED: convert_sch_to_xyz
c     (rdir, reast, rnorth), dot, enubasis, getangs, getarg, getTCN_TCvec,
c     latlon, lincomb, lookvec, matvec, norm, orrmread1, radar_to_xyz,
c     rdf_init, rdf_read,
c     tranmat, unitvec, utmtoll, zbrent
c     
c     NOTES: -
c     
c     UTM projection (dem_corner_flag = 1) is assumed to be NAD27 (used for USGS DEM's)
c
c     UPDATE LOG: Additions made by Paul Rosen, Scott Hensley and
c     Sean Buckley.
c     
c     Paul Rosen modified to use RDF input, refine location alg.,f etc. Summer 99
c     changed file name string lengths to allow long file names EJF 99/3/4
c     modified printing of DEM corners (added int() to match format) EJF 99/3/22
c     
c     modified initial orbit search window to work with double scenes 
c     (especially ascending) EJF 99/4/30
c     
c     added check if pixel value is valid 
c     --assume negative and zero values before scale and offset are 'no data' EJF 99/7/19
c    
c     added option (dem_corner_flag = 4) for DEM in UTM with WGS84 datum/ellipsoid EJF 99/7/21-22
c
c     merged PAR and EJF modifications EJF 2000/1/7
c
c     added RDF input file read for DEM Datum EJF 2000/8/2
c     modified calculation of SAR corners in DEM and size of initial search window EJF 2000/8/4
c
c     SOURCE: -
c     
c     23456789012345678901234567890123456789012345678901234567890123456789012
c
c Modifications by M.P. Doin
c Modified version of IntSim.f taking into quadratic offsets between the simulation and the
c master geometry.
c A modified input file is IntSim.in.
c Other modifications include  a DEM oversampling in range and azimuth by a factor 2
c before projection into radar cells. If more than 2 DEM points fall into the same
c radar cell, the minimum value is chosen.

      
      program IntSim
      
      
      implicit none


ccccc declarations

      character*1  a_grid

      character*512 demfile    ! dem file name
      character*512 file       ! parameter file name
      character*512 gpsinfile  ! gps input filename
      character*512 gpsoutfile ! gps output filename
      character*512 mapsimfile ! rough map simulation file
      character*512 moutfile   ! magnitude output filename
      character*512 orbitfile  ! orbit file name
      character*512 simfile    ! gps output filename
      character*512 simoutfile ! gps output filename
      character*512 slpfile    ! slp file name
      character*512 string     ! useful array

      integer*4 mapwidth, rdwidth, rdlength
      parameter (mapwidth = 15000, rdwidth=7200, rdlength=7200)

c      complex*8 slparr(mapwidth)  ! dem slopes  (m/m) in (ac,dn) derivative pairs

c      integer*2 hgtarr(mapwidth)  ! dem heights (m)
      complex*8, DIMENSION(:), ALLOCATABLE :: slparr
      integer*2, DIMENSION(:), ALLOCATABLE :: hgtarr

      integer*4 i             ! loop counter
      integer*4 ia
      integer*4 iargc         ! function
      integer*4 ir
      integer*4 i_diamond(-5:6,-5:6)
      integer*4 i_dogps
      integer*4 i_dolatlon
      integer*4 i_dosim
      integer*4 i_g
      integer*4 i_ga
      integer*4 i_gpscnt
      integer*4 i_gpsflag
      integer*4 i_gr
      integer*4 i_last
      integer*4 i_redcnt
      integer*4 i_simsamp
      integer*4 i_skip
      integer*4 dem_spacing_flag
      integer*4 i_type
      integer*4 i_type_fc
      integer*4 dem_corner_flag
      integer*4 i_zone
      integer*4 j             ! loop counter
      integer*4 jrec             ! loop counter
      integer*4 k             ! loop counter
      integer*4 kk
      integer*4 l
      integer*4 ll
      integer*4 m             ! number of command line arguments
      integer*4 namax         ! number of pixels to map in output
      integer*4 nlatpx        ! number of pixels to map in lat lon
      integer*4 nlonpx        ! number of pixels to map in lat lon
      integer*4 nrmax         ! number of pixels to map in output
      integer*4 nslatpx       ! start of pixels to map in lat lon
      integer*4 nslonpx       ! start of pixels to map in lat lon
      integer*4 ntotlatpx     ! number of pixels in dem
      integer*4 ntotlonpx     ! number of pixels in dem


      real*4 azm(5),val
c      real*4 hgtmap(2*rdwidth,rdlength)

      REAL*4, DIMENSION(:,:), ALLOCATABLE :: hgtmap
      REAL*4, DIMENSION(:,:), ALLOCATABLE :: layover

c      real*4 latlonmap(2*mapwidth)
      REAL*4, DIMENSION(:), ALLOCATABLE :: latlonmap
c
      real*4, dimension(:), allocatable :: rsave, rsavep, azmsave, azmsavep
      real*4, dimension(:), allocatable :: csave, csavep
      integer*2, DIMENSION(:), ALLOCATABLE :: hgtarrp
      real*4 hgttmp

      real*4 r(5)
      real*4 r_ga
      real*4 r_gr
      real*4 a_ga
      real*4 a_gr
      real*4 r_sd(rdwidth)


      real*8 alt_bias         ! dem map bias
      real*8 alt_scale        ! dem map scale factor
      real*8 asamp            ! sample spacings for output map
      real*8 c
      real*8 cosinc
      real*8 deleast
      real*8 dellat           ! latitude spacing of dem 
      real*8 dellon           ! longitude spacing dem 
      real*8 delnorth
      real*8 dtor             ! degrees to radians conversion
      real*8 enumat(3,3)
      real*8 enuvel(3)
      real*8 fc              ! external function
      real*8 fd_coef(4)
      real*8 fd_coef_hertz(4)
      real*8 rd_ref
      real*8 gm               ! gravitational constant
      real*8 grad(3)
      real*8 lat              ! latitude
      real*8 latcorner        ! latitude of dem upper left corner
      real*8 lon              ! longitude
      real*8 loncorner        ! longitude of dem upper left corner
      real*8 omega_earth      ! rotation of earth (radians/s)
      real*8 pi
      real*8 pos(3)
      real*8 prf
      real*8 r0               ! start range for mapping
      real*8 rad              ! radius
      real*8 range            ! range variable
      real*8 rdir
      real*8 reast
      real*8 rnorth
      real*8 rsamp            ! sample spacings for output map
      real*8 rsamp_dopp         ! single look range sample spacings for doppler calculation
      real*8 r_a
      real*8 r_am
      real*8 r_a1866
      real*8 r_a1866m
      real*8 r_az             ! computed azimuth pointing
      real*8 r_bias(3)
      real*8 r_cen_lon
      real*8 r_det
      real*8 r_e2
      real*8 r_e21866
      REAL*8, DIMENSION(:), ALLOCATABLE :: r_gpslat, r_gpslatorg, r_gpslon, r_gpslonorg
      REAL*8, DIMENSION(:), ALLOCATABLE :: r_edis, r_eerr, r_ndis, r_nerr, r_udis, r_uerr, r_losdisp
      real*8 r_enubias(3)
      real*8 r_gpsvec(3)
      real*8 r_junk(9)
      real*8 r_lat_edg(mapwidth)
      real*8 r_lon_edg(mapwidth)
      real*8 r_mat(2,2)
      real*8 r_quad(2,3)
      real*8 r_matrect(2,2)
      real*8 r_nebias(2)
      real*8 r_sc_az          ! azimuth pointing of s/c (right -pi/2, left pi/2)
      real*8 r_sc_az_nom      ! nominal azimuth angle angle
      real*8 r_sc_look        ! look angle of s/c boresight
      real*8 r_time
      real*8 r_tv(2)
      real*8 r_v(2)
      real*8 r_vs(2)
      real*8 sc_losv(3)       ! s/c line of sight vector to target center
      real*8 sc_losvenu(3)
      real*8 sc_r
      real*8 sc_vel
      real*8 sc_hdg
      real*8 sc_pitch
      real*8 t0               ! start time for mapping
      real*8 tol              ! error tolerance for numerical solution
      real*8 tpv(3)           ! image center position vector (km) XYZ
      real*8 trk_rad
      real*8 ts              ! time since periapsis
      real*8 tslast
      real*8 tslon            ! time save value
      real*8 twin             ! time window
      real*8 twinred
      real*8 ugrad(3)
      real*8 usc_losvenu(3)
      real*8 vel(3), tc_vec(3), tc_vel
      real*8 xyz2enu(3,3)
      real*8 xyz2enu_sat(3,3)
      real*8 zbrent           ! external function
      real*8 wvl


ccccc paradd

      integer*4 mod_counter, i_dumdum, first_pixel(2), last_pixel(2)
      real*8 sim_cen_lat, sim_cen_lon, sim_cen_time, sim_cen_range
      real*8 r_m , tanm , sinm , cosg , sing , sim_corner_sch(3), sim_corner_sch2(3), r_s, r_gpss, r_gpsc, tpv_dum(3), sch_dum(3)

      real*8 dem_fraction
      real*8 dem_pixel(2)
      real*8 dem_pixel_min(2)
      real*8 dem_pixel_max(2)
      real*8 easting_pad
      real*8 img_pln_pad
      real*8 lat_pad
      real*8 lon_pad
      real*8 s_pad
      real*8 c_pad
      real*8 max_height
      real*8 max_lat
      real*8 northing_pad
      real*8 img_pln_rad
      real*8 look_angle
      real*8 look_angle_0
      real*8 sc_d
      real*8 sc_sch(3)
      real*8 sch_mat(3,3)
      real*8 sch_mat2(3,3)
      real*8 sch_ov(3)
      real*8 sch_ov2(3)
      real*8 sim_corner_h
      real*8 sim_corner_lat
      real*8 sim_corner_lon
      real*8 sim_corner_range(5)
      real*8 sim_corner_time(5)
      real*8 sim_corner_xyz(3)
      real*8 sim_corner_xyzm(3)
      real*8 sim_total_time     ! total length of simulation 
      real*8 sc_h
      real*8 sc_lat
      real*8 sc_lon
      real*8 sc_hdg_0
      real*8 target_d
      real*8 time
      real*8 u_lk(3)
      real*8 u_lk_xyz(3)
      real*8 lk_xyz(3)
      real*8 lk_enu(3)
      real*8 u_lk_enu(3)
      real*8 r_h

ccccc EJF added
      logical bad_elev

      integer*4 ntotcpx
      integer*4 ntotspx

      real*8 ccorner
      real*8 delc
      real*8 dels
      real*8 sch_dem_peg_hdg
      real*8 sch_dem_peg_lat
      real*8 sch_dem_peg_lon
      real*8 sch_dem_peg_rad
      real*8 scorner
      real*8 dem_corner_sch(3)
      real*8 dem_corner_xyz(3)
      real*8 dem_sch(3)
      real*8 dem_xyz(3)
      real*8 r_llh(3)
      real*8 r_fe,r_fn(2)
      real*8 h, rr, v, re, dop, pix, lr, th

      type ellipsoid
      	   real (8) r_a        
      	   real (8) r_e2
      end type ellipsoid
      type (ellipsoid) elp

      type pegtype
      	   real (8) r_lat
      	   real (8) r_lon
      	   real (8) r_hdg
      end type pegtype
      type (pegtype) peg

      type pegtrans
        real (8) r_mat(3,3)
        real (8) r_matinv(3,3)
        real (8) r_ov(3)
        real (8) r_radcur
        end type pegtrans
      type (pegtrans) ptm
      type (pegtrans) ptm2

      integer i_xyztollh,i_llhtoxyz
      parameter(i_xyztollh=2, i_llhtoxyz=1) 
      integer i_schtoxyz,i_xyztosch
      parameter(i_schtoxyz=0,i_xyztosch=1) 
      integer i_utmtollh,i_llhtoutm
      parameter (i_utmtollh=2,i_llhtoutm=1)

      character*255 rdfval,rdftmp
      integer*4 rdflen
      character*255 rdfint
      character*255 rdfreal,rdfdble
      real summean,summeana
      integer isummean


ccccc common statements
 
      common /sc_point/ r_sc_look, r_sc_az
      common /target/ tpv
      common /ellipsoid/ r_a, r_e2
c      common /scposvel/ poscmn, velcmn
      common /dopplercommon/ rd_ref, rsamp_dopp, wvl, fd_coef_hertz, i_type_fc



ccccc data statements
      
      data c /2.99792458d5/             ! speed of light (km/s)
      data dtor /.0174532925199d0/
      data gm /3.98600448073d5/         ! Earth gravitational constant (km^3/s^2)
      data omega_earth /72.92115d-6/    ! sidereal rotation rate of Earth (rad/s)
      data pi /3.1415926535897943d0/
      data r_am /6378137.d0/
      data r_a1866 /6378.2064d0/
      data r_a1866m /6378206.4d0/
      data r_bias /.061d3,-.285d3,-.181d3/
      data r_e21866 /.00676865799761d0/
      data tol /1.d-12/
      data a_grid /'N'/
      data i_zone /1/
      data r_fe,r_fn /500000.d0,0.d0,10000000.d0/

ccccc external statements
      
      external fc       ! compute distance to a scatterer from a fixed location
      external zbrent    ! zero of a function

ccccc function statements

      real*8 dot


ccccc save statements

      save hgtmap
      save latlonmap

ccccc usage note

      write (*,*)
      write (*,*)
      write (*,*) '*** IntSim by par, sh, smb, ejf ***'
      write (*,*)
      write (*,*)
      
      r_a  = 6378.137d0      ! WGS84 ellipsoid, I think
      r_e2 = .00669437999015d0
      m = iargc()

      if (m .lt. 1) then
         write (*,*) 'Maps latitude, longitude and height to range, azimuth and height.'
         write (*,*)
         write (*,*)
         write (*,*) 'Usage: IntSim <rdf_input_file> '
         write (*,*)
         write (*,'(8x,a)') 'rdf_input_file   name of rdf format input file'
         write (*,*)
         write (*,*)
         stop
      endif


ccccc initializations

      dtor       = atan(1.d0)/45.d0
      i_dogps    = 0
      i_dolatlon = 0
      i_dosim    = 0
      i_skip     = 1


ccccc initializations - form 'GPS diamond'

      do i = -5 , 6
         do j = -5 , 6
            i_diamond(i,j) = 0
         enddo
      enddo

      do i = -4 , 5
         if (i .le. 0) then
            k = i
         else
            k = -i + 1
         endif
         do j = -(k+4) , k+5
            i_diamond(j,i) = 1
         enddo
      enddo


ccccc read input file

      call getarg(1,file)
      
      call rdf_init('ERRFILE=SCREEN')
      write(6,'(a)') 'Reading command file data...'
      call rdf_read(file)

      rdftmp=rdfval('Digital Elevation Model Filename','-')
      read(unit=rdftmp,fmt='(a)') demfile
      rdftmp=rdfval('Slope Filename','-')
      read(unit=rdftmp,fmt='(a)') slpfile
      rdftmp=rdfval('ORRM formatted filename','-')
      read(unit=rdftmp,fmt='(a)') orbitfile
      rdftmp=rdfval('Height in simulated range,doppler coordinates','-')
      read(unit=rdftmp,fmt='(a)') mapsimfile
      rdftmp=rdfval('Coordinates of simulated range,doppler in map format','-')
      read(unit=rdftmp,fmt='(a)') moutfile
      rdftmp=rdfval('GPS vector inputs filename','-')
      read(unit=rdftmp,fmt='(a)') gpsinfile
      rdftmp=rdfval('GPS vector mapped to radar LOS output filename','-')
      read(unit=rdftmp,fmt='(a)') gpsoutfile
      rdftmp=rdfval('Rectified height in simulated coordinates','-')
      read(unit=rdftmp,fmt='(a)') simfile
      rdftmp=rdfval('Rectified height in simulated coordinates with GPS','-')
      read(unit=rdftmp,fmt='(a)') simoutfile
      rdftmp=rdfval('Dimensions of rectified height file','-')
      read(unit=rdftmp,fmt=*) i_simsamp, i_last
      rdftmp=rdfval('DEM projection','-')
      read(unit=rdftmp,fmt='(a)') string
      if(index(string,'UTM') .ne. 0)then
         dem_spacing_flag = 1
         rdftmp=rdfval('DEM corner easting','m')
         read(unit=rdftmp,fmt=*) loncorner
         rdftmp=rdfval('DEM easting spacing','m')
         read(unit=rdftmp,fmt=*) dellon
         rdftmp=rdfval('DEM total easting pixels','-')
         read(unit=rdftmp,fmt=*) ntotlonpx
         rdftmp=rdfval('DEM UTM zone','-')
         read(unit=rdftmp,fmt=*) i_zone
         rdftmp=rdfval('DEM corner northing','m')
         read(unit=rdftmp,fmt=*) latcorner
         rdftmp=rdfval('DEM northing spacing','m')
         read(unit=rdftmp,fmt=*) dellat
         rdftmp=rdfval('DEM total northing pixels','-')
         read(unit=rdftmp,fmt=*) ntotlatpx
      elseif(index(string,'LL') .ne. 0)then
         dem_spacing_flag = 2
         dem_corner_flag = 2
         rdftmp=rdfval('DEM corner latitude','deg')
         read(unit=rdftmp,fmt=*) latcorner
         rdftmp=rdfval('DEM latitude spacing','deg')
         read(unit=rdftmp,fmt=*) dellat
         rdftmp=rdfval('DEM total latitude pixels','-')
         read(unit=rdftmp,fmt=*) ntotlatpx
         rdftmp=rdfval('DEM corner longitude','deg')
         read(unit=rdftmp,fmt=*) loncorner
         rdftmp=rdfval('DEM longitude spacing','deg')
         read(unit=rdftmp,fmt=*) dellon
         rdftmp=rdfval('DEM total longitude pixels','-')
         read(unit=rdftmp,fmt=*) ntotlonpx
      elseif(index(string,'SCH') .ne. 0)then
         dem_spacing_flag = 3
         dem_corner_flag = 3
         rdftmp=rdfval('DEM corner s','m')
         read(unit=rdftmp,fmt=*) scorner
         rdftmp=rdfval('DEM s spacing','m')
         read(unit=rdftmp,fmt=*) dels
         rdftmp=rdfval('DEM total s pixels','-')
         read(unit=rdftmp,fmt=*) ntotspx
         rdftmp=rdfval('DEM corner c','m')
         read(unit=rdftmp,fmt=*) ccorner
         rdftmp=rdfval('DEM c spacing','m')
         read(unit=rdftmp,fmt=*) delc
         rdftmp=rdfval('DEM total c pixels','-')
         read(unit=rdftmp,fmt=*) ntotcpx
         rdftmp=rdfval('SCH Peg lat','deg')
         read(unit=rdftmp,fmt=*) sch_dem_peg_lat
         rdftmp=rdfval('SCH Peg lon','deg')
         read(unit=rdftmp,fmt=*) sch_dem_peg_lon
         rdftmp=rdfval('SCH Heading','deg')
         read(unit=rdftmp,fmt=*) sch_dem_peg_hdg
      else                      ! added check for bad projection EJF 00/8/2
         write(*,*)'DEM projection ',string,' not recognized'
         stop
      end if
!     added read of datum info EJF 00/8/2

      if (dem_spacing_flag .eq. 1) then ! UTM
         rdftmp=rdfval('DEM datum','-')
         read(unit=rdftmp,fmt='(a)') string
         if(index(string,'NAD27') .ne. 0)then
            dem_corner_flag = 1
         elseif (index(string,'WGS84') .ne. 0)then
            dem_corner_flag = 4
         else
            write(*,*)'DEM datum ',string,' not recognized'
            stop
         endif
         if(latcorner .ge. 0 .and. latcorner .lt. r_fn(2)) then
            a_grid = 'N'
         elseif( ( (latcorner .ge. r_fn(2))  
     $     .and.   (latcorner .lt. 2.*r_fn(2)) ) 
     $      .or.   (latcorner .lt. 0.0)         ) then
            a_grid = 'M'
         else 
            a_grid = 'N'
            write(*,*)
     $              'WARNING: Nonsensical UTM northing specified in input!'
         end if
      end if

      rdftmp=rdfval('DEM height bias','m')
      read(unit=rdftmp,fmt=*) alt_bias
      rdftmp=rdfval('DEM height scale factor','m')
      read(unit=rdftmp,fmt=*) alt_scale
      rdftmp=rdfval('DEM Northing bias','m')
      read(unit=rdftmp,fmt=*) r_nebias(2)
      rdftmp=rdfval('DEM Easting bias','m')
      read(unit=rdftmp,fmt=*) r_nebias(1)
      rdftmp=rdfval('Range output reference','km')
      read(unit=rdftmp,fmt=*) r0
      rdftmp=rdfval('Time  output reference','sec')
      read(unit=rdftmp,fmt=*) t0
      rdftmp=rdfval('Range output spacing','m')
      read(unit=rdftmp,fmt=*) rsamp
      rdftmp=rdfval('Azimuth output spacing','m')
      read(unit=rdftmp,fmt=*) asamp
      rdftmp=rdfval('Number of range pixels','-')
      read(unit=rdftmp,fmt=*) nrmax
      rdftmp=rdfval('Number of azimuth pixels','-')
      read(unit=rdftmp,fmt=*) namax
      rdftmp=rdfval('Nominal squint angle from heading','deg')
      read(unit=rdftmp,fmt=*) r_sc_az_nom 
      rdftmp=rdfval('GPS scale factor','-')
      read(unit=rdftmp,fmt=*) r_time
      rdftmp=rdfval('Datum conversion bias vector','m,m,m')
      read(unit=rdftmp,fmt=*) r_bias
      rdftmp=rdfval('Inverted Affine matrix row 1','-,-')
      read(unit=rdftmp,fmt=*) r_mat(1,1) , r_mat(1,2) 
      rdftmp=rdfval('Inverted Affine matrix row 2','-,-')
      read(unit=rdftmp,fmt=*) r_mat(2,1) , r_mat(2,2) 
      rdftmp=rdfval('Inverted Affine offset vector','-,-')
      read(unit=rdftmp,fmt=*) r_tv(1),r_tv(2)
      rdftmp=rdfval('Inverted Quadratic terms row 1','-,-')
      read(unit=rdftmp,fmt=*) r_quad(1,1) , r_quad(1,2), r_quad(1,3)
      rdftmp=rdfval('Inverted Quadratic terms row 2','-,-')
      read(unit=rdftmp,fmt=*) r_quad(2,1) , r_quad(2,2), r_quad(2,3)
      i_dosim = 0
      i_dolatlon = 0
      i_dogps = 0
      rdftmp=rdfval('Do simulation?','-')
      read(unit=rdftmp,fmt='(a)') string
      if(index(string,'yes') .ne. 0 .or. index(string,'YES') .ne. 0)then
         i_dosim = 1
      end if
      rdftmp=rdfval('Do mapping?','-')
      read(unit=rdftmp,fmt='(a)') string
      if(index(string,'yes') .ne. 0 .or. index(string,'YES') .ne. 0)then
         i_dolatlon = 1
      end if
      rdftmp=rdfval('Do GPS mapping?','-')
      read(unit=rdftmp,fmt='(a)') string
      if(index(string,'yes') .ne. 0 .or. index(string,'YES') .ne. 0)then
         i_dogps = 1
      end if
      rdftmp=rdfval('Output skip factor','-')
      read(unit=rdftmp,fmt=*) i_skip
      
      i_type_fc = 0
      rdftmp=rdfval('Search method','-')
      read(unit=rdftmp,fmt='(a)') string
      if(index(string,'Angle') .ne. 0 .or. index(string,'ANGLE') .ne. 0
     $     .or. index(string,'angle') .ne. 0 )then
         i_type_fc = 0
      end if
      if(index(string,'Doppler') .ne. 0 .or. index(string,'DOPPLER') .ne
     $     . 0 .or. index(string,'doppler') .ne. 0 )then
         i_type_fc = 1
         rdftmp=rdfval('Range reference for Doppler','km')
         read(unit=rdftmp,fmt=*) rd_ref
         rdftmp=rdfval('Range spacing for Doppler','m')
         read(unit=rdftmp,fmt=*) rsamp_dopp
         rdftmp=rdfval('Doppler coefficients','-,-,-,-')
         read(unit=rdftmp,fmt=*) fd_coef
         rdftmp=rdfval('Radar Wavelength','m')
         read(unit=rdftmp,fmt=*) wvl
         rdftmp=rdfval('Radar PRF','-')
         read(unit=rdftmp,fmt=*) prf
      end if
      rdftmp=rdfval('Desired center latitude','deg')
      read(unit=rdftmp,fmt=*) sim_cen_lat
      rdftmp=rdfval('Desired center longitude','deg')
      read(unit=rdftmp,fmt=*) sim_cen_lon

ccccc perform conversions

      namax = namax / i_skip
      nrmax = nrmax / i_skip
      asamp = asamp * i_skip
      rsamp = rsamp * i_skip
      
      ALLOCATE( hgtmap(2*nrmax,namax) )
      ALLOCATE( layover(nrmax,namax) )
      write(*,*) 'Allocated a range/doppler map of dimension ',nrmax,namax
      ALLOCATE( hgtarr(ntotlonpx) )
      ALLOCATE( hgtarrp(ntotlonpx) )
      ALLOCATE( slparr(2*ntotlonpx) )
      ALLOCATE( latlonmap(2*ntotlonpx) )
      write(*,*) 'Allocated linear arrays of dimension ',ntotlonpx

      sch_dem_peg_lat = sch_dem_peg_lat * dtor    ! deg to rad
      sch_dem_peg_lon = sch_dem_peg_lon * dtor    ! deg to rad
      sch_dem_peg_hdg = sch_dem_peg_hdg * dtor    ! deg to rad
      sim_cen_lat     = sim_cen_lat     * dtor    ! deg to rad
      sim_cen_lon     = sim_cen_lon     * dtor    ! deg to rad
      r_sc_az_nom         = r_sc_az_nom         * dtor    ! deg to rad
c      r_sc_look       = r_sc_look       * dtor    ! deg to rad

      do k = 1 , 4
         fd_coef_hertz(k) = fd_coef(k) * prf      ! Hz/prf to Hz
      enddo
      do k = 1 , 3
         r_bias(k) = r_bias(k) / 1.d3             ! m to km
      enddo

      wvl = wvl / 1000.d0                         ! m to km

      lr = 1.
      if(r_sc_az_nom .lt. 0.) lr = -1.

ccccc open files
      print*,simfile     
      open (31,file=demfile,form='unformatted',access='direct',recl=ntotlonpx*2,action='read')
      open (32,file=slpfile,form='unformatted',access='direct',recl=ntotlonpx*8)
      open (16,file=orbitfile,form='formatted',status='old')
      open (19,file=mapsimfile,form='unformatted',access='direct',recl=nrmax*8)
      open (20,file=moutfile,form='unformatted',access='direct',recl=ntotlonpx*8)
      open (21,file=simfile,form='unformatted',access='direct',recl=i_simsamp*8)
      open (22,file=simoutfile,form='unformatted',access='direct',recl=i_simsamp*8)
      open (23,file='layover.hgt',form='unformatted',access='direct',recl=nrmax*4)
      open (25,file=gpsinfile,status='unknown')
      open (26,file=gpsoutfile,status='unknown')

      write (*,*)
      write (*,*)
      write (*,*) 'Initializing orbit file...'
      write (*,*)

      call orrmread1(16,t0,pos,vel,1) ! get body fixed state vector


ccc initialize arrays
      if (i_dosim .eq. 1) then
         do j = 1 , namax
            do i = 1 , 2*nrmax
               hgtmap(i,j) = 0.0
            enddo
            do i = 1 , nrmax
               layover(i,j) = 0.0
            enddo
         enddo
      endif


ccccc invert matrix to convert to final resamp position
cc input matrix already inverted

c      r_det = r_mat(1,1) * r_mat(2,2) - r_mat(1,2) * r_mat(2,1)
c
c      r_matrect(1,1) =  r_mat(2,2) / r_det
c      r_matrect(1,2) = -r_mat(1,2) / r_det
c      r_matrect(2,1) = -r_mat(2,1) / r_det
c      r_matrect(2,2) =  r_mat(1,1) / r_det

ccccc convert DEM corner position into latitude and longitude in radians

      if (dem_corner_flag .eq. 1) then                ! convert NAD27 UTM to lat, lon
         r_v(2)  = latcorner
         r_v(1)  = loncorner
         r_vs(2) = r_v(2)       ! save corner coords in UTM meters
         r_vs(1) = r_v(1)
                                ! convert latcorner and loncorner to radians
         elp%r_a = r_a1866m
         elp%r_e2 = r_e21866
         call utmtoll(elp,i_zone,a_grid,r_v,latcorner,loncorner,i_utmtollh)
      elseif (dem_corner_flag .eq. 2) then            ! convert lat, lon from degrees to radians
         latcorner = latcorner * dtor
         loncorner = loncorner * dtor
      elseif (dem_corner_flag .eq. 3) then            ! convert SCH to lat, lon
         r_v(2)  = scorner
         r_v(1)  = ccorner
         r_vs(2) = r_v(2)
         r_vs(1) = r_v(1)
         dem_corner_sch(1) = r_v(2)
         dem_corner_sch(2) = r_v(1)
         dem_corner_sch(3) = 0.d0                     ! or should I use the height value from the DEM?
         sch_dem_peg_rad = rdir(r_am,r_e2,sch_dem_peg_hdg,sch_dem_peg_lat)  ! in meters
         elp%r_a = r_am
         elp%r_e2 = r_e2
         peg%r_lat = sch_dem_peg_lat
         peg%r_lon = sch_dem_peg_lon
         peg%r_hdg = sch_dem_peg_hdg
         call radar_to_xyz(elp,peg,ptm)
         call convert_sch_to_xyz(ptm,dem_corner_sch,dem_corner_xyz,i_schtoxyz)
         call latlon(elp,dem_corner_xyz,r_llh,i_xyztollh)
         latcorner = r_llh(1)
         loncorner = r_llh(2)
      elseif (dem_corner_flag .eq. 4) then                ! convert WGS84 UTM to lat, lon
         r_v(2)  = latcorner
         r_v(1)  = loncorner          
         r_vs(2) = r_v(2)       ! save corner coords in UTM meters
         r_vs(1) = r_v(1)        
         elp%r_a = r_am
         elp%r_e2 = r_e2
         call utmtoll(elp,i_zone,a_grid,r_v,latcorner,loncorner,i_utmtollh)
      else
         write (*,*)' dem_corner_flag option not recognized'
         stop
      endif


ccccc write out a few things

      write (*,*)
      write (*,1011) 'Skip factor' , i_skip
      write (*,1003) 'Bias vector (km)' , r_bias
      write (*,1001) 'Azimuth angle (deg)', r_sc_az_nom/dtor
c     write (*,1001) 'Look angle (deg)', r_sc_look/dtor
c     write (*,1001) 'Antenna rotation about radial axis (deg)', sc_rrot
      write (*,1001) 'DEM corner latitude  (deg)' , latcorner/dtor
      write (*,1001) 'DEM corner longitude (deg)' , loncorner/dtor
      write (*,1001) 'Given slant range to first pixel (km)' , r0
      write (*,1001) 'Given time of first pixel (s)' , t0


ccccc write out information about input DEM

      write (*,*)
      write (*,*)
      write (*,*) 'Input DEM information:'
      write (*,*)

      if (dem_corner_flag .eq. 1) then
         write (*,1004) 'Corner in UTM northing, easting (m) & zone' , r_vs(2) , r_vs(1) , i_zone
         write (*,*) '***using NAD27 datum and Clarke 1866 ellipsoid'
      elseif (dem_corner_flag .eq. 2) then
         write (*,1002) 'Corner in latitude and longitude (deg)' , latcorner/dtor , loncorner/dtor
      elseif (dem_corner_flag .eq. 3) then
         write (*,1002) 'Corner in (s,c) (m)' , r_vs(2), r_vs(1)
         write (*,1003) 'SCH peg latitude, longitude, heading (deg)', sch_dem_peg_lat/dtor, sch_dem_peg_lon/dtor,
     &                                                                sch_dem_peg_hdg/dtor
      elseif (dem_corner_flag .eq. 4) then
         write (*,1004) 'Corner in UTM northing, easting (m) & zone' , r_vs(2) , r_vs(1) , i_zone
         write (*,*) '***using WGS84 datum and ellipsoid'
      endif

      if (dem_spacing_flag .eq. 1) then
         write (*,1002) 'Spacing in UTM northing and easting (m)' , dellat , dellon
      elseif (dem_spacing_flag .eq. 2) then
         write (*,1002) 'Spacing in latitude and longitude (deg)' , dellat , dellon
      elseif (dem_spacing_flag .eq. 3) then
         write (*,1002) 'Spacing in (s,c) (m)' , dels , delc
      endif

      if ((dem_corner_flag .eq. 1) .or. (dem_corner_flag .eq. 3)
     &     .or. (dem_corner_flag .eq. 4) ) then
         write (*,1002) 'Corner latitude and longitude (deg)' , latcorner/dtor , loncorner/dtor
      endif


ccccc read in orbit file

      call orrmread1(16,t0,pos,vel,0) ! get body fixed state vector



      if(i_type_fc .eq. 1) then
         call orrmread1(16,t0,pos,vel,0)
         elp%r_a = r_a
         elp%r_e2 = r_e2
         call latlon(elp,pos,r_llh,i_xyztollh)
         lat = r_llh(1)
         lon = r_llh(2)
         rad = r_llh(3)
         call enubasis(lat,lon,enumat)
         call tranmat(enumat,xyz2enu)
         call matvec(enumat,r_bias,r_enubias)
         call matvec(xyz2enu,vel,enuvel)
         sc_hdg = atan2(enuvel(1),enuvel(2))
         trk_rad = rdir(r_a,r_e2,sc_hdg,lat)
         call norm(vel,v)
         call norm(pos,sc_r)
         elp%r_a = r_a
         elp%r_e2 = r_e2
         call latlon(elp,pos,r_llh,i_xyztollh)
         h  = r_llh(3)
         re = trk_rad
         rr = r0+(1.d-3*rsamp*nrmax/2.)
         pix = (rr-rd_ref)/(1.d-3*rsamp_dopp)
         dop = fd_coef_hertz(1) + fd_coef_hertz(2) * pix +
     $        fd_coef_hertz(3) * pix**2 + fd_coef_hertz(4) * pix**3
         th=acos(((h+re)**2+rr*rr-re**2)/(2.d0*rr*(re+h)))
         r_sc_az_nom = lr * (pi/2. - asin(dop * wvl/(2.d0*v*sin(th))))
      end if

      write (*,*)  'Nominal squint angle ',r_sc_az_nom/dtor

ccccc if center point given, then determine new slant range and time

      if ((sim_cen_lat .ne. 0.d0) .and. (sim_cen_lon .ne. 0.d0)) then
         write (*,*)
         write (*,*)
         write (*,*) 'Simulation center point information:'
         write (*,*)
         write (*,1001) 'Sim cen latitude  (deg)' , sim_cen_lat/dtor
         write (*,1001) 'Sim cen longitude (deg)' , sim_cen_lon/dtor
         r_h = 0.d0
         elp%r_a = r_a
         elp%r_e2 = r_e2
         r_llh(1) = sim_cen_lat
         r_llh(2) = sim_cen_lon
         r_llh(3) = r_h
         call latlon(elp,tpv,sim_cen_lat,sim_cen_lon,r_h,i_llhtoxyz)
         r_sc_az = r_sc_az_nom
         sim_cen_time = zbrent(fc,t0-50.0,t0+50.0,tol) ! solve for position of s/c
         if (sim_cen_time .eq. 1.e9) then
            write (*,*) '***** IntSim - zbrent - no zero in bracket'
            stop
         endif
         call orrmread1(16,sim_cen_time,pos,vel,0)  ! get body fixed state vector
         call lincomb(-1.d0,pos,1.d0,tpv,sc_losv)
         call norm(sc_losv,sim_cen_range)
         call enubasis(sim_cen_lat,sim_cen_lon,enumat)
         call tranmat(enumat,xyz2enu)
         call matvec(enumat,r_bias,r_enubias)
         call matvec(xyz2enu,vel,enuvel)
         sc_hdg = atan2(enuvel(1),enuvel(2))
         trk_rad = rdir(r_a,r_e2,sc_hdg,sim_cen_lat)
         call norm(pos,sc_r)
         call norm(vel,sc_vel)
         call getTCN_TCvec(pos,vel,vel,tc_vec)
         call norm(tc_vec,tc_vel)
         r0 = sim_cen_range - (1.d-3 * rsamp) * (nrmax/2.d0)
         t0 = sim_cen_time - (1.d-3 * asamp) * (namax/2.d0) * sc_r / (tc_vel * trk_rad)

         call latlon(elp,pos,r_llh,i_xyztollh)
         sc_lat = r_llh(1)
         sc_lon = r_llh(2)
         sc_h = r_llh(3)
         if (r0 .lt. sc_h) then
            write (*,*)
            write (*,1001) '*** New slant range to first pixel (km)' , r0
            write (*,1001) '*** Spacecraft height above ellipsoid (km)' , sc_h
            write (*,*)
            write (*,*) '*** New slant range to first pixel < spacecraft height above ellipsoid'
            write (*,*) '*** Adjust simulation center information and try again'
            write (*,*)
            stop
         endif

c update nominal squint angle if necessary

         if(i_type_fc .eq. 1) then
            call orrmread1(16,t0,pos,vel,0)
            call latlon(elp,pos,r_llh,i_xyztollh)
            lat = r_llh(1)
            lon = r_llh(2)
            rad = r_llh(3)
            call enubasis(lat,lon,enumat)
            call tranmat(enumat,xyz2enu)
            call matvec(enumat,r_bias,r_enubias)
            call matvec(xyz2enu,vel,enuvel)
            sc_hdg = atan2(enuvel(1),enuvel(2))
            trk_rad = rdir(r_a,r_e2,sc_hdg,lat)
            call norm(vel,v)
            call norm(pos,sc_r)
            elp%r_a = r_a
            elp%r_e2 = r_e2
            call latlon(elp,pos,r_llh,i_xyztollh)
            h  = r_llh(3)
            re = trk_rad
            rr = r0+(1.d-3*rsamp*nrmax/2)
            pix = (rr-rd_ref)/(1.d-3*rsamp_dopp)
            dop = fd_coef_hertz(1) + fd_coef_hertz(2) * pix +
     $           fd_coef_hertz(3) * pix**2 + fd_coef_hertz(4) * pix**3
            th=acos(((h+re)**2+rr*rr-re**2)/(2.d0*rr*(re+h)))
            r_sc_az_nom = lr * (pi/2. - asin(dop * wvl/(2.d0*v*sin(th)))
     $           )
         end if

         write (*,*)  'Updated Nominal squint angle ',r_sc_az_nom/dtor

         call unitvec(sc_losv,u_lk_xyz)
         call matvec(xyz2enu,sc_losv,lk_enu)
         call matvec(xyz2enu,u_lk_xyz,u_lk_enu)
         write (*,1001) 'Sim cen slant range (km)' , sim_cen_range
         write (*,1001) 'Sim cen time (s)' , sim_cen_time
         write (*,1001) 'Sim cen look angle (deg)' , look_angle/dtor
         write (*,1003) 'Sim cen look vector (body-fixed xyz) (km)' , sc_losv
         write (*,1003) 'Sim cen unit look vector (body-fixed xyz)' , u_lk_xyz
         write (*,1003) 'Sim cen look vector (ENU) (km)' , lk_enu
         write (*,1003) 'Sim cen unit look vector (ENU)' , u_lk_enu
         write (*,*)
         write (*,*) '*** Slant range to and time of first pixel being adjusted'
         write (*,1001) '*** New slant range to first pixel (km)' , r0
         write (*,1001) '*** New time of first pixel (s)' , t0
      endif


ccccc to determine lat,lon edge values for UTM DEM

      if (dem_spacing_flag .eq. 2) then

         dellat = dellat * dtor
         dellon = dellon * dtor

      elseif (dem_spacing_flag .eq. 1) then

         deleast = dellon / 1.d3 ! convert from m to km
         delnorth = dellat / 1.d3 ! convert from m to km
c calculate spacing of DEM in radians at DEM corner 
         if ( dem_corner_flag .eq. 1) then ! NAD27 datum
            dellat = delnorth / rnorth(r_a1866,r_e21866,latcorner)
            dellon = deleast / (reast(r_a1866,r_e21866,latcorner) * cos(latcorner))
         elseif ( dem_corner_flag .eq. 4) then ! WGS84 datum
            dellat = delnorth / rnorth(r_a,r_e2,latcorner)
            dellon = deleast / (reast(r_a,r_e2,latcorner) * cos(latcorner))
         else
            write (*,*)'inconsistent dem_corner_flag',dem_corner_flag,' and dem_spacing_flag',dem_spacing_flag
            stop
         endif
         r_lat_edg(1) = latcorner
cccccccc determine DEM center longitude
         if (dem_corner_flag .eq. 1) then
            r_v(2) = r_vs(2)
            r_v(1) = r_vs(1) + deleast * 1000.d0 * (nint(float(ntotlonpx) / 2.d0) - 1)
            elp%r_a = r_a1866m
            elp%r_e2 = r_e21866
            call utmtoll(elp,i_zone,a_grid,r_v,lat,lon,i_utmtollh)
            r_cen_lon = lon
         elseif (dem_corner_flag .eq. 2) then
            r_cen_lon = loncorner
            do i = 1 , nint(float(ntotlonpx) / 2.d0) - 1
               r_cen_lon = r_cen_lon + dellon
            enddo
         elseif (dem_corner_flag .eq. 3) then
            r_v(2) = r_vs(2)
            r_v(1) = r_vs(1) + delc * (nint(float(ntotcpx) / 2.d0) - 1)
            dem_sch(1) = r_v(2)
            dem_sch(2) = r_v(1)
            dem_sch(3) = 0.d0                     ! or should I use the height value from the DEM?
            call convert_sch_to_xyz(ptm,dem_sch,dem_xyz,i_schtoxyz)
            elp%r_a = r_am
            elp%r_e2 = r_e2
            call latlon(elp,dem_xyz,r_llh,i_xyztollh)
            lat = r_llh(1)
            lon = r_llh(2)
            r_cen_lon = lon
         else ! WGS84 datum UTM
            r_v(2) = r_vs(2)
            r_v(1) = r_vs(1) + deleast * 1000.d0 * (nint(float(ntotlonpx) / 2.d0) - 1)
            elp%r_a = r_am
            elp%r_e2 = r_e2
            call utmtoll(elp,i_zone,a_grid,r_v,lat,lon,i_utmtollh)
         endif
cccccccc ?????
         do i = 2 , ntotlatpx
            if (dem_corner_flag .eq. 1) then
               r_lat_edg(i) = r_lat_edg(i-1) + delnorth/rnorth(r_a1866,r_e21866,r_lat_edg(i-1))
               dellon = deleast / (reast(r_a1866,r_e21866,r_lat_edg(i)) * cos(r_lat_edg(i)))
            else                ! WGS84 datum
               r_lat_edg(i) = r_lat_edg(i-1) + delnorth/rnorth(r_a,r_e2,r_lat_edg(i-1))
               dellon = deleast / (reast(r_a,r_e2,r_lat_edg(i)) * cos(r_lat_edg(i)))
            endif

            if (i .eq. (nint(float(ntotlatpx)/2.d0)-1)) then
c               write(*,1001) 'DEM center latitude  (deg)' , r_lat_edg(i)/dtor
            endif

            r_lon_edg(i) = r_cen_lon
            do j = 1 , nint(float(ntotlonpx)/2.d0) - 1
               r_lon_edg(i) = r_lon_edg(i) - dellon
            enddo
c     write (77,*) i , r_lat_edg(i)/dtor , r_lon_edg(i)/dtor
c     write (79,*) i , dellat/dtor , dellon/dtor
            if (dem_corner_flag .eq. 1) then ! NAD27
               r_v(2) = r_v(2) + delnorth * 1000.d0
               r_v(1) = r_vs(1) 
               elp%r_a = r_a1866m
               elp%r_e2 = r_e21866
               call utmtoll(elp,i_zone,a_grid,r_v,lat,lon,i_utmtollh)
               r_lon_edg(i) = lon
               r_lat_edg(i) = lat
c     write (78,*) i , lat/dtor , lon/dtor
c     if (i .eq. (nint(float(ntotlatpx)/2.d0)-1)) then
c     write (*,1001) 'DEM center lat (deg)' , r_lat_edg(i)/dtor
c     write (*,*)
c     endif
            else                ! WGS84
               r_v(2) = r_v(2) + delnorth * 1000.d0
               r_v(1) = r_vs(1) 
               elp%r_a = r_am
               elp%r_e2 = r_e2
               call utmtoll(elp,i_zone,a_grid,r_v,lat,lon,i_utmtollh)
               r_lon_edg(i) = lon
               r_lat_edg(i) = lat

            endif
         enddo

c already calculated above EJF 00/8/3
c$$$         if (dem_corner_flag .eq. 1) then ! NAD27
c$$$            dellat = delnorth / rnorth(r_a1866,r_e21866,latcorner)
c$$$            dellon = deleast / (reast(r_a1866,r_e21866,latcorner) * cos(latcorner))
c$$$         else                   ! WGS84
c$$$            dellat = delnorth / rnorth(r_a,r_e2,latcorner)
c$$$            dellon = deleast / (reast(r_a,r_e2,latcorner) * cos(latcorner))
c$$$         endif

      elseif (dem_spacing_flag .eq. 3) then   ! convert spacing from SCH (m) to lat, lon (rad)

ccccc nothing needed here

      endif


ccccc determine spacecraft position and velocity at time t0

      call orrmread1(16,t0,pos,vel,0) ! get body fixed state vector
      elp%r_a = r_a
      elp%r_e2 = r_e2
      call latlon(elp,pos,r_llh,i_xyztollh)
      lat = r_llh(1)
      lon = r_llh(2)
      rad = r_llh(3)
      call enubasis(lat,lon,enumat)
      call tranmat(enumat,xyz2enu)
      call matvec(enumat,r_bias,r_enubias)
      call matvec(xyz2enu,vel,enuvel)
      sc_hdg = atan2(enuvel(1),enuvel(2))
      trk_rad = rdir(r_a,r_e2,sc_hdg,lat)
      call norm(pos,sc_r)
      call norm(vel,sc_vel)
      call getTCN_TCvec(pos,vel,vel,tc_vec)
      call norm(tc_vec, tc_vel)

ccccc write out a few things

      write (*,*)
      write (*,*)
      write (*,*) 'At the initial time:'
      write (*,*)
      write (*,1003) 'Spacecraft position vector (km)' , pos
      write (*,1003) 'Spacecraft velocity vector (km/s)' , vel
      write (*,1003) 'Spacecraft TC velocity vector (km/s)' , tc_vec
      write (*,1001) 'Spacecraft heading (deg)' , sc_hdg/dtor
      write (*,1001) 'Spacecraft radius (km)' , sc_r
      write (*,1001) 'Along track radius of curvature (km)' , trk_rad
      write (*,1001) 'Spacecraft speed (km)' , sc_vel
      write (*,1001) 'Spacecraft ground speed (km/s)' , sc_vel * trk_rad / sc_r
      write (*,1001) 'Spacecraft track speed (km)' , tc_vel
      write (*,1001) 'Spacecraft ground track speed (km/s)' , tc_vel * (sc_r - rad ) / sc_r
      write (*,1003) 'ENU bias vector (km)' , r_enubias


ccccc geolocation of simulation output window corners

      write (*,*)
      write (*,*)
      write (*,*) 'Geolocating corners of simulation output window:'


ccccc determine time of and slant range to simulation output window corners

      sim_corner_time(1)  = t0
      sim_corner_time(2)  = t0
      sim_total_time = (1.d-3 * asamp) * namax * sc_r / (tc_vel * trk_rad)
      sim_corner_time(3)  = t0 + sim_total_time
      sim_corner_time(4)  = t0 + sim_total_time

      sim_corner_range(1) = r0
      sim_corner_range(2) = r0 + (1.d-3 * rsamp) * nrmax
      sim_corner_range(3) = r0
      sim_corner_range(4) = r0 + (1.d-3 * rsamp) * nrmax


ccccc begin loop over simulation corners

      do j = 1 , 4

         time  = sim_corner_time(j)
         range = sim_corner_range(j)

         write (*,*)
         write (*,'(1x,a,i1,a)') 'Corner ' , j , ' (on ellipsoid):'
         write (*,1001) 'Slant range (km)' , range
         write (*,1001) 'Time (s)' , time


ccccc determine position and velocity of spacecraft at corner point

         call orrmread1(16,time,pos,vel,0)
         call norm(pos,sc_r)
         call norm(vel,sc_vel)
         elp%r_a = r_a
         elp%r_e2 = r_e2
         call latlon(elp,pos,r_llh,i_xyztollh)
         sc_lat = r_llh(1)
         sc_lon = r_llh(2)
         sc_h = r_llh(3)
         call enubasis(sc_lat,sc_lon,enumat)
         call tranmat(enumat,xyz2enu)
         call matvec(enumat,r_bias,r_enubias)
         call matvec(xyz2enu,vel,enuvel)
         sc_hdg = atan2(enuvel(1),enuvel(2))


ccccc solve law of cosines to determine look angle to reference ellipsoid

         elp%r_a = r_a
         elp%r_e2 = r_e2
         peg%r_lat = sc_lat
         peg%r_lon = sc_lon
         peg%r_hdg = sc_hdg+r_sc_az_nom
         img_pln_rad = rdir(r_a,r_e2,sc_hdg+r_sc_az_nom,sc_lat)
         call radar_to_xyz(elp,peg,ptm2)
         call convert_sch_to_xyz(ptm2,sc_sch,pos,i_xyztosch)

         target_d = img_pln_rad
         sc_d = img_pln_rad + sc_sch(3)
         look_angle = acos((sc_d**2 + range**2 - target_d**2) / (2.d0 * sc_d * range))
         if (j .eq. 1) then
            look_angle_0 = look_angle  ! used for DEM padding
            sc_hdg_0 = sc_hdg          ! used for DEM padding
         endif

ccccc construct look vector (in SCH coord.) from computed look angle

         u_lk(1) = sin(look_angle)
         u_lk(2) = 0.d0
         u_lk(3) = -cos(look_angle)
c         write (*,*)  'look info ', look_angle/dtor, u_lk

ccccc compute xyz vector from earth center to ellipsoid

         do i = 1 , 3
            sim_corner_xyz(i) = sc_sch(i) + u_lk(i) * range
         enddo
c     write (*,*) 'sim_corner_xyz' , sim_corner_xyz
         r_m = sqrt(sim_corner_xyz(1)**2 + sim_corner_xyz(2)**2)
         tanm = r_m / (img_pln_rad+sim_corner_xyz(3))
         sinm = r_m / (r_m**2+(img_pln_rad+sim_corner_xyz(3))**2)
         cosg = sim_corner_xyz(1) / r_m
         sing = sim_corner_xyz(2) / r_m
         sim_corner_sch2(1) = img_pln_rad * atan(tanm * cosg)
         sim_corner_sch2(2) = img_pln_rad * asin(sinm * sing)
         sim_corner_sch2(3) = sqrt((img_pln_rad + sim_corner_xyz(3))**2 + r_m**2) - img_pln_rad
         call convert_sch_to_xyz(ptm2,sim_corner_sch2,sim_corner_xyz,i_schtoxyz)

         elp%r_a = r_a
         elp%r_e2 = r_e2
         call latlon(elp,pos,r_llh,i_xyztollh)
         lat = r_llh(1)
         lon = r_llh(2)
         rad = r_llh(3)


ccccc compute lat, lon of corner point

         call latlon(elp,sim_corner_xyz,r_llh,i_xyztollh)
         sim_corner_lat = r_llh(1)
         sim_corner_lon = r_llh(2)
         sim_corner_h   = r_llh(3)

         if (dem_spacing_flag .eq. 1) then
                                ! convert corner to UTM meters using DEM zone (output r_v)
            if (dem_corner_flag .eq. 1) then ! NAD27
               elp%r_a = r_a1866m
               elp%r_e2 = r_e21866
               call utmtoll(elp,-i_zone,a_grid,r_v,sim_corner_lat,sim_corner_lon,i_llhtoutm)
            else                ! WGS84
               elp%r_a = r_am
               elp%r_e2 = r_e2
               call utmtoll(elp,-i_zone,a_grid,r_v,sim_corner_lat,sim_corner_lon,i_llhtoutm)
            endif
            write(*,1002)'SAR corner UTM northing, easting',r_v(2),r_v(1)
            dem_pixel(1) = ((r_v(2) - r_vs(2)) / (delnorth*1000.)) + 1.d0
            dem_pixel(2) = ((r_v(1) - r_vs(1)) / (deleast*1000.)) + 1.d0
         elseif (dem_spacing_flag .eq. 2) then
            dem_pixel(1) = ((sim_corner_lat - latcorner) / dellat) + 1.d0
            dem_pixel(2) = ((sim_corner_lon - loncorner) / dellon) + 1.d0
         elseif (dem_spacing_flag .eq. 3) then
            sim_corner_xyzm(1) = sim_corner_xyz(1) * 1000.d0
            sim_corner_xyzm(2) = sim_corner_xyz(2) * 1000.d0
            sim_corner_xyzm(3) = sim_corner_xyz(3) * 1000.d0
            call convert_sch_to_xyz(ptm,sim_corner_sch,sim_corner_xyzm,i_xyztosch)
            dem_pixel(1) = ((sim_corner_sch(1) - scorner) / dels) + 1.d0
            dem_pixel(2) = ((sim_corner_sch(2) - ccorner) / delc) + 1.d0
         endif

         do i = 1 , 2
            if (j .eq. 1) then
               dem_pixel_max(i) = dem_pixel(i)
               dem_pixel_min(i) = dem_pixel(i)
            else
               if (dem_pixel(i) .gt. dem_pixel_max(i)) dem_pixel_max(i) = dem_pixel(i)
               if (dem_pixel(i) .lt. dem_pixel_min(i)) dem_pixel_min(i) = dem_pixel(i)
            endif
         enddo


ccccc write out a few things

         do i = 1 , 3
            lk_xyz(i) = sim_corner_xyz(i) - pos(i)
         enddo
         call unitvec(lk_xyz,u_lk_xyz)
         call matvec(xyz2enu,lk_xyz,lk_enu)
         call matvec(xyz2enu,u_lk_xyz,u_lk_enu)
         write (*,1001) 'Look angle (deg)', look_angle/dtor
         write (*,1003) 'Look vector (body-fixed xyz) (km)' , lk_xyz
         write (*,1003) 'Unit look vector (body-fixed xyz)' , u_lk_xyz
         write (*,1003) 'Look vector (ENU) (km)' , lk_enu
         write (*,1003) 'Unit look vector (ENU)' , u_lk_enu
         write (*,1002) 'Latitude and longitude (deg)' , sim_corner_lat/dtor , sim_corner_lon/dtor
         write (*,1012) 'DEM pixel (col , row)' , nint(dem_pixel(2)) , nint(dem_pixel(1))

      enddo

ccccc end loop over simulation corners


ccccc compute minimum DEM size (with pad) needed to fill range doppler image
ccccc update max and min DEM pixels to take into consideration topography

      max_height = 5.d0             ! km
      img_pln_pad = max_height * tan(look_angle_0)
      northing_pad = abs(cos(sc_hdg_0+r_sc_az_nom) * img_pln_pad)
      easting_pad  = abs(sin(sc_hdg_0+r_sc_az_nom) * img_pln_pad)

      max_lat = sim_corner_lat
      lat_pad = northing_pad / trk_rad
      lon_pad = easting_pad  / (trk_rad * cos(max_lat))
! removed following unnecessary line EJF 00/8/3
      if (dem_spacing_flag .eq. 1) then
         lat = r_lat_edg(j+nslatpx-1)
         if ( dem_corner_flag .eq. 1 ) then ! NAD27
            dellon = deleast / (reast(r_a1866,r_e21866,lat) * cos(lat))
         else                   ! WGS84
            dellon = deleast / (reast(r_a,r_e2,lat) * cos(lat))
         endif
         dem_pixel_max(1) = dem_pixel_max(1) + abs(lat_pad/dellat)
         dem_pixel_min(1) = dem_pixel_min(1) - abs(lat_pad/dellat)
         dem_pixel_max(2) = dem_pixel_max(2) + abs(lon_pad/dellon)
         dem_pixel_min(2) = dem_pixel_min(2) - abs(lon_pad/dellon)
         loncorner = r_lon_edg(j+nslatpx-1)
      elseif (dem_spacing_flag .eq. 2) then
         dem_pixel_max(1) = dem_pixel_max(1) + abs(lat_pad/dellat)
         dem_pixel_min(1) = dem_pixel_min(1) - abs(lat_pad/dellat)
         dem_pixel_max(2) = dem_pixel_max(2) + abs(lon_pad/dellon)
         dem_pixel_min(2) = dem_pixel_min(2) - abs(lon_pad/dellon)
      elseif (dem_spacing_flag .eq. 3) then
         s_pad = abs(cos(sc_hdg_0-sch_dem_peg_hdg+r_sc_az_nom) * img_pln_pad * 1000.d0)
         c_pad = abs(sin(sc_hdg_0-sch_dem_peg_hdg+r_sc_az_nom) * img_pln_pad * 1000.d0)
         dem_pixel_max(1) = dem_pixel_max(1) + abs(s_pad/dels)
         dem_pixel_min(1) = dem_pixel_min(1) - abs(s_pad/dels)
         dem_pixel_max(2) = dem_pixel_max(2) + abs(c_pad/delc)
         dem_pixel_min(2) = dem_pixel_min(2) - abs(c_pad/delc)
      endif
!MP
! latitude N sous estimee
      nslatpx = int(dem_pixel_min(1))-70
      nslonpx = int(dem_pixel_min(2))
      nlatpx = nint(dem_pixel_max(1)) - nslatpx + 1
      nlonpx = nint(dem_pixel_max(2)) - nslonpx + 1

      if (dem_spacing_flag .eq. 3) then
         ntotlatpx = ntotspx
         ntotlonpx = ntotcpx
      endif


      write (*,*)
      write (*,*)
      write (*,*) 'DEM section over which to loop:'
      write (*,*)

      if ((dem_spacing_flag .eq. 1) .or. (dem_spacing_flag .eq. 2)) then
         write (*,1012) 'DEM padding for 5 km max topo (pixels)' , nint(abs(lat_pad/dellat)) , nint(abs(lon_pad/dellon))
      elseif (dem_spacing_flag .eq. 3) then
         write (*,1012) 'DEM padding for 5 km max topo (pixels)' , nint(abs(s_pad/dels)) , nint(abs(c_pad/delc))
      endif
      write (*,*)

      if ((nslatpx .gt. ntotlatpx) .or. (nslonpx .gt. ntotlonpx) .or.
     &    ((nslatpx+nlatpx-1) .lt. 1) .or. ((nslonpx+nlonpx-1) .lt. 1)) then
         write (*,*)
         write (*,*) '*** DEM does not fall within simulation'
         write (*,*)
         stop
      endif

      if (nslatpx .lt. 1) then
         write (*,*) '*** First DEM row to simulate outside DEM given - setting to 1'
         write (*,*)
         nlatpx = nlatpx - (1 - nslatpx)
         nslatpx = 1
      endif

      if (nslonpx .lt. 1) then
         write (*,*) '*** First DEM column to simulate outside DEM given - setting to 1'
         write (*,*)
         nlonpx = nlonpx - (1 - nslonpx)
         nslonpx = 1
      endif

      if ((nslatpx+nlatpx-1) .gt. ntotlatpx) then
         write (*,*) '*** Last DEM row to simulate outside DEM given - setting to ' , ntotlatpx
         write (*,*)
         nlatpx = ntotlatpx - nslatpx + 1
      endif

      if ((nslonpx+nlonpx-1) .gt. ntotlonpx) then
         write (*,*) '*** Last DEM column to simulate outside DEM given - setting to ' , ntotlonpx
         write (*,*)
         nlonpx = ntotlonpx - nslonpx + 1
      endif

      first_pixel(1) = nslatpx
      first_pixel(2) = nslonpx
      last_pixel(1)  = nslatpx + nlatpx - 1
      last_pixel(2)  = nslonpx + nlonpx - 1
      dem_fraction = float(nlatpx * nlonpx) / float(ntotlonpx * ntotlatpx)

      write (*,1012) 'First DEM pixel (col , row)' , first_pixel(2), first_pixel(1)
      write (*,1012) 'Last  DEM pixel (col , row)' , last_pixel(2) , last_pixel(1)
      write (*,1012) 'DEM dimensions (col pixels, row pixels)', ntotlonpx , ntotlatpx 
      write (*,1001) 'Fraction of DEM to be used (%)' , 100.d0 * dem_fraction

ccccc read in gps input file

      write (*,*)
      write (*,*)
      write (*,*) 'Scanning GPS input file...'
      i_gpscnt = 1
      do while (1 .eq. 1)
         read (25,*,end=27) (r_junk(j),j=1,9)
         i_gpscnt = i_gpscnt + 1
      end do
 27   i_gpscnt = i_gpscnt - 1

      write(*,*)
      write(*,1011) 'Number of GPS points' , i_gpscnt
      write(*,*)

      write(*,*) 'Reading GPS input file...'

      close(25)

      ALLOCATE( r_gpslon(i_gpscnt) )
      ALLOCATE( r_gpslat(i_gpscnt) )
      ALLOCATE( r_gpslonorg(i_gpscnt) )
      ALLOCATE( r_gpslatorg(i_gpscnt) )
      ALLOCATE( r_edis(i_gpscnt) )
      ALLOCATE( r_ndis(i_gpscnt) )
      ALLOCATE( r_eerr(i_gpscnt) )
      ALLOCATE( r_nerr(i_gpscnt) )
      ALLOCATE( r_udis(i_gpscnt) )
      ALLOCATE( r_uerr(i_gpscnt) )
      ALLOCATE( r_losdisp(i_gpscnt) )

      open (25,file=gpsinfile,status='old')

      do j = 1 , i_gpscnt
         read (25,*) r_gpslon(j), r_gpslat(j), r_edis(j),
     &                      r_ndis(j), r_eerr(j), r_nerr(j),
     &                      r_junk(1), r_udis(j), r_uerr(j)
         r_gpslon(j)    = r_gpslon(j) * dtor
         r_gpslonorg(j) = r_gpslon(j)
         r_gpslat(j)    = r_gpslat(j) * dtor
         r_gpslatorg(j) = r_gpslat(j)

c this section is converting GPS points to NAD27 datum with bias correction
c should be updated to keep them in WGS84, but I'm not sure how to do that EJF 99/7/22

         write (*,*)
         write (*,'(1x,a,i3,a)') 'GPS point ' , j , ':'
         write (*,1002) 'Latitude and longitude (WGS-84) (deg)' , r_gpslat(j)/dtor ,r_gpslon(j)/dtor

         r_h = 0.d0
         elp%r_a = r_a
         elp%r_e2 = r_e2
         r_llh(1) = r_gpslat(j)
         r_llh(2) = r_gpslon(j)
         r_llh(3) = r_h
         call latlon(elp,tpv,r_llh,i_llhtoxyz)
         do i = 1 , 3
            tpv(i) = tpv(i) - r_bias(i)
         enddo
         elp%r_a = r_a1866
         elp%r_e2 = r_e21866
         call latlon(elp,tpv,r_llh,i_xyztollh)
         r_gpslat(j) = r_llh(1)
         r_gpslon(j) = r_llh(2)
         r_h = r_llh(3)
         elp%r_a = r_a1866m
         elp%r_e2 = r_e21866
         call utmtoll(elp,i_zone,a_grid,r_v,r_gpslat(j),r_gpslon(j),i_llhtoutm)

         write (*,1002) 'Northing and easting (Clarke 1866) (m)' , r_v(2) , r_v(1)

         r_v(2) = r_v(2) + r_nebias(2) 
         r_v(1) = r_v(1) + r_nebias(1)
         elp%r_a = r_a1866m
         elp%r_e2 = r_e21866
         call utmtoll(elp,i_zone,a_grid,r_v,r_gpslat(j),r_gpslon(j),i_utmtollh)

         write (*,1002) 'Northing and easting (sim datum) (m)' , r_v(2) , r_v(1)
         write (*,1002) 'Latitude and longitude (sim datum) (deg)' , r_gpslat(j)/dtor , r_gpslon(j)/dtor

      enddo

      close(25)

      allocate(rsave(nlonpx),rsavep(nlonpx),azmsave(nlonpx),azmsavep(nlonpx))
      allocate(csave(nlonpx),csavep(nlonpx))
ccccc looping over DEM

      write(*,*)
      write(*,*) 'Looping over DEM...'
      write(*,*)

      i_type = 1
      tslon = t0 + sim_total_time/2.
      mod_counter = 0

      do j = 1 , nlatpx, i_skip

         r_sc_az = r_sc_az_nom
         mod_counter = mod_counter + 1
         ts = tslon
c         lat = latcorner + float(j+nslatpx-2) * dellat
         if (dem_spacing_flag .eq. 1) then
            lat = r_lat_edg(j+nslatpx-1)
            if ( dem_corner_flag .eq. 1 ) then ! NAD27
               dellon = deleast / (reast(r_a1866,r_e21866,lat) * cos(lat))
            else                ! WGS84
               dellon = deleast / (reast(r_a,r_e2,lat) * cos(lat))
            endif
            loncorner = r_lon_edg(j+nslatpx-1)
         elseif (dem_spacing_flag .eq. 2) then
            lat = latcorner + float(j+nslatpx-2) * dellat
         elseif (dem_spacing_flag .eq. 3) then
            r_s = scorner + float(j+nslatpx-2) * dels
         endif

         if (dem_corner_flag .eq. 1) then ! NAD27
            r_v(2) = r_vs(2) + delnorth*(j+nslatpx-2)*1000.d0 
            r_v(1) = r_vs(1) + deleast*(nlonpx/2+nslonpx-2)*1000.d0
            elp%r_a = r_a1866m
            elp%r_e2 = r_e21866
            call utmtoll(elp,i_zone,a_grid,r_v,lat,lon,i_utmtollh)
         elseif (dem_corner_flag .eq. 3) then
            r_v(2) = r_vs(2) + dels * (j+nslatpx-2)
            r_v(1) = r_vs(1) + delc * (nlonpx/2+nslonpx-2)
            dem_sch(1) = r_v(2)
            dem_sch(2) = r_v(1)
            dem_sch(3) = 0.d0                     ! or should I use the height value from the DEM?
            call convert_sch_to_xyz(ptm,dem_sch,dem_xyz,i_schtoxyz)
            elp%r_a = r_am
            elp%r_e2 = r_e2
            call latlon(elp,dem_xyz,r_llh,i_xyztollh)
            lat = r_llh(1)
            lon = r_llh(2)
         elseif (dem_corner_flag .eq. 4) then ! WGS84
            r_v(2) = r_vs(2) + delnorth*(j+nslatpx-2)*1000.d0 
            r_v(1) = r_vs(1) + deleast*(nlonpx/2+nslonpx-2)*1000.d0
            elp%r_a = r_am
            elp%r_e2 = r_e2
            call utmtoll(elp,i_zone,a_grid,r_v,lat,lon,i_utmtollh)

         endif

c     check to see if any of the gps data points are at this latitude
         
         i_gpsflag = 0
         do i_g = 1 , i_gpscnt
            if ((dem_corner_flag .eq. 1) .or. (dem_corner_flag .eq. 2)
     $           .or. (dem_corner_flag .eq. 4)) then
               if (abs(r_gpslat(i_g)-lat) .lt. 20.d0*abs(dellat)) then
                  i_gpsflag = 1
               endif
            elseif (dem_corner_flag .eq. 3) then
               r_h = 0.d0
               elp%r_a = r_a1866
               elp%r_e2 = r_e21866
               r_llh(1) = r_gpslat(i_g) 
               r_llh(2) = r_gpslon(i_g)
               r_llh(3) = r_h
               call latlon(elp,tpv_dum,r_llh,i_llhtoxyz)
               call convert_sch_to_xyz(ptm,sch_dum,tpv_dum,i_xyztosch)
               r_gpss = sch_dum(1) 
               if (abs(r_gpss-r_s) .lt. 20.d0*abs(dels)) then
                  i_gpsflag = 1
               endif
            endif
         enddo

         hgtarrp(:)=hgtarr(:)

         read (31,rec=j+nslatpx-1) (hgtarr(k) , k = 1 , ntotlonpx)
         read (32,rec=j+nslatpx-1) (slparr(k) , k = 1 , ntotlonpx)

         rsavep(:)=rsave(:)
         csavep(:)=csave(:)
         azmsavep(:)=azmsave(:)
c     loop over DEM pixels in line
         do i = 1 , nlonpx, i_skip

c     check if pixel height value is valid--assume negative and zero values before scale and offset are 'no data'
c added EJF 00/1/7
            if ( (hgtarr(i+nslonpx-1) .lt. 25000) .and. 
     &           (hgtarr(i+nslonpx-1) .gt. -1000 )) then         ! good elev.
               bad_elev = .FALSE.
            else
               bad_elev = .TRUE.
            endif

            lon = loncorner + float(i+nslonpx-2) * dellon
            
            if (dem_corner_flag .eq. 1) then
               r_v(2) = r_vs(2) + delnorth*(j+nslatpx-2)*1000.d0 
               r_v(1) = r_vs(1) + deleast*(i+nslonpx-2)*1000.d0
               elp%r_a = r_a1866m
               elp%r_e2 = r_e21866
               call utmtoll(elp,i_zone,a_grid,r_v,lat,lon,i_utmtollh)
            elseif (dem_corner_flag .eq. 2) then
               lon = loncorner + float(i+nslonpx-2) * dellon
            elseif (dem_corner_flag .eq. 3) then
               r_v(2) = r_vs(2) + dels * (j+nslatpx-2)
               r_v(1) = r_vs(1) + delc * (i+nslonpx-2)
               dem_sch(1) = r_v(2)
               dem_sch(2) = r_v(1)
               dem_sch(3) = 0.d0                     ! or should I use the height value from the DEM?
               call convert_sch_to_xyz(ptm,dem_sch,dem_xyz,i_schtoxyz)
               elp%r_a = r_am
               elp%r_e2 = r_e2
               call latlon(elp,dem_xyz,r_llh,i_xyztollh)
               lat = r_llh(1)
               lon = r_llh(2)
               if (((i .eq. 1) .or. (i .eq. nlonpx)) .and. (mod(j,32) .eq. 0)) then
                  write (*,1001) 'Latitude  (deg)' , lat/dtor
                  write (*,1001) 'Longitude (deg)' , lon/dtor
               endif
            elseif (dem_corner_flag .eq. 4) then ! WGS84
               r_v(2) = r_vs(2) + delnorth*(j+nslatpx-2)*1000.d0 
               r_v(1) = r_vs(1) + deleast*(i+nslonpx-2)*1000.d0
               elp%r_a = r_am
               elp%r_e2 = r_e2
               call utmtoll(elp,i_zone,a_grid,r_v,lat,lon,i_utmtollh)
               if (((i .eq. 1) .or. (i .eq. nlonpx)) .and. (mod(j,32) .eq. 0)) then
                  write (*,1001) 'Latitude  (deg)' , lat/dtor
                  write (*,1001) 'Longitude (deg)' , lon/dtor
               endif
             
            endif

            
            if ( bad_elev ) then
               rad = trk_rad    ! default zero elevation, OK?

            else                ! good elevation
               rad = (float(hgtarr(i+nslonpx-1)) - alt_bias)*alt_scale/1.d3 + trk_rad
            endif

            r_llh(1) = lat
            r_llh(2) = lon
            r_llh(3) = rad-trk_rad

            if (dem_corner_flag .eq. 1) then ! NAD27
               elp%r_a = r_a1866
               elp%r_e2 = r_e21866
               call latlon(elp,tpv,r_llh,i_llhtoxyz)
            else                ! WGS84
               elp%r_a = r_a
               elp%r_e2 = r_e2
               call latlon(elp,tpv,r_llh,i_llhtoxyz)
            endif
            call enubasis(lat,lon,enumat)
            call tranmat(enumat,xyz2enu)
            
            if ((i_gpsflag .eq. 1) .and. (i_dogps .eq. 1) .and. (i_dosim .eq. 0)) then
               if ((i .eq. 1) .and. (j .eq. 1)) then
! Changed here to take into account very long stripes, so that tested duration does'nt exceed the extrcted orbit length
                  twin = max(30.0d0,sim_total_time*1.1d0)  ! need larger search for multiple scenes
               elseif (i .eq. 1) then
                  twin = 30.d0
               else
                  twin = 30.d0
               endif
               ts = zbrent(fc,ts-twin,ts+twin,tol) ! solve for position of s/c
            else
               tslast = ts
               if ((i .eq. 1) .and. (j .eq. 1)) then
                  twin = max(30.0d0,sim_total_time*1.1d0)  ! need larger search for multiple scenes
               elseif (i .eq. 1) then
                  twin = 0.1d0 * i_skip
               else
                  twin = 0.01d0 * i_skip
               endif
               ts = zbrent(fc,ts-twin,ts+twin,tol) ! solve for position of s/c
               if (ts .eq. 1.d9) then
                  i_redcnt = 0
                  do while(ts .gt. 1.e8 .and. i_redcnt .lt. 10)
                     twinred = (i_redcnt+2)*.01d0 + twin
                     ts = zbrent(fc,tslast-twinred,tslast+twinred,tol) ! solve for position of s/c
                     i_redcnt = i_redcnt+1
                  enddo
                  if (ts .gt. 1.d8) then
                     if ((i .eq. 1) .and. (j .eq. 1)) then
                        write(*,*)'failed to find s/c location'
                        write(*,*)'tslast =',tslast,' ts=',ts
                        stop
                     endif

                     ts = tslast
                  endif
               endif
            endif

c solution now close enough for refinement of imaging angle if required

            if(i_type_fc .eq. 1) then
c               call orrmread1(16,ts,pos,vel,0)
c               call lincomb(-1.d0,pos,1.d0,tpv,sc_losv)
c               call norm(sc_losv,rr)
c               call norm(vel,v)
c               call norm(pos,sc_r)
c               elp%r_a = r_a
c               elp%r_e2 = r_e2
c               call latlon(elp,pos,r_llh,i_xyztollh)
c               h  = r_llh(3)
c               re = trk_rad
c               pix = (rr-rd_ref)/(rsamp_dopp*1.d-3)
c               dop = fd_coef_hertz(1) + fd_coef_hertz(2) * pix +
c     $              fd_coef_hertz(3) * pix**2 + fd_coef_hertz(4) * pix**3
c               th=acos(((h+re)**2+rr*rr-re**2)/(2.d0*rr*(re+h)))
       
c	       call latlon(elp,pos,r_llh,i_xyztollh)
c               call enubasis(r_llh(1),r_llh(2),enumat)
c               call tranmat(enumat,xyz2enu_sat)
c               call matvec(xyz2enu_sat,vel,enuvel)
c	       sc_pitch = atan2( enuvel(3) , sqrt( enuvel(1)**2 + enuvel(2)**2 ) );
       
c	       write (*,*) "pitch:", sc_pitch/dtor

       
c	       write(*,*) "----"
c	       write(*,*) "dop target before:", dop

c               r_sc_az = lr*(pi/2.d0 - asin(dop * wvl/(2.d0*v*sin(th))))
c	       write(*,*) "r_sc_az:", r_sc_az/dtor
c               r_sc_az = lr*(pi/2.d0 - asin(dop * wvl/(2.d0*v*sin(th)*cos(sc_pitch)) + tan(sc_pitch)/tan(th)))
c	       write(*,*) "r_sc_az:", r_sc_az/dtor
c               twin = 0.3d0 * i_skip
c               ts = zbrent(fc,ts-twin,ts+twin,tol) ! solve for position of s/c

       
c               call orrmread1(16,ts,pos,vel,0)
c               call lincomb(-1.d0,pos,1.d0,tpv,sc_losv)
c               call norm(sc_losv,rr)
c               call norm(vel,v)
c               call norm(pos,sc_r)
c               h  = sc_r-trk_rad
c               re = trk_rad
c               pix = (rr-rd_ref)/(rsamp_dopp*1.d-3)
c               th=acos(((h+re)**2+rr*rr-re**2)/(2.d0*rr*(re+h)))
       
c	    write (*,*) "dop found:", 1.d3 * ( vel(1) * sc_losv(1) / rr
c     &	                                      +vel(2) * sc_losv(2) / rr  
c     &	                                      +vel(3) * sc_losv(3) / rr ) * 2. / ( wvl * 1000. )

c               dop = dop + fd_coef_hertz(1) + fd_coef_hertz(2) * pix +
c     $                fd_coef_hertz(3) * pix**2 + fd_coef_hertz(4) * pix**3
c     $              - 1.d3 * ( vel(1) * sc_losv(1) / rr
c     &	                      +vel(2) * sc_losv(2) / rr  
c     &	                      +vel(3) * sc_losv(3) / rr ) * 2. / ( wvl * 1000. )

c     
c	       write(*,*) "dop target after:", dop
c               r_sc_az = lr*(pi/2.d0 - asin(dop * wvl/(2.d0*v*sin(th))))
c               r_sc_az = lr*(pi/2.d0 - asin(dop * wvl/(2.d0*v*sin(th)*cos(sc_pitch)) + tan(sc_pitch)/tan(th)))
c               if(i.eq. 1) then
c                  twin = 0.3d0 * i_skip
c               else
c                  twin = 0.03d0 * i_skip
c               end if
               twin = 0.3d0 * i_skip
               ts = zbrent(fc,ts-twin,ts+twin,tol) ! solve for position of s/c
cccccccccccccc	       
            end if
               
            if (i .eq. 1) tslon = ts
               
            call orrmread1(16,ts,pos,vel,0)
               
c     position found; compute range and azimuth locations
               
            call lincomb(-1.d0,pos,1.d0,tpv,sc_losv)
            call norm(sc_losv,rr)
c	    write (*,*) "dop found:", 1.d3 * ( vel(1) * sc_losv(1) / rr
c     &	                                      +vel(2) * sc_losv(2) / rr  
c     &	                                      +vel(3) * sc_losv(3) / rr ) * 2 / ( wvl * 1000. )
     
               
            call matvec(xyz2enu,sc_losv,sc_losvenu)
            
            call unitvec(sc_losvenu,usc_losvenu)
            
            grad(1) = -real(slparr(i+nslonpx-1))
            grad(2) = -aimag(slparr(i+nslonpx-1))
            grad(3) = 1.d0
            
            call unitvec(grad,ugrad)
            
            cosinc = dot(ugrad,usc_losvenu)
            cosinc = abs(cosinc)
            call getangs(pos,vel,sc_losv,r_az,r_sc_look)
            
            call norm(sc_losv,range)
            call norm(vel,sc_vel)
            call norm(pos,sc_r)
            call getTCN_TCvec(pos,vel,vel,tc_vec)
            call norm(tc_vec, tc_vel)
c            azm(1) = sngl(1.d3*(ts-t0) * tc_vel * trk_rad / sc_r)
            elp%r_a = r_a
            elp%r_e2 = r_e2
            call latlon(elp,pos,r_llh,i_xyztollh)
            h  = r_llh(3)
c            azm(1) = sngl(1.d3*(ts-t0) * tc_vel * (sc_r - h ) / sc_r)
            azm(1) = sngl(1.d3*(ts-t0) * tc_vel * ( trk_rad ) / ( trk_rad + h ))

            r(1) = sngl(1.d3*(range-r0))

            ir = nint(r(1)/rsamp) +1
            ia = nint(azm(1)/asamp) +1
c Place r et azm in array to memorize position of preceding DEM line ---> for oversampling
            rsave(i)=r(1)
            azmsave(i)=azm(1)
            csave(i)=cosinc

            if (((((j .eq. 1) .or. (j .eq. (1+(int((nlatpx-1)/i_skip))*i_skip)))) .or.
     &           (mod((mod_counter-1),20) .eq. 0)) .and.
     &          (((i .eq. 1) .or. (i .eq. (1+(int((nlonpx-1)/i_skip))*i_skip))))) then
               write (*,1012) 'DEM pixel (col, row)', nslonpx+i-1, nslatpx+j-1
               write (*,1002) 'Latitude and longitude (deg)', lat/dtor, lon/dtor
               write (*,1012) 'Simulation pixel (col, row)', ir , ia
               write (*,*)
            endif


c map to predetermined radar coordinates
c
cMP : cas quadratique : l inversion de la transformation affine n est plus possible
c l inversion se fait donc avant l appel de Intsim par inv_aff..._quad.pl
c ici on applique donc directement la transfo donnee en input
c de plus on garde les valeurs reelles r(1)/rsamp et azm(1)/asamp
c            r_gr = float(ir) - r_tv(1)
c            r_ga = float(ia) - r_tv(2) 
            r_gr =r(1)/rsamp+1
            r_ga =azm(1)/asamp +1
            
            a_gr = r_mat(1,1)*r_gr + r_mat(1,2)*r_ga + r_tv(1) +
     $          r_quad(1,1)*r_gr**2 + r_quad(1,2)*r_ga**2 +r_quad(1,3)*r_ga*r_gr
            a_ga = r_mat(2,1)*r_gr + r_mat(2,2)*r_ga+ r_tv(2) +
     $          r_quad(2,1)*r_gr**2 + r_quad(2,2)*r_ga**2 +r_quad(2,3)*r_ga*r_gr

            i_gr = nint(a_gr)
            i_ga = nint(a_ga)

c place lat and lon pixels in array 
            
            if (i_dolatlon .eq. 1) then
               latlonmap(i+nslonpx-1) = 0.
               latlonmap(i+ntotlonpx+nslonpx-1) = 0.
               if ((i_gr .gt. 0) .and. (i_gr .le. i_simsamp)
     $              .and. (i_ga .gt. 0) .and. (i_ga .le. i_last)
     $              .and. .not. bad_elev) then ! skip 'no data' points
                  latlonmap(i+nslonpx-1) = a_gr
                  latlonmap(i+ntotlonpx+nslonpx-1) = a_ga
               endif
            endif

c            write (*,*) usc_losvenu
              
 
c     check if this point is a current GPS point
               
            if ((i_gpsflag .eq. 1) .and. (i_dogps .eq. 1)) then
               do i_g = 1 , i_gpscnt

                  i_dumdum = 0

                  if ((dem_corner_flag .eq. 1) .or. (dem_corner_flag .eq. 2)
     $                 .or. (dem_corner_flag .eq. 4)) then
                     if ((abs(r_gpslat(i_g)-lat) .lt. abs(dellat)/2.d0) .and.
     &                   (abs(r_gpslon(i_g)-lon) .lt. abs(dellon)/2.d0)) i_dumdum = 1
                  elseif (dem_corner_flag .eq. 3) then
                     r_h = 0.d0
                     elp%r_a = r_a1866
                     elp%r_e2 = r_e21866
                     r_llh(1) = r_gpslat(i_g)
                     r_llh(2) = r_gpslon(i_g)
                     r_llh(3) = r_h
                     call latlon(elp,tpv_dum,r_llh,i_llhtoxyz)  ! 1866 is correct here
                     call convert_sch_to_xyz(ptm,sch_dum,tpv_dum,i_xyztosch)
                     r_gpss = sch_dum(1)
                     r_gpsc = sch_dum(2)
                     if ((abs(r_gpss-dem_sch(1)) .lt. abs(dels)/2.d0) .and.
     &                   (abs(r_gpsc-dem_sch(2)) .lt. abs(delc)/2.d0)) i_dumdum = 1
                  endif

                  if (i_dumdum .eq. 1) then
                     
                     r_gpsvec(1) = r_edis(i_g)
                     r_gpsvec(2) = r_ndis(i_g)
                     r_gpsvec(3) = r_udis(i_g)
                     r_losdisp(i_g) = dot(r_gpsvec,usc_losvenu)
                     
                     write (*,*) 'GPS point = ',i_g
                     write (*,*) 'Position orig = ', r_gpslatorg(i_g)/dtor , r_gpslonorg(i_g)/dtor
                     write (*,*) 'Position new  = ' , r_gpslat(i_g)/dtor , r_gpslon(i_g)/dtor
                    
 
c     update the simulation file
                     
c                     r_gr = float(ir) - r_tv(1)
c                     r_ga = float(ia) - r_tv(2) 
            r_gr =r(1)/rsamp+1
            r_ga =azm(1)/asamp +1
                     
                     a_gr = r_mat(1,1)*r_gr + r_mat(1,2)*r_ga + r_tv(1) +
     $                   r_quad(1,1)*r_gr**2 + r_quad(1,2)*r_ga**2 +r_quad(1,3)*r_ga*r_gr
                     a_ga = r_mat(2,1)*r_gr + r_mat(2,2)*r_ga+ r_tv(2) +
     $                   r_quad(2,1)*r_gr**2 + r_quad(2,2)*r_ga**2 +r_quad(2,3)*r_ga*r_gr

                     i_gr = nint(a_gr)
                     i_ga = nint(a_ga)

                     write(6,126) i_g , i_gr , i_ga , r_gpslatorg(i_g)/dtor ,
     &                            r_gpslonorg(i_g)/dtor , usc_losvenu ,
     &                            r_losdisp(i_g)/(r_time*10.)
                     
                     
                     if ((i_ga .gt. 5) .and. (i_ga .lt. i_last-6) .and. (i_gr .gt. 5) .and.
     &                   (i_gr .lt. i_simsamp-6)) then
                        
                        do kk = i_ga - 5 , i_ga + 6
                           
                           read (21,rec=kk) (r_sd(l) , l = 1 , 2*i_simsamp)
                           do ll = i_gr-5 , i_gr+6
                              if (i_diamond(ll-i_gr,kk-i_ga) .eq. 1) then
                                 r_sd(ll) = 100.d0
                              else
                                 r_sd(ll) = 0.d0
                              endif
                           enddo
                           write (22,rec=kk) (r_sd(l) , l = 1 , 2*i_simsamp)

                        enddo 

                        write(26,126) i_g , i_gr , i_ga , r_gpslatorg(i_g)/dtor ,
     &                                r_gpslonorg(i_g)/dtor , usc_losvenu ,
     &                                r_losdisp(i_g)/(r_time*10.)

                     endif

                  endif

               enddo

            endif

            if (i_dosim .eq. 1) then
               if ((ir .gt. 0) .and. (ir .le. nrmax) .and. (ia .gt. 0) .and. (ia .le. namax)) then
                  if (cosinc .eq. 1.0) cosinc = 0.999
                  if (cosinc .lt. 0.01) cosinc = 0.01
                  if ( .not. bad_elev ) then 

c     skip 'no data' points
c     check for layover--duplication of points in height map
c     doesn't work well

c     $              if ( hgtmap(ir,ia) .ne. 0.0 ) then ! must be layover
c     $                 hgtmap(ir,ia) = -1.0
c     $                 hgtmap(ir+nrmax,ia) = -10000.
c     $              else

c     pas de valeur preexistante : on met nouvelle valeur
c     valeur preexistante + grande que nouvelle valeur :  on met nouvelle valeur

                     hgttmp=(float(hgtarr(i+nslonpx-1)) - alt_bias) * alt_scale
                     if(abs(hgtmap(ir,ia)).lt.1.e-6)layover(ir,ia)=hgttmp
                     if(abs(hgtmap(ir,ia)).gt.1.e-6.and.hgttmp.gt.layover(ir,ia))layover(ir,ia)=hgttmp
                     if(abs(hgtmap(ir,ia)).lt.1.e-6.or.hgttmp.lt.hgtmap(ir+nrmax,ia))then
                        hgtmap(ir,ia) = cosinc**2 / sqrt(1.d0-cosinc**2)
                        hgtmap(ir+nrmax,ia) = hgttmp
                     endif

c     ATTENTION Marche seulement en pleine resolution: commenter si 4 ou 2 rlks !!!!!!!!!!! Sauf si MNT a 90 m ??
c     Peu de layover en 4rlks avec MNT a 30m ---> Pas necessaire, mais garde vallees pour desc, et sommets pour montantes
c     check for layover--duplication of points in height map : ecrase avec les points susceptibles d etre les + bas en NR
c     si descendante NR a l E: ds zones de lay-over, on commence par les sommets, on garde les vallees
c     si montante NR a l'W : ds zones de lay-over, on commence par les vallees, mais apres on ne garde pas les sommets 
c     a tester, en particulier changer condition + bas.
c     par contre, sur les flancs pentus "normaux", topo sur estimee ou sous estimee de qq m selon l orientation / satellite

                     if(i.gt.0.and.i.lt.nlonpx.and.j.gt.1.and.j.lt.nlatpx)then
c     point milieu entre i et i-1 : selon E-W
                        ir = nint((rsave(i)+rsave(i-1))/(2.*rsamp)) +1
                        ia = nint((azmsave(i)+azmsave(i-1))/(2.*asamp)) +1
                        if ((ir .gt. 0) .and. (ir .le. nrmax) .and. (ia .gt. 0) .and. (ia .le. namax)) then
                           if ( (hgtarr(i+nslonpx-2) .gt. 25000) .or.
     &                        (hgtarr(i+nslonpx-2) .lt. -1000 )) bad_elev = .TRUE.
                           if ( .not. bad_elev ) then
c     altitude :point  milieu

                              hgttmp=(float(hgtarr(i+nslonpx-2)) - alt_bias) * alt_scale
                              hgttmp=(hgttmp+(float(hgtarr(i+nslonpx-1)) - alt_bias) * alt_scale)/2.

c     on ecrase l'ancienne valeur, si elle existe deja et si la nouvelle est plus petite
c     en ascendant la condition devrait changer : on ote la condition "si existe deja ", pour remplir au maximum
c     avec les points des vallees avant d'arriver sur les montagnes. a tester ??

                     if(abs(hgtmap(ir,ia)).lt.1.e-6)layover(ir,ia)=hgttmp
                     if(abs(hgtmap(ir,ia)).gt.1.e-6.and.hgttmp.gt.layover(ir,ia))layover(ir,ia)=hgttmp
                              if(abs(hgtmap(ir,ia)).lt.1.e-6.or.hgttmp.lt.hgtmap(ir+nrmax,ia))then
                                 cosinc=(csave(i)+csave(i-1))/2.
                                 if (cosinc .eq. 1.0) cosinc = 0.999
                                 if (cosinc .lt. 0.01) cosinc = 0.01
                                 hgtmap(ir,ia) = cosinc**2 / sqrt(1.d0-cosinc**2)
                                 hgtmap(ir+nrmax,ia) = hgttmp
                              endif
                           endif
                        endif

c     point milieu entre i et i-1 : selon E-W (1/4 i 3/4 i-1)
                        ir = nint((3*rsave(i)+rsave(i-1))/(4.*rsamp)) +1
                        ia = nint((3*azmsave(i)+azmsave(i-1))/(4.*asamp)) +1
                        if ((ir .gt. 0) .and. (ir .le. nrmax) .and. (ia .gt. 0) .and. (ia .le. namax)) then
                           if ( .not. bad_elev ) then
c     altitude :point  milieu

                              hgttmp=(float(hgtarr(i+nslonpx-2)) - alt_bias) * alt_scale
                              hgttmp=(hgttmp+3*(float(hgtarr(i+nslonpx-1)) - alt_bias) * alt_scale)/4.

c     on ecrase l'ancienne valeur, si elle existe deja et si la nouvelle est plus petite
c     en ascendant la condition devrait changer : on ote la condition "si existe deja ", pour remplir au maximum
c     avec les points des vallees avant d'arriver sur les montagnes. a tester ??

                     if(abs(hgtmap(ir,ia)).lt.1.e-6)layover(ir,ia)=hgttmp
                     if(abs(hgtmap(ir,ia)).gt.1.e-6.and.hgttmp.gt.layover(ir,ia))layover(ir,ia)=hgttmp
                              if(abs(hgtmap(ir,ia)).lt.1.e-6.or.hgttmp.lt.hgtmap(ir+nrmax,ia))then
                                 cosinc=(3*csave(i)+csave(i-1))/4.
                                 if (cosinc .eq. 1.0) cosinc = 0.999
                                 if (cosinc .lt. 0.01) cosinc = 0.01
                                 hgtmap(ir,ia) = cosinc**2 / sqrt(1.d0-cosinc**2)
                                 hgtmap(ir+nrmax,ia) = hgttmp
                              endif
                           endif
                        endif

c     point milieu entre i et i-1 : selon E-W (1/4 i-1 3/4 i)
                        ir = nint((rsave(i)+3*rsave(i-1))/(4.*rsamp)) +1
                        ia = nint((azmsave(i)+3*azmsave(i-1))/(4.*asamp)) +1
                        if ((ir .gt. 0) .and. (ir .le. nrmax) .and. (ia .gt. 0) .and. (ia .le. namax)) then
                           if ( .not. bad_elev ) then
c     altitude :point  milieu

                              hgttmp=(float(hgtarr(i+nslonpx-2)) - alt_bias) * alt_scale
                              hgttmp=(3*hgttmp+(float(hgtarr(i+nslonpx-1)) - alt_bias) * alt_scale)/4.

c     on ecrase l'ancienne valeur, si elle existe deja et si la nouvelle est plus petite
c     en ascendant la condition devrait changer : on ote la condition "si existe deja ", pour remplir au maximum
c     avec les points des vallees avant d'arriver sur les montagnes. a tester ??

                     if(abs(hgtmap(ir,ia)).lt.1.e-6)layover(ir,ia)=hgttmp
                     if(abs(hgtmap(ir,ia)).gt.1.e-6.and.hgttmp.gt.layover(ir,ia))layover(ir,ia)=hgttmp
                              if(abs(hgtmap(ir,ia)).lt.1.e-6.or.hgttmp.lt.hgtmap(ir+nrmax,ia))then
                                 cosinc=(csave(i)+3*csave(i-1))/4.
                                 if (cosinc .eq. 1.0) cosinc = 0.999
                                 if (cosinc .lt. 0.01) cosinc = 0.01
                                 hgtmap(ir,ia) = cosinc**2 / sqrt(1.d0-cosinc**2)
                                 hgtmap(ir+nrmax,ia) = hgttmp
                              endif
                           endif
                        endif
c
c     point milieu en NS, entre j et j-1
                        ir = nint((rsavep(i)+rsave(i))/(2.*rsamp)) +1
                        ia = nint((azmsavep(i)+azmsave(i))/(2.*asamp)) +1
                        if ((ir .gt. 0) .and. (ir .le. nrmax) .and. (ia .gt. 0) .and. (ia .le. namax)) then
                           if ( (hgtarrp(i+nslonpx-1) .gt. 25000) .or.
     &                        (hgtarrp(i+nslonpx-1) .lt. -1000 )) bad_elev = .TRUE.
                           if ( .not. bad_elev ) then

                              hgttmp=(float(hgtarr(i+nslonpx-1)) - alt_bias) * alt_scale
                              hgttmp=(hgttmp+(float(hgtarrp(i+nslonpx-1)) - alt_bias) * alt_scale)/2.

                     if(abs(hgtmap(ir,ia)).lt.1.e-6)layover(ir,ia)=hgttmp
                     if(abs(hgtmap(ir,ia)).gt.1.e-6.and.hgttmp.gt.layover(ir,ia))layover(ir,ia)=hgttmp
                              if(abs(hgtmap(ir,ia)).lt.1.e-6.or.hgttmp.lt.hgtmap(ir+nrmax,ia))then
                                 cosinc=(csave(i)+csavep(i))/2.
                                 if (cosinc .eq. 1.0) cosinc = 0.999
                                 if (cosinc .lt. 0.01) cosinc = 0.01
                                 hgtmap(ir,ia) = cosinc**2 / sqrt(1.d0-cosinc**2)
                                 hgtmap(ir+nrmax,ia) = hgttmp
                              endif
                           endif
                        endif
c
c     Point milieu, a la fois en NS et en EW: i et i-i, j et j-1
                        ir = nint((rsave(i)+rsave(i-1)+rsavep(i)+rsavep(i-1))/(4.*rsamp)) +1
                        ia = nint((azmsave(i)+azmsave(i-1)+azmsavep(i)+azmsavep(i-1))/(4.*asamp)) +1
                        if ((ir .gt. 0) .and. (ir .le. nrmax) .and. (ia .gt. 0) .and. (ia .le. namax)) then
                           if ( .not. bad_elev ) then
c     moyenne de 4 altitudes

                              hgttmp=(float(hgtarr(i+nslonpx-2)) - alt_bias) * alt_scale
                              hgttmp=hgttmp+(float(hgtarr(i+nslonpx-1)) - alt_bias) * alt_scale
                              hgttmp=hgttmp+(float(hgtarrp(i+nslonpx-1)) - alt_bias) * alt_scale
                              hgttmp=(hgttmp+(float(hgtarrp(i+nslonpx-2)) - alt_bias) * alt_scale)/4.

c     on ecrase l'ancienne valeur, si elle existe deja et si la nouvelle est plus petite
c     en ascendant la condition devrait changer : on ote la condition "si existe deja ", pour remplir au maximum
c     avec les points des vallees avant d'arriver sur les montagnes. a tester ??
                     if(abs(hgtmap(ir,ia)).lt.1.e-6)layover(ir,ia)=hgttmp
                     if(abs(hgtmap(ir,ia)).gt.1.e-6.and.hgttmp.gt.layover(ir,ia))layover(ir,ia)=hgttmp
                              if(abs(hgtmap(ir,ia)).lt.1.e-6.or.hgttmp.lt.hgtmap(ir+nrmax,ia))then
                                 cosinc=(csave(i)+csavep(i)+csave(i-1)+csavep(i-1))/4.
                                 if (cosinc .eq. 1.0) cosinc = 0.999
                                 if (cosinc .lt. 0.01) cosinc = 0.01
                                 hgtmap(ir,ia) = cosinc**2 / sqrt(1.d0-cosinc**2)
                                 hgtmap(ir+nrmax,ia) = hgttmp
                              endif
                           endif
                        endif

                     endif
                  endif
               endif
            endif

         enddo                  ! i= DEM pixels

         if (i_dolatlon .eq. 1) then
            write (20,rec=j+nslatpx-1) (latlonmap(ll) , ll = 1 , 2*ntotlonpx)
         endif

      enddo                     ! j= DEM lines

! nettoyage dans zone de lay over des points aberrants.
! on ote valeurs isolee/voisins trop elevee dans zone de layover
      do j = 2 , namax-1
         do i=nrmax+2, 2*nrmax-1
            if(hgtmap(i,j) .ne. 0.0 ) then
              summean=0.
              isummean=0
              if(hgtmap(i+1,j) .ne. 0.0 ) then
              summean=summean+hgtmap(i+1,j)
              isummean=isummean+1
              endif
              if(hgtmap(i-1,j) .ne. 0.0 ) then
              summean=summean+hgtmap(i-1,j)
              isummean=isummean+1
              endif
              if(hgtmap(i,j+1) .ne. 0.0 ) then
              summean=summean+hgtmap(i,j+1)
              isummean=isummean+1
              endif
              if(hgtmap(i,j-1) .ne. 0.0 ) then
              summean=summean+hgtmap(i,j-1)
              isummean=isummean+1
              endif
              if(hgtmap(i+1,j+1) .ne. 0.0 ) then
              summean=summean+hgtmap(i+1,j+1)
              isummean=isummean+1
              endif
              if(hgtmap(i+1,j-1) .ne. 0.0 ) then
              summean=summean+hgtmap(i+1,j-1)
              isummean=isummean+1
              endif
              if(hgtmap(i-1,j+1) .ne. 0.0 ) then
              summean=summean+hgtmap(i-1,j+1)
              isummean=isummean+1
              endif
              if(hgtmap(i-1,j-1) .ne. 0.0 ) then
              summean=summean+hgtmap(i-1,j-1)
              isummean=isummean+1
              endif
              if(isummean.gt.2)then
              summean=summean/isummean
              if(hgtmap(i,j) .gt. (summean+100.))then
                 hgtmap(i,j)=0.
                 hgtmap(i-nrmax,j)=0.
              endif
              endif
            endif
         enddo
      enddo

      if (i_dosim .eq. 1) then
         do j = 1 , namax
            write (19,rec=j) (hgtmap(i,j) , i = 1 , 2*nrmax)
            do i=1,nrmax
              if((layover(i,j)-hgtmap(i+nrmax,j)).gt.80.)then
                layover(i,j)=1.01
              elseif((layover(i,j)-hgtmap(i+nrmax,j)).gt.5.)then
                val=layover(i,j)-hgtmap(i+nrmax,j)
                layover(i,j)=(val-5.)/75.+0.01
              else
                layover(i,j)=0.01
              endif
            enddo
         jrec=2*j-1
         write (23,rec=jrec) (hgtmap(i,j) , i = 1 , nrmax)
         jrec=2*j
         write (23,rec=jrec) (layover(i,j) , i = 1 , nrmax)
         enddo
      endif


ccccc close files

      close(31)
      close(32)
      close(16)
      close(19)
      close(20)
      close(21)
      close(22)
      close(23)
      close(26)


ccccc formats

 126  format (1x,i5,1x,i5,1x,i5,1x,f15.8,1x,f15.8,1x,3(f12.8,1x),f15.8)
 1001 format (1x,a,t44,' = ',f12.4)
 1002 format (1x,a,t44,' = ',2(f12.4,1x))
 1003 format (1x,a,t44,' = ',3(f12.4,1x))
 1004 format (1x,a,t44,' = ',2(f12.4,1x),i7)
 1011 format (1x,a,t44,' = ',i7)
 1012 format (1x,a,t44,' = ',2(i7,6x))



      end

CPOD      
CPOD=pod
CPOD
CPOD=head1 USAGE
CPOD
CPOD usage: IntSim cmd_file 
CPOD
CPOD where cmd_file is the name of the command file (= RDF ascii formatted file).
CPOD
CPOD=head1 FUNCTION
CPOD
CPOD   FUNCTIONAL DESCRIPTION: Maps DEM (llh,NEH,SCH) to
CPOD   range, azimuth and height.
CPOD     
CPOD   NOTES: -
CPOD     
CPOD   UPDATE LOG: Additions made by Paul Rosen, Scott Hensley and
CPOD   Sean Buckley.
CPOD     
CPOD   Paul Rosen modified to use RDF input, refine location alg.,f etc. Summer 99
CPOD   changed file name string lengths to allow long file names EJF 99/3/4
CPOD   modified printing of DEM corners (added int() to match format) EJF 99/3/22
CPOD    
CPOD   modified initial orbit search window to work with double scenes 
CPOD   (especially ascending) EJF 99/4/30
CPOD    
CPOD   added check if pixel value is valid 
CPOD    --assume negative and zero values before scale and offset are 'no data' EJF 99/7/19
CPOD   
CPOD   added option (dem_corner_flag = 4) for DEM in UTM with WGS84 datum/ellipsoid EJF 99/7/21-22
CPOD
CPOD   merged PAR and EJF modifications EJF 2000/1/7
CPOD
CPOD   added RDF input file read for DEM Datum EJF 2000/8/2
CPOD   modified calculation of SAR corners in DEM and size of initial search window EJF 2000/8/4
CPOD
CPOD=head1 ROUTINES CALLED
CPOD
CPOD   convert_sch_to_xyz
CPOD   (rdir, reast, rnorth), dot, enubasis, getangs, getarg, getTCN_TCvec,
CPOD   latlon, lincomb, matvec, norm, orrmread1, radar_to_xyz,
CPOD   rdf_init, rdf_read
CPOD   tranmat, unitvec, utmtoll, zbrent
CPOD     
CPOD
CPOD=head1 CALLED BY
CPOD
CPOD
CPOD=head1 FILES USED
CPOD
CPOD Program reads in an RDF command file; template listing:
CPOD
CPOD Digital Elevation Model Filename - i*2 DEM 
CPOD Slope Filename - floating point complex slope file from Gradient.f
CPOD ORRM formatted filename - orbit file in ORRM format
CPOD Height in simulated range,doppler coordinates - output simulation file
CPOD Coordinates of simulated range,doppler in map format  - output file containing the r/d coordinate for each DEM grid point
CPOD GPS vector inputs filename - input file with a list of GPS points to map into r/d coordinates
CPOD GPS vector mapped to radar LOS output filename - output file as stated
CPOD Rectified height in simulated coordinates - output file that
CPOD      is a warped version of original simulated file to match DEM better
CPOD Rectified height in simulated coordinates with GPS - Same as above, with GPS points drawn in
CPOD Dimensions of rectified height file - across, down size of output file in pixels
CPOD DEM projection - UTM/LL/SCH
CPOD DEM DATUM - WGS84/NAD27
CPOD DEM corner easting - for UTM DEM
CPOD DEM easting spacing - for UTM DEM
CPOD DEM total easting pixels - for UTM DEM
CPOD DEM UTM zone - for UTM DEM
CPOD DEM corner northing - for UTM DEM
CPOD DEM northing spacing - for UTM DEM
CPOD DEM total northing pixels - for UTM DEM
CPOD DEM corner latitude - for LL DEM
CPOD DEM latitude spacing - for LL DEM
CPOD DEM total latitude pixels - for LL DEM
CPOD DEM corner longitude - for LL DEM
CPOD DEM longitude spacing - for LL DEM    
CPOD DEM total longitude pixels - for LL DEM
CPOD DEM corner s - for SCH DEM             
CPOD DEM s spacing - for SCH DEM             
CPOD DEM total s pixels - for SCH DEM             
CPOD DEM corner c - for SCH DEM             
CPOD DEM c spacing - for SCH DEM             
CPOD DEM total c pixels - for SCH DEM             
CPOD SCH Peg lat - for SCH DEM             
CPOD SCH Peg lon - for SCH DEM             
CPOD SCH Heading - for SCH DEM             
CPOD DEM height bias - if the DEM is not zero based
CPOD DEM height scale factor - if DEM is not in meters
CPOD DEM Northing bias - UTM adjustment   
CPOD DEM Easting bias - UTM adjustment
CPOD Range output reference - output map start range of first range sample
CPOD Time  output reference - output map start time for first line
CPOD Range output spacing - as stated
CPOD Azimuth output spacing - as stated
CPOD Number of range pixels - as stated
CPOD Number of azimuth pixels - as stated
CPOD Nominal squint angle from heading - in degrees
CPOD GPS scale factor - scale to apply to GPS displacement vector to correspond to interferogram time span
CPOD Datum conversion bias vector - as stated
CPOD Affine matrix row 1 - affine transformation fine adjusts simulation to match data
CPOD Affine matrix row 2 - affine transformation fine adjusts simulation to match data
CPOD Affine offset vector - affine transformation fine adjusts simulation to match data
CPOD Do simulation? - yes/no; can just do mapping or GPS mapping if desired
CPOD Do mapping? - yes/no; can just do simulation or GPS mapping if desired
CPOD Do GPS mapping? - yes/no; can just do simulation or mapping if desired
CPOD Output skip factor - can speed up computation by skipping samples for diagnostics
CPOD Search method - ANGLE/DOPPLER
CPOD Range reference for Doppler  - the start range when Doppler polynomial was computed     
CPOD Range spacing for Doppler - the range spacing when the Doppler polynomial was computed
CPOD Doppler coefficients - cubic doppler polynomials as a function of range pixels
CPOD Radar Wavelength - as stated
CPOD Radar PRF - as stated      
CPOD Desired center latitude - can set the center latitude of the output map if desired
CPOD Desired center longitude - can set the center longitude of the output map if desired
CPOD
CPOD LISTING: example RDF command file
CPOD =============================================================================================
CPOD Digital Elevation Model Filename                      (-) = /u/riesen2/par/NorthRidge/DEM/mosA.dte
CPOD Slope Filename                                        (-) = /u/riesen2/par/NorthRidge/DEM/mosA.slp
CPOD ORRM formatted filename                               (-) = ../93.orrm
CPOD Height in simulated range,doppler coordinates         (-) = north.sim
CPOD Coordinates of simulated range,doppler in map format  (-) = north.sim.mapping
CPOD GPS vector inputs filename                            (-) = ../north.gps.in
CPOD GPS vector mapped to radar LOS output filename        (-) = north.gps.out
CPOD Rectified height in simulated coordinates             (-) = north.sim.rect
CPOD Rectified height in simulated coordinates with GPS    (-) = north.sim.rect.gps
CPOD Dimensions of rectified height file                 (-,-) = 6000 6800  
CPOD DEM projection                                        (-) = UTM
CPOD 
CPOD DEM corner easting                                    (m) = 3874620.00
CPOD DEM easting spacing                                   (m) = -20.
CPOD DEM total easting pixels                              (-) =  11177
CPOD DEM UTM zone                                          (-) = 11
CPOD 
CPOD DEM corner northing                                   (m) =  306620.00 
CPOD DEM northing spacing                                  (m) =   20.
CPOD DEM total northing pixels                             (-) =  14326
CPOD 
CPOD DEM corner latitude                                 (deg) = 0.
CPOD DEM latitude spacing                                (deg) = 0.
CPOD DEM total latitude pixels                             (-) = 0
CPOD 
CPOD DEM corner longitude                                (deg) = 0.
CPOD DEM longitude spacing                               (deg) = 0.
CPOD DEM total longitude pixels                            (-) = 0
CPOD 
CPOD DEM corner s                                          (m) = 0.
CPOD DEM s spacing                                         (m) = 0.
CPOD DEM total s pixels                                    (-) = 0.
CPOD 
CPOD DEM corner c                                          (m) = 0.
CPOD DEM c spacing                                         (m) = 0.
CPOD DEM total c pixels                                    (-) = 0.
CPOD 
CPOD SCH Peg lat                                           (m) = 0.
CPOD SCH Peg lon                                           (m) = 0.
CPOD SCH Heading                                         (deg) = 0.
CPOD 
CPOD DEM height bias                                       (-) = 800.
CPOD DEM height scale factor                               (-) = 0.125
CPOD DEM Northing bias                                     (-) = 0.
CPOD DEM Easting bias                                      (-) = 0.
CPOD Range output reference                               (km) =   685.0
CPOD Time  output reference                              (sec) = 23210.0
CPOD Range output spacing                                  (m) =     8.778 
CPOD Azimuth output spacing                                (m) =    13.51
CPOD Number of range pixels                                (-) = 6560
CPOD Number of azimuth pixels                              (-) = 7200
CPOD Nominal squint angle from heading                   (deg) =    92.96
CPOD GPS scale factor                                      (-) = 1.
CPOD Datum conversion bias vector                          (m) = 0. 0. 0. 
CPOD Affine matrix row 1                                   (-) = 0.9998237528    0.0001005364 
CPOD Affine matrix row 2                                   (-) = -0.0009237903    1.0003657474
CPOD Affine offset vector                                  (-) =   411.526   474.574
CPOD Do simulation?                                        (-) = yes
CPOD Do mapping?                                           (-) = yes
CPOD Do GPS mapping?                                       (-) = yes
CPOD Output skip factor                                    (-) = 10
CPOD Search method                                         (-) = Doppler
CPOD Range reference for Doppler                          (km) = 697.043 
CPOD Range spacing for Doppler                             (m) = 8.778 
CPOD Doppler coefficients                              (-,-,-) = -1.16 -5.1e-5 0. 
CPOD Radar Wavelength                                     (cm) = 23.51
CPOD Radar PRF                                             (-) = 1555.2
CPOD Desired center latitude                             (deg) = 34.3
CPOD Desired center longitude                            (deg) = -118.33
CPOD
CPOD=head1 FILES CREATED
CPOD
CPOD see list above
CPOD Height in simulated range,doppler coordinates - output simulation file
CPOD Coordinates of simulated range,doppler in map format  - output mapping file 
CPOD GPS vector mapped to radar LOS output filename - output file as stated
CPOD Rectified height in simulated coordinates - output file 
CPOD
CPOD=head1 DIAGNOSTIC FILES
CPOD
CPOD
CPOD=head1 HISTORY
CPOD
CPOD Original Routines: P.A.Rosen, S Hensley
CPOD
CPOD=head1 LAST UPDATE
CPOD Date Changed        Reason Changed 
CPOD ------------       ----------------
CPOD
CPOD trm Jan 8th '04
CPOD par Jan 20 '04
CPOD=cut
