c  
      parameter(nxm=12000,nym=50000)
      real amp(nxm),phs(nxm)
      complex cpx(nxm),a
      character*512 nom_input,nom_output,buff
c
      call getarg(1,nom_input)
      call getarg(2,nom_output)
      call getarg(3,buff)
      read(buff,*)iwidth
      call getarg(4,buff)
      read(buff,*)ascale
c
      open(1,file=nom_input,status='old',form='unformatted',
     $   access='direct',recl=8*iwidth)
      open(2,file=nom_output,status='unknown',form='unformatted',
     $   access='direct',recl=4*iwidth)
      do j=1,nym
        read(1,rec=j,err=10)(cpx(i),i=1,iwidth)
        do i=1,iwidth
           amp(i)=sqrt(aimag(cpx(i))**2+real(cpx(i))**2)
           phs(i)=atan2(aimag(cpx(i)),real(cpx(i)))*ascale
        enddo
        irec=2*j-1
        write(2,rec=irec)(amp(i),i=1,iwidth)
        irec=2*j
        write(2,rec=irec)(phs(i),i=1,iwidth)
       enddo
10     close(1)
       close(2)
c
       stop
       end
