      a_rdtmp = rdfval('Master input file','-')
      read(unit=a_rdtmp,fmt='(a)') name_in_1
      a_rdtmp = rdfval('Second input file','-')
      read(unit=a_rdtmp,fmt='(a)') name_in_2
      a_rdtmp = rdfval('Master SLC','-')
      read(unit=a_rdtmp,fmt='(a)') name_out
      a_rdtmp = rdfval('Second SLC','-')
      read(unit=a_rdtmp,fmt='(a)') name_outp
      a_rdtmp = rdfval('Eight look image file','-')
      read(unit=a_rdtmp,fmt='(a)') name_8lk
      a_rdtmp = rdfval('Debug flag','-')
      read(unit=a_rdtmp,fmt=*) iflag
      a_rdtmp = rdfval('Bytes per input line','-')
      read(unit=a_rdtmp,fmt=*) nbytes1, nbytes2
      a_rdtmp = rdfval('Good bytes per input line','-')
      read(unit=a_rdtmp,fmt=*) ngood1, ngood2
      a_rdtmp = rdfval('First line to read','-')
      read(unit=a_rdtmp,fmt=*) ifirstline
      a_rdtmp = rdfval('Number of patches','-')
      read(unit=a_rdtmp,fmt=*) npatches
      a_rdtmp = rdfval('First sample pair to use','-')
      read(unit=a_rdtmp,fmt=*) ifirst
      a_rdtmp = rdfval('Azimuth Patch Size','-')
      read(unit=a_rdtmp,fmt=*) nnn
      a_rdtmp = rdfval('Number of valid pulses','-')
      read(unit=a_rdtmp,fmt=*) na_valid
      a_rdtmp = rdfval('Deskew?','-')
      read(unit=a_rdtmp,fmt='(a)') deskew
      a_rdtmp = rdfval('Caltone location','-')
      read(unit=a_rdtmp,fmt=*) caltone1, caltone2
      a_rdtmp = rdfval('Start range bin, number to process','-')
      read(unit=a_rdtmp,fmt=*) isave, nlinesaz
      a_rdtmp = rdfval('Delta azimuth, range pixels for second file','-')
      read(unit=a_rdtmp,fmt=*) iazdelta,iradelta
      a_rdtmp = rdfval('Master file Doppler centroid coefs','-')
      read(unit=a_rdtmp,fmt=*) fd1, fdd1, fddd1, fdddd1
      a_rdtmp = rdfval('Second file Doppler centroid coefs','-')
      read(unit=a_rdtmp,fmt=*) fd2, fdd2, fddd2, fdddd2
      a_rdtmp = rdfval('Doppler average method','-')
      read(unit=a_rdtmp,fmt=*) iusedopp
      a_rdtmp = rdfval('Earth radius','m')
      read(unit=a_rdtmp,fmt=*) re
      a_rdtmp = rdfval('Body fixed S/C velocities','m/s')
      read(unit=a_rdtmp,fmt=*) vel1, vel2
      a_rdtmp = rdfval('Spacecraft height','m')
      read(unit=a_rdtmp,fmt=*) ht1, ht2
      a_rdtmp = rdfval('Planet GM','-')
      read(unit=a_rdtmp,fmt=*) gm
      if(gm .eq. 0.d0) then
         gm = earthgm
      end if
      a_rdtmp = rdfval('Left, Right or Unknown Pointing','-')
      read(unit=a_rdtmp,fmt='(a)') leftright
      i_lrl = 0
      if(leftright .eq. 'Left' .or. leftright .eq. 'LEFT' .or.
     $     leftright .eq. 'left') then
         i_lrl = 1
      elseif(leftright .eq. 'Right' .or. leftright .eq. 'RIGHT' .or.
     $        leftright .eq. 'right') then
         i_lrl = -1
      end if
      do i = 1 , 3
         r_platvel1(i) = 0.d0
         r_platacc1(i) = 0.d0
         r_platvel2(i) = 0.d0
         r_platacc2(i) = 0.d0
      end do
      if(i_lrl .ne. 0) then
         a_rdtmp = rdfval('SCH Velocity Vector 1','m/s,m/s,m/s')
         read(unit=a_rdtmp,fmt=*) r_platvel1
         a_rdtmp = rdfval('SCH Acceleration Vector 1','-,-,-')
         read(unit=a_rdtmp,fmt=*) r_platacc1
         a_rdtmp = rdfval('SCH Velocity Vector 2','m/s,m/s,m/s')
         read(unit=a_rdtmp,fmt=*) r_platvel2
         a_rdtmp = rdfval('SCH Acceleration Vector 2','-,-,-')
         read(unit=a_rdtmp,fmt=*) r_platacc2
      end if
      a_rdtmp = rdfval('Range of first sample in data file','m')
      read(unit=a_rdtmp,fmt=*) r001, r002
      a_rdtmp = rdfval('PRF','-')
      read(unit=a_rdtmp,fmt=*) prf1, prf2
      a_rdtmp = rdfval('i/q means','-')
      read(unit=a_rdtmp,fmt=*) xmi1, xmq1, xmi2, xmq2
      a_rdtmp = rdfval('Flip i/q?','-')
      read(unit=a_rdtmp,fmt='(a)') iqflip
      a_rdtmp = rdfval('Azimuth resolution forward backward','m')
      read(unit=a_rdtmp,fmt=*) azres1,azres2
      a_rdtmp = rdfval('Number of azimuth looks','-')
      read(unit=a_rdtmp,fmt=*) nlooks
      a_rdtmp = rdfval('Range sampling rate','hz')
      read(unit=a_rdtmp,fmt=*) fs
      a_rdtmp = rdfval('Range chirp slope','-')
      read(unit=a_rdtmp,fmt=*) slope
      a_rdtmp = rdfval('Range pulse duration','s')
      read(unit=a_rdtmp,fmt=*) pulsedur
      a_rdtmp = rdfval('Range chirp extension points','-')
      read(unit=a_rdtmp,fmt=*) nextend
      a_rdtmp = rdfval('Secondary range migration correction?','-')
      read(unit=a_rdtmp,fmt='(a)') srm
      a_rdtmp = rdfval('Radar wavelength','m')
      read(unit=a_rdtmp,fmt=*) wavl
      a_rdtmp = rdfval('Range spectral weighting','-')
      read(unit=a_rdtmp,fmt=*) rhww
      a_rdtmp = rdfval('Spectral shift fraction','-')
      read(unit=a_rdtmp,fmt=*) pctbw, pctbwaz
      a_rdtmp = rdfval('Linear resampling coefs','-')
      read(unit=a_rdtmp,fmt=*) sloper,interr,slopea, intera
      a_rdtmp = rdfval('Linear resampling deltas','-')
      read(unit=a_rdtmp,fmt=*) dsloper,dinterr,dslopea, dintera
      a_rdtmp = rdfval('AGC file','-')
      read(unit=a_rdtmp,fmt='(a)') agcfile
      a_rdtmp = rdfval('DWP file','-')
      read(unit=a_rdtmp,fmt='(a)') dwpfile
      a_rdtmp = rdfval('Forward Backward filtering','-')
      read(unit=a_rdtmp,fmt=*)iforback
