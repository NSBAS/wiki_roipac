#!/usr/bin/perl
### baseline.pl

$] >= 5.004 or die "Perl version must be >= 5.004 (Currently $]).\n";

use Env qw(INT_SCR);
use lib "$INT_SCR";  #### Location of Generic.pm
use Generic;
use Math::MatrixReal;

###Usage info/check

sub Usage{

`$INT_SCR/pod2man.pl  $INT_SCR/baseline.pl`;
exit 1;
}
@ARGV >= 3 or Usage();
@args = @ARGV;
 
$infile1       = shift;
$infile2       = shift;
$orbit_type    = shift;
$orbit_type    =~ /(ODR|PRC|HDR)/ or die "Orbit type must be ODR, PRC or HDR\n";
$baseline_file = shift;

if ($baseline_file){
  $baseline_file =~ /\.rsc$/ or $baseline_file = "$baseline_file.rsc";
}
else {$baseline_file = "/dev/null";}

$hdrdate1=$infile1;
$hdrdate1=~ s/.slc//;
$hdrdate2=$infile2;
$hdrdate2=~ s/.slc//;

#######################
Message "Checking I/O";
#######################
#$outfile  = "$infile1-${infile2}_baseline";
@Infiles  = ("$infile1.rsc", "$infile2.rsc");
#@Outfiles = ($outfile);
@Outfiles = ($baseline_file);
&IOcheck(\@Infiles, \@Outfiles);
Log("baseline.pl", @args);

##############################################
Message "Reading resource file: $infile1.rsc";
##############################################
$length             = Use_rsc "$infile1 read FILE_LENGTH";
$first_line_utc_1   = Use_rsc "$infile1 read FIRST_LINE_UTC";
$range_pixel_size   = Use_rsc "$infile1 read RANGE_PIXEL_SIZE";
$azimuth_pixel_size = Use_rsc "$infile1 read AZIMUTH_PIXEL_SIZE";
$year1              = Use_rsc "$infile1 read FIRST_LINE_YEAR";
$month1             = Use_rsc "$infile1 read FIRST_LINE_MONTH_OF_YEAR";
$day1               = Use_rsc "$infile1 read FIRST_LINE_DAY_OF_MONTH";
$sat1               = Use_rsc "$infile1 read PLATFORM";
$prf1               = Use_rsc "$infile1 read PRF";
$antenna_side       = Use_rsc "$infile1 read ANTENNA_SIDE";
$h                  = Use_rsc "$infile1 read HEIGHT";
$rt                 = Use_rsc "$infile1 read EARTH_RADIUS";
$r0                 = Use_rsc "$infile1 read STARTING_RANGE";

##############################################
Message "Reading resource file: $infile2.rsc";
##############################################
$first_line_utc_2 = Use_rsc "$infile2 read FIRST_LINE_UTC";
$year2            = Use_rsc "$infile2 read FIRST_LINE_YEAR";
$month2           = Use_rsc "$infile2 read FIRST_LINE_MONTH_OF_YEAR";
$day2             = Use_rsc "$infile2 read FIRST_LINE_DAY_OF_MONTH";
$sat2             = Use_rsc "$infile2 read PLATFORM";
$prf2             = Use_rsc "$infile2 read PRF";
$r0_2             = Use_rsc "$infile2 read STARTING_RANGE";


### Calculate variables
$TimeSpanYear = ($year2-$year1)+($month2-$month1)/12+($day2-$day1)/365.25;
$cosl         = (2*$h*$rt+$h**2+$r0**2)/(2*$r0*($rt+$h));
$sinl         = sqrt(1-$cosl**2);
$l = atan2($sinl,$cosl);
$lDeg = $l/3.14159*180;
print STDERR "lookangle $lDeg\n";

#####################################
Message "Getting orbital parameters";
#####################################

$BaselineOrder = 2;   #The keywords for the baseline only support quadratic fit

for( my $i=0; $i<$BaselineOrder+1; $i++ ){

  $utc1   = $first_line_utc_1 + $i*$length/$prf1/$BaselineOrder;
  ($q1, $q2, $q3, $q4, $q5, $x1[0], $x1[1], $x1[2], $v[0], $v[1], $v[2], $qq)=split /\s+/,
  `$INT_SCR/state_vector.pl $year1$month1$day1 \\
                            $utc1              \\
                            $sat1              \\
                            $orbit_type        \\
                            $hdrdate1`;
  Status "state_vector.pl";
  
  @r = Normalize( @x1 );
  $v = Norm( @v );
  @v = Normalize( @v );
  @c = CrossProduct( \@r, \@v );
  @c = Normalize( @c );
  @v = CrossProduct( \@c, \@r );
  
  if( $i > 0 ){
       $s[$i]  = $s[$i-1] + $length / $prf1 / $BaselineOrder * $v;
  }else{
       $s[$i] = 0;
  }

  $utc2   = $first_line_utc_2 + $i*$length/$prf2/$BaselineOrder;
  ($q1, $q2, $q3, $q4, $q5, $x2[0], $x2[1], $x2[2], $qq) = split /\s+/,
  `$INT_SCR/state_vector.pl $year2$month2$day2 \\
                            $utc2              \\
                            $sat2              \\
                            $orbit_type        \\
                            $hdrdate2`;
  Status "state_vector.pl"; 

  foreach $j (0,1,2){  
    $dx[$j] = $x2[$j]-($x1[$j]);
  }
  $z_offset      = DotProduct( \@dx, \@v );
  $az_offset[$i] = $z_offset;

  $utc2    = $utc2-($z_offset/$v);
  ($q1, $q2, $q3, $q4, $q5, $x2[0], $x2[1], $x2[2], $qq) = split /\s+/,
  `$INT_SCR/state_vector.pl $year2$month2$day2 \\
                            $utc2              \\
                            $sat2              \\
                            $orbit_type        \\
                            $hdrdate2`;
  Status "state_vector.pl";
  foreach $j (0,1,2){
    $dx[$j] = $x2[$j]-($x1[$j]);
  }
  $z_offset      = DotProduct( \@dx, \@v );
  $az_offset[$i] = $az_offset[$i]+$z_offset;

  $utc2    = $utc2-($z_offset/$v);
  ($q1, $q2, $q3, $q4, $q5, $x2[0], $x2[1], $x2[2], $qq) = split /\s+/,
  `$INT_SCR/state_vector.pl $year2$month2$day2 \\
                            $utc2              \\
                            $sat2              \\
                            $orbit_type        \\
                            $hdrdate2`;
  Status "state_vector.pl";
  foreach $j (0,1,2){
    $dx[$j] = $x2[$j]-($x1[$j]);
  }
  $z_offset      = DotProduct( \@dx, \@v );
  $v_offset      = DotProduct( \@dx, \@r );
  $c_offset      = DotProduct( \@dx, \@c );
  $az_offset[$i] = $az_offset[$i]+$z_offset;

  $utc2    = $utc2-($z_offset/$v);
  ($q1, $q2, $q3, $q4, $q5, $x2[0], $x2[1], $x2[2], $qq) = split /\s+/,
  `$INT_SCR/state_vector.pl $year2$month2$day2 \\
                            $utc2              \\
                            $sat2              \\
                            $orbit_type        \\
                            $hdrdate2`;
  Status "state_vector.pl";
   
  foreach $j (0,1,2){  
    $dx[$j] = $x2[$j]-($x1[$j]);
  }
  $z_offset      = DotProduct( \@dx, \@v );
  $v_offset      = DotProduct( \@dx, \@r );
  $c_offset      = DotProduct( \@dx, \@c );
  $vb[$i]   = $v_offset;
  $hb[$i]   = $c_offset;
#  Sign error correction (Belle Philibosian, 2/9/2008)
#  These calculations still assume a right-looking beam; will not work for left-looking
#  $csb[$i]    = $hb[$i]*$cosl+$vb[$i]*$sinl;
#  $asb[$i]    = $hb[$i]*$sinl-$vb[$i]*$cosl;
  $csb[$i]    = -1*$hb[$i]*$cosl+$vb[$i]*$sinl;
  $asb[$i]    = -1*$hb[$i]*$sinl-$vb[$i]*$cosl;
 
}

############################
Message "Calculating baseline";
############################
@hBasPoly = GetCoeff( \@s, \@hb );
@vBasPoly = GetCoeff( \@s, \@vb );

$h_rate    = ($hb[-1]-$hb[0])/($v*$length/$prf1);
$v_rate    = ($vb[-1]-$vb[0])/($v*$length/$prf1);
$h_acc     = 0. ;
$v_acc     = 0. ;

$asb_avg   = ($asb[0]+$asb[-1])/2;
$azb_avg   = ($az_offset[0]+$az_offset[-1])/2;
$az_offset = (-1*$azb_avg-($h_rate)*$r0*$sinl)/$azimuth_pixel_size;
$r_offset  = ($r0-$r0_2-($asb_avg))/$range_pixel_size;
# changed this back to the previous formula EJF 2009/2/9
#$r_offset  = (($asb_avg)+($r0-$r0_2))/$range_pixel_size;

##########################################
#Message "Printing to file $baseline_file.old";
##########################################
#$num = $r0-$r0_2;
#open (TMP, ">$baseline_file.old") or die "Can't write to $baseline_file.old: $!\n";
#print TMP <<END;
#TIME_SPAN_YEAR                  $TimeSpanYear
#H_BASELINE_TOP_${orbit_type}    $hb[0] 
#H_BASELINE_RATE_${orbit_type}   $h_rate      
#H_BASELINE_ACC_${orbit_type}    $h_acc
#V_BASELINE_TOP_${orbit_type}    $vb[0]
#V_BASELINE_RATE_${orbit_type}   $v_rate
#V_BASELINE_ACC_${orbit_type}    $v_acc
#P_BASELINE_TOP_${orbit_type}    $csb[0]
#P_BASELINE_BOTTOM_${orbit_type} $csb[-1]  
#ORB_SLC_AZ_OFFSET_${orbit_type} $az_offset
#ORB_SLC_R_OFFSET_${orbit_type}  $r_offset
#RANGE_OFFSET_${orbit_type}      $num
#PHASE_CONST_${orbit_type}       -99999
#END
#close(TMP) or warn "Can't close $baseline_file.old:$!\n";

##########################################
Message "Printing to file $baseline_file";
##########################################
$num = $r0-$r0_2;

Use_rsc "$baseline_file write TIME_SPAN_YEAR                  $TimeSpanYear";
Doc_rsc(
 RSC_Tip => 'Years between scene collections',
 RSC_Doc => q[
    Precision of one day.  Accuracy of one to two days.
   ],
 RSC_Derivation => q[
    $year2/1, $month2/1, $day2/1 come from
    keywords FIRST_LINE_YEAR, FIRST_LINE_MONTH_OF_YEAR, FIRST_LINE_DAY_OF_MONTH
    in .slc files
    $TimeSpanYear = ($year2-$year1)+($month2-$month1)/12+($day2-$day1)/365.25;
   ],
 RSC_Comment => q[
    Does not account for actual leap-years, or hours/min/sec of scene time.
   ],
 RSC_Type => Real,
 RSC_Unit => 'roughly years',
);


Use_rsc "$baseline_file write H_BASELINE_TOP_${orbit_type}    $hBasPoly[0]";
Doc_rsc(
 RSC_Doc => q[
    'Initial Cross Track Baseline Component' based on RDF usage in flatten.pl
    'Cross Track Baseline' based on RDF usage in diffnsim.pl
   ],
 RSC_Derivation => q[
    See baseline.pl
   ],
 RSC_Comment => q[
    NOTE: Keyword name is created dynamically.
    Portion after last underscore ('_') is ${orbit_type}.
   ],
 RSC_Type => Real,
 RSC_Unit => 'SI:meter',
);


Use_rsc "$baseline_file write H_BASELINE_RATE_${orbit_type}   $hBasPoly[1]";
Doc_rsc(
 RSC_Doc => q[
    'Cross Track Baseline Rate' based on RDF usage in flatten.pl and diffnsim.pl
   ],
 RSC_Derivation => q[
    See baseline.pl
   ],
 RSC_Comment => q[
    NOTE: Keyword name is created dynamically.
    Portion after last underscore ('_') is ${orbit_type}.
   ],
 RSC_Type => Real,
 RSC_Unit => 'SI:meter/SI:meter'
);


Use_rsc "$baseline_file write H_BASELINE_ACC_${orbit_type}    $hBasPoly[2]";
Doc_rsc(
 RSC_Doc => q[
    'Cross Track Baseline Acceleration' based on RDF usage in diffnsim.pl
   ],
 RSC_Derivation => q[
    See baseline.pl
   ],
 RSC_Comment => q[
    NOTE: Keyword name is created dynamically.
    Portion after last underscore ('_') is ${orbit_type}.
   ],
 RSC_Type => Real,
 RSC_Unit => 'SI:meter/SI:meter**2'
);


Use_rsc "$baseline_file write V_BASELINE_TOP_${orbit_type}    $vBasPoly[0]";
Doc_rsc(
 RSC_Doc => q[
    'Initial Vertical Baseline Component' based on RDF usage in flatten.pl
    'Vertical Baseline' based on RDF usage in diffnsim.pl
   ],
 RSC_Derivation => q[
    See baseline.pl
   ],
 RSC_Comment => q[
    NOTE: Keyword name is created dynamically.
    Portion after last underscore ('_') is ${orbit_type}.
   ],
 RSC_Type => Real,
 RSC_Unit => 'SI:meter',
);


Use_rsc "$baseline_file write V_BASELINE_RATE_${orbit_type}   $vBasPoly[1]";
Doc_rsc(
 RSC_Doc => q[
    'Vertical Baseline Rate' based on RDF usage in flatten.pl and diffnsim.pl
   ],
 RSC_Derivation => q[
    See baseline.pl
   ],
 RSC_Comment => q[
    NOTE: Keyword name is created dynamically.
    Portion after last underscore ('_') is ${orbit_type}.
   ],
 RSC_Type => Real,
 RSC_Unit => 'SI:meter/SI:meter'
);


Use_rsc "$baseline_file write V_BASELINE_ACC_${orbit_type}    $vBasPoly[2]";
Doc_rsc(
 RSC_Doc => q[
    'Vertical Baseline Acceleration' based on RDF usage in diffnsim.pl
   ],
 RSC_Derivation => q[
    See baseline.pl
   ],
 RSC_Comment => q[
    NOTE: Keyword name is created dynamically.
    Portion after last underscore ('_') is ${orbit_type}.
   ],
 RSC_Type => Real,
 RSC_Unit => 'SI:meter/SI:meter**2'
);


Use_rsc "$baseline_file write P_BASELINE_TOP_${orbit_type}    $csb[0]";
Doc_rsc(
 RSC_Doc => q[
    Further investigation needed.
   ],
 RSC_Derivation => q[
    See baseline.pl
   ],
 RSC_Comment => q[
    Value does not appear to be used anywhere.

    NOTE: Keyword name is created dynamically.
    Portion after last underscore ('_') is ${orbit_type}.
   ],
 RSC_Type => Real,
 RSC_Unit => 'needs further investigation',
);


Use_rsc "$baseline_file write P_BASELINE_BOTTOM_${orbit_type} $csb[-1]";
Doc_rsc(
 RSC_Doc => q[
    Further investigation needed.
   ],
 RSC_Derivation => q[
    See baseline.pl
   ],
 RSC_Comment => q[
    Value does not appear to be used anywhere.

    NOTE: Keyword name is created dynamically.
    Portion after last underscore ('_') is ${orbit_type}.
   ],
 RSC_Type => Real,
 RSC_Unit => 'needs further investigation',
);


Use_rsc "$baseline_file write ORB_SLC_AZ_OFFSET_${orbit_type} $az_offset";
Doc_rsc(
 RSC_Tip => 'an azimuth_offset',
 RSC_Doc => q[
    Further investigation needed.
   ],
 RSC_Derivation => q[
    See baseline.pl
   ],
 RSC_Comment => q[
    used in make_offset.pl
      $azimuth_offset = int(Use_rsc "$baseline_file read ORB_SLC_AZ_OFFSET_${BaselineType}");
    which is passed as positional parameter 7 to offset.pl
    which becomes $DY in offset.pl
    which becomes $iDY in offset.pl
      via $iDY = int($DY);
    which is passed as RDF parameter
      'Mean Offset Between Reference and Search Images Lines'
    to ampcor.


    NOTE: Keyword name is created dynamically.
    Portion after last underscore ('_') is ${orbit_type}.
   ],
 RSC_Type => Real,
 RSC_Unit => 'needs further investigation',
);


Use_rsc "$baseline_file write ORB_SLC_R_OFFSET_${orbit_type}  $r_offset";
Doc_rsc(
 RSC_Tip => 'a range offset ',
 RSC_Doc => q[
    Further investigation needed.
   ],
 RSC_Derivation => q[
    See baseline.pl
   ],
 RSC_Comment => q[
    used in make_offset.pl
      $range_offset   = int(Use_rsc "$baseline_file read ORB_SLC_R_OFFSET_${BaselineType}");
    which is passed as positional parameter 6 to offset.pl
    which becomes $DX in offset.pl
    which becomes $iDX in offset.pl
      via $iDX = int($DX);
    which is passed as RDF parameter
      'Mean Offset Between Reference and Search Images Samples'
    to ampcor.


    NOTE: Keyword name is created dynamically.
    Portion after last underscore ('_') is ${orbit_type}.
   ],
 RSC_Type => Real,
 RSC_Unit => 'needs further investigation',
);


Use_rsc "$baseline_file write RANGE_OFFSET_${orbit_type}      $num";
Doc_rsc(
 RSC_Tip => 'difference in starting range values',
 RSC_Derivation => q[
    # $r0 is .slc file 1 STARTING_RANGE value
    # $r0_2 is .slc file 2 STARTING_RANGE value
    $num = $r0-$r0_2;
   ],
 RSC_Comment => q[
    NOTE: Keyword name is created dynamically.
    Portion after last underscore ('_') is ${orbit_type}.
   ],
 RSC_Type => Real,
 RSC_Unit => 'SI:meter',
);


Use_rsc "$baseline_file write PHASE_CONST_${orbit_type}       -99999";
Doc_rsc(
 RSC_Tip => 'delta phase',
 RSC_Derivation => q[
    Set to constant -99999 in baseline.pl

    Also,
    'phase' value on
    'Delta range/phase (m/rad)' line in phase2base.out
    output by baseest
    becomes $phase_const in phase2base.pl
   ],
 RSC_Comment => q[
    NOTE: Keyword name is created dynamically.
    Portion after last underscore ('_') is ${orbit_type}.
   ],
 RSC_Type => Real,
 RSC_Unit => 'SI:radian',
);



exit (0);

################################################################################

sub GetCoeff{
	my ( $xRef, $yRef );
	( $xRef, $yRef ) =  @_;
	my @Out;
	my $i;
	my $size = $#$xRef + 1;
	
	my $M = Math::MatrixReal->new($size, $size);     #index start at 1 
	my $Y = Math::MatrixReal->new($size, 1);         #first index is row
	my $A = Math::MatrixReal->new($size, 1);         #second index is column

	for( $j=1; $j<=$size; $j++ ){
		for( $i=1; $i<=$size; $i++ ){
			$M->assign($j,$i,$$xRef[$j-1]**($i-1));
		}
		$Y->assign($j,1,$$yRef[$j-1]);
	}
	
	$A = $M->inverse * $Y;
	
	#$str = $A->as_matlab;
	
	#print"$str\n";
	
	for( $j=0; $j<$size; $j++ ){
		$Out[$j] = $A->[0][$j][0];
	}
	
	return( @Out );
	
}

sub Normalize{
	my @InVector;
	my ($Norm, $i);
	
	@InVector = @_;
	$Norm = Norm( @InVector );
	for( $i=0; $i<=$#InVector; $i++ ){
		$InVector[$i] /= $Norm;
	}
	return( @InVector );
}
		
sub Norm{
	my @InVector;
	my ($Norm, $Component);
	
	@InVector = @_;
	foreach $Component ( @InVector ){
		$Norm += $Component**2;
	}
	return( sqrt( $Norm ) );
}

sub DotProduct{
	my $InVectorRef1;
	my $InVectorRef2;
	my $Out;
	my $i;
	
	( $InVectorRef1, $InVectorRef2 ) = @_;
	
	for( $i=0; $i<=$#$InVectorRef1; $i++ ){
		$Out += $$InVectorRef1[$i] * $$InVectorRef2[$i];
	}
	return( $Out );
	
}
sub CrossProduct{
	my $InVectorRef1;
	my $InVectorRef2;
	my @OutVector;
	my ($i,$j,$k);
	
	( $InVectorRef1, $InVectorRef2 ) = @_;

	for( $i=0; $i<=$#$InVectorRef1; $i++ ){
	
		$j = $i+1;
		if( $j > $#$InVectorRef1 ){ $j -= ( $#$InVectorRef1 + 1 ); }
		$k = $i+2;
		if( $k > $#$InVectorRef1 ){ $k -= ( $#$InVectorRef1 + 1 ); }
		
		$OutVector[$i] = $$InVectorRef1[$j] * $$InVectorRef2[$k]
		               - $$InVectorRef1[$k] * $$InVectorRef2[$j];
	}
	return( @OutVector );
}

################################################################################

=pod

=head1 USAGE

B<baseline.pl>  I<image1> I<image2> I<orbittype> I<[baseline_file]>

I<image*>    = name of slc to be used in interferogram

I<orbittype> = PRC or ODR

=head1 FUNCTION

Computes the baseline of the interferogram formed by the images I<image1> and I<image2>

=head1 ROUTINES CALLED

state_vector.pl

=head1 CALLED BY

process.pl

=head1 FILES USED

I<image1>.slc.rsc

I<image2>.slc.rsc

=head1 FILES CREATED

I<infile1-infile2>_baseline

I<baseline_file>

=head1 HISTORY

Shell Script : Francois ROGEZ 96/98
Perl  Script : Rowena LOHMAN 04/18/98
Rowena Lohman, Jun 10, 1998
Frederic Crampe, Aug 26, 1999

=head1 LAST UPDATE

Belle Philibosian Feb. 9, 2009

=cut
