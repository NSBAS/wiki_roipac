NSBAS_SOURCE_DIR= ../..
include ${NSBAS_SOURCE_DIR}/mk/build2.mk

PROGRAMS= \
	${BIN}/slave_coregistration \
	${BIN}/fitoff\
	${BIN}/fitoff_quad\
	${BIN}/rect_quad\
	${BIN}/nbymr4\
	${BIN}/rect_lookup\
	${BIN}/rect_lookup_trans\
    ${BIN}/rect_lookup_geo2radar\
	${BIN}/IntSim_quad\
	${BIN}/IntSim_rmLayOver \
	${BIN}/rad2incidence \
    ${BIN}/cchz_wave_ln \
	second.o

all: ${PROGRAMS}

second.o:
	${LD} -c second.c

${BIN}/rad2incidence: rad2incidence.o
	${LD} ${LDFLAGS} -o $@ rad2incidence.o -lm

${BIN}/slave_coregistration: slave_coregistration.o second.o
	${FC} ${FFLAGS} ${LDFLAGS} -o $@ slave_coregistration.o ${LIBS}

${BIN}/fitoff: fitoff.o
	${FC} ${FFLAGS} ${LDFLAGS} -o $@ fitoff.o ${LIBS}

${BIN}/fitoff_quad: fitoff_quad.o
	${FC} ${FFLAGS} ${LDFLAGS} -o $@ fitoff_quad.o ${LIBS}

${BIN}/rect_quad: rect_quad.o
	${FC} ${FFLAGS} ${LDFLAGS} -o $@ rect_quad.o ${LIBS}

${BIN}/rect_lookup_trans: rect_lookup_trans.o
	${FC} ${FFLAGS} ${LDFLAGS} -o $@ rect_lookup_trans.o ${LIBS}

${BIN}/rect_lookup_geo2radar: rect_lookup_geo2radar.o
	${FC} ${FFLAGS} ${LDFLAGS} -o $@ rect_lookup_geo2radar.o ${LIBS}

${BIN}/rect_lookup: rect_lookup.o
	${FC} ${FFLAGS} ${LDFLAGS} -o $@ rect_lookup.o ${LIBS}

${BIN}/IntSim_quad: IntSim_quad.o
	${FC} ${FFLAGS} ${LDFLAGS} -o $@ IntSim_quad.o ${LIBS}

${BIN}/IntSim_rmLayOver: IntSim_rmLayOver.o
	${FC} ${FFLAGS} ${LDFLAGS} -o $@ IntSim_rmLayOver.o ${LIBS}

${BIN}/nbymr4: nbymr4.o
	${FC} ${FFLAGS} ${LDFLAGS} -o $@ nbymr4.o ${LIBS}

${BIN}/cchz_wave_ln: cchz_wave_ln.o
	${LD} ${LDFLAGS} -o $@ cchz_wave_ln.o ${LIBS}

clean:
	rm -f *.o; rm -f *.bck;

.PHONY: all clean
