#!/usr/bin/perl

# *** JPL/Caltech Repeat Orbit Interferometry (ROI) Package ***

### rad2inc.pl

use Env qw(NSBAS_BIN INT_SCR NSBAS INT_BIN NSBAS_PERL);
use lib "$INT_SCR";  #### Location of Generic.pm
use Generic;

###Usage info/check
sub Usage{

`$INT_SCR/pod2man.pl  $NSBAS/src/wiki_roipac/rad2incidence.pl`;
exit 1;
}
@ARGV > 2 or Usage();
@args = @ARGV;

$radar    = shift; 
$trans    = shift; 
$geo_inc  = shift; 
$skip = shift;

#$skip or $skip = 1;

#################
Message "Checking I/O";
#################
@Infiles  = ($radar,$trans);
@Outfiles = ($geo_inc);
&IOcheck(\@Infiles, \@Outfiles);
Log("rad2incidence.pl", @args);
	
#########################################
Message "Read $radar.rsc file";
#########################################
	$starting_range   = Use_rsc "$radar read STARTING_RANGE";
	$range_pixel_size = Use_rsc "$radar read RANGE_PIXEL_SIZE";
	$earth_radius     = Use_rsc "$radar read EARTH_RADIUS";
	$height           = Use_rsc "$radar read HEIGHT";
	$width            = Use_rsc "$radar read WIDTH";
	$length           = Use_rsc "$radar read FILE_LENGTH";

#########################################
Message "Create incidence file";
#########################################
Message " $NSBAS_BIN/rad2incidence $starting_range $range_pixel_size $earth_radius $height $width $length radar_inc.r4";
	`$NSBAS_BIN/rad2incidence $starting_range   \\
                                             $range_pixel_size \\
			                     $earth_radius     \\
			                     $height           \\
                                             $width  \\
                                             $length  \\
			                     radar_inc.r4`;
                                             Status "rad2incidence";

        `rmg2mag_phs $radar mag phs $width`;
        `mag_phs2rmg mag radar_inc.r4 radar_inc.unw $width`;
  	`cp $radar.rsc radar_inc.unw.rsc`;

	`$NSBAS_PERL/nsb_geocode.pl $trans        \\
		             radar_inc.unw \\
			     $geo_inc \\
                             $skip`;
			     Status "nsb_geocode.pl";
			     
	`rm rect_lookup.in log*`;

exit 0;

=pod

=head1 USAGE

B<rad2incidence.pl> I<radar_rsc_file trans geo_inc (skip)>

=head1 FUNCTION

compute the incidence geocoded interferogram

=head1 ROUTINES CALLED

rad2incidence

geocode.pl


=head1 CALLED BY


=head1 FILES USED


=head1 FILES CREATED

=head1 HISTORY

Perl  Script : Frederic CRAMPE 06/03/98

=head1 LAST UPDATE

Frederic CRAMPE, Jan 18, 1999

=cut
